alias Ticketee.Repo
alias Ticketee.Projects
alias Ticketee.Projects.{Project, Ticket, Comment, State}
alias Ticketee.Projects.TicketAttachment

alias Ticketee.Accounts
alias Ticketee.Accounts.User

alias Ticketee.Roles
alias Ticketee.Roles.Role

alias Ticketee.Attachments.Upload
alias TicketeeWeb.Router.Helpers, as: Routes

alias Ticketee.Tags.Tag

alias Ticketee.Notifications.{Watchers, TicketWatcher}
