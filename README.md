# Ticketee

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## User Roles

`Viewer` for people who will be able to read everything on the project,
but not edit anything.

`Editor` for people who will be able to read everything and also
create and update tickets on the project.

`Manager` - for people who will be able to read everything,
manage tickets, and edit a project's details.

`Admin` for people able to manage user accounts, user roles,
and projects (including creation and deletion).

## Launching tests

```
    # Go to selenium install folder
    cd ~/Downloads/selenium

    # Launch Selenium server
    java -jar selenium-server-standalone-3.141.59.jar
```

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix
