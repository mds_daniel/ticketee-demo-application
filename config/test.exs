use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :ticketee, Ticketee.Repo,
  username: "postgres",
  password: "postgres",
  database: "ticketee_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ticketee, TicketeeWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure Wallaby for feature testing
config :wallaby,
  otp_app: :ticketee,
  driver: Wallaby.Selenium,
  selenium: [
    capabilities: %{
      javascriptEnabled: true,
      browserName: "firefox",
      # Comment following option in order to see browser window
      "moz:firefoxOptions": %{
        args: ["-headless"]
      }
    }
  ]

# Configure Endpoint server and SQL sandbox
config :ticketee, TicketeeWeb.Endpoint, server: true
config :ticketee, :sql_sandbox, true

# Set Argon2 encryption options for tests
# https://hexdocs.pm/argon2_elixir/Argon2.Stats.html
config :argon2_elixir,
  t_cost: 1,
  m_cost: 8


  # Sending email configuration
  config :ticketee, TicketeeWeb.Notifications.Mailer,
    adapter: Bamboo.TestAdapter
