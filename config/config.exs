# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :ticketee,
  ecto_repos: [Ticketee.Repo]

config :ticketee, Ticketee.Repo,
  migration_timestamps: [type: :timestamptz]

# Configures the endpoint
config :ticketee, TicketeeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "d6IHxXyWVXpzrknJr1eLgKMmH1dHyghELFzxALZwowvm4bC+agl+xErUdD+bwWtF",
  render_errors: [view: TicketeeWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Ticketee.PubSub,
  live_view: [signing_salt: "fYB8PfoW"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure JSON-API support
config :jsonapi,
  namespace: "/api",
  field_transformation: :underscore,
  remove_links: true

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
