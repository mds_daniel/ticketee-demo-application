defmodule Ticketee.Repo.Migrations.CreateRoles do
  use Ecto.Migration

  def change do
    execute(&create_role_enum/0, &drop_role_enum/0)

    create table(:roles) do
      add :role, :project_role, null: false, default: "viewer"
      add :user_id, references(:users,  type: :binary_id, on_delete: :delete_all), null: false
      add :project_id, references(:projects, on_delete: :delete_all), null: false

      timestamps()
    end

    create index(:roles, :user_id)
    create index(:roles, :project_id)
    create unique_index(:roles, [:user_id, :project_id])
  end

  defp create_role_enum do
    repo().query!("
      CREATE TYPE project_role AS ENUM (
        'viewer',
        'editor',
        'manager'
      );
    ")
  end

  defp drop_role_enum do
    repo().query!("
      DROP TYPE project_role;
    ")
  end
end
