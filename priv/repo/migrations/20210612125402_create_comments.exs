defmodule Ticketee.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def change do
    create table(:comments) do
      add :description, :text, null: false
      add :ticket_id, references(:tickets, on_delete: :delete_all), null: false
      add :user_id, references(:users, type: :binary_id, on_delete: :nilify_all)

      timestamps()
    end

    create index(:comments, :ticket_id)
  end
end
