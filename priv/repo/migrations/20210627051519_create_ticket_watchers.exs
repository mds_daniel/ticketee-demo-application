defmodule Ticketee.Repo.Migrations.CreateTicketWatchers do
  use Ecto.Migration

  def change do
    create table(:ticket_watchers) do
      add :ticket_id, references(:tickets, on_delete: :delete_all), null: false
      add :user_id, references(:users, type: :binary_id, on_delete: :delete_all), null: false
    end

    create unique_index(:ticket_watchers, [:ticket_id, :user_id])
  end
end
