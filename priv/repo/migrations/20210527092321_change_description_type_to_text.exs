defmodule Ticketee.Repo.Migrations.ChangeDescriptionTypeToText do
  use Ecto.Migration

  def change do
    alter table(:projects) do
      modify :description, :text, from: :string
    end

    alter table(:tickets) do
      modify :description, :text, from: :string
    end
  end
end
