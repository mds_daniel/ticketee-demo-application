defmodule Ticketee.Repo.Migrations.AddArchivedAtToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :archived_at, migration_timestamp_type()
    end
  end

  def migration_timestamp_type do
    Application.get_env(:ticketee, Ticketee.Repo)
    |> Keyword.fetch!(:migration_timestamps)
    |> Keyword.fetch!(:type)
  end
end
