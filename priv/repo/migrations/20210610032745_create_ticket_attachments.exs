defmodule Ticketee.Repo.Migrations.CreateTicketAttachments do
  use Ecto.Migration

  def change do
    create table(:ticket_attachments) do
      add :file, :string, null: false
      add :size, :integer, null: false, default: 0
      add :description, :text
      add :ticket_id, references(:tickets, on_delete: :delete_all), null: false

      timestamps()
    end

    create constraint(:ticket_attachments, "size_must_be_non_negative", check: "size >= 0")
    create unique_index(:ticket_attachments, [:ticket_id, :file])
  end
end
