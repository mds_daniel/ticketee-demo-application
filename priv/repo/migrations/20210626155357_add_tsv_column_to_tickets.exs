defmodule Ticketee.Repo.Migrations.AddTsvColumnToTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      add :tsv, :tsvector
    end

    create index(:tickets, :tsv, using: :gin)

    execute(&create_tsv_trigger/0, &drop_tsv_trigger/0)
  end

  defp create_tsv_trigger do
    repo().query!("""
      CREATE TRIGGER tickets_tsv_trigger
      BEFORE INSERT OR UPDATE ON tickets
      FOR EACH ROW EXECUTE FUNCTION
      tsvector_update_trigger(tsv, 'pg_catalog.english', title)
    """, [], [log: :info])

    # Update existing records through update trigger
    repo().query!("""
      UPDATE tickets SET tsv = ''::tsvector
    """, [], [log: :info])
  end

  defp drop_tsv_trigger do
    repo().query!("""
      DROP TRIGGER tickets_tsv_trigger ON tickets;
    """, [], [log: :info])
  end
end
