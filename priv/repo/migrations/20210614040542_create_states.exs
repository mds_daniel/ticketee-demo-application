defmodule Ticketee.Repo.Migrations.CreateStates do
  use Ecto.Migration

  def change do
    create table(:states) do
      add :name, :string, null: false
      add :color, :string, null: false
      add :order_no, :integer, null: false, default: 0
      add :default, :boolean, null: false, default: false
    end

    create unique_index(:states, :name)

    alter table(:tickets) do
      add :state_id, references(:states, on_delete: :restrict)
    end

    create index(:tickets, :state_id)

    alter table(:comments) do
      add :state_id, references(:states, on_delete: :restrict)
      add :previous_state_id, references(:states, on_delete: :restrict)
    end

    create index(:comments, :state_id)
  end
end
