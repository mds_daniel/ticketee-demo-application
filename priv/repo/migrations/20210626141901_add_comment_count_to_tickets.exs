defmodule Ticketee.Repo.Migrations.AddCommentCountToTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      add :comment_count, :integer, null: false, default: 0
    end
  end
end
