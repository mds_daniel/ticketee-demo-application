defmodule Ticketee.Repo.Migrations.AddAttachmentToTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      add :attachment, :string
    end
  end
end
