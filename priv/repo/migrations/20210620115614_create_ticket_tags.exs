defmodule Ticketee.Repo.Migrations.CreateTicketTags do
  use Ecto.Migration

  def change do
    create table(:ticket_tags) do
      add :ticket_id, references(:tickets, on_delete: :delete_all), null: false
      add :tag_id, references(:tags, on_delete: :delete_all), null: false
    end

    create index(:ticket_tags, :ticket_id)
    create index(:ticket_tags, :tag_id)
    create unique_index(:ticket_tags, [:ticket_id, :tag_id])
  end
end
