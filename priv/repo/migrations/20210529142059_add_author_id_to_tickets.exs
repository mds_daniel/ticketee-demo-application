defmodule Ticketee.Repo.Migrations.AddAuthorIdToTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      add :author_id, references(:users, type: :uuid, on_delete: :nilify_all)
    end

    create index(:tickets, :author_id)
  end
end
