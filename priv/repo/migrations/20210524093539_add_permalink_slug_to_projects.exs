defmodule Ticketee.Repo.Migrations.AddPermalinkSlugToProjects do
  use Ecto.Migration

  def change do
    alter table(:projects) do
      add :slug, :string
    end
  end
end
