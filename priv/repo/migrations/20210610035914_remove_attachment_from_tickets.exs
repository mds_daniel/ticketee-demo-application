defmodule Ticketee.Repo.Migrations.RemoveAttachmentFromTickets do
  use Ecto.Migration

  def change do
    alter table(:tickets) do
      remove :attachment, :string
    end
  end
end
