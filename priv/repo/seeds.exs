# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Ticketee.Repo.insert!(%Ticketee.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Ticketee.Projects

{:ok, _} = Projects.create_state(%{name: "New", color: "#0066CC", order_no: 1})
{:ok, _} = Projects.create_state(%{name: "Open", color: "#008000", order_no: 2})
{:ok, _} = Projects.create_state(%{name: "In Progress", color: "#ffc107", order_no: 3})
{:ok, _} = Projects.create_state(%{name: "Awesome", color: "#663399", order_no: 4})
{:ok, _} = Projects.create_state(%{name: "Closed", color: "#990000", order_no: 5})
