defmodule Ticketee.Authorize.TicketWatchersTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Roles
  alias Ticketee.Notifications.TicketWatcher


  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, user: user, project: project, ticket: ticket}
  end

  test "when not logged in no action is permitted", %{ticket: ticket} do
    refute Authorize.allowed?(nil, :create, {ticket, TicketWatcher})
    refute Authorize.allowed?(nil, :delete, {ticket, TicketWatcher})
  end

  test "without role on project no action is permitted", %{user: user, ticket: ticket} do
    refute Authorize.allowed?(user, :create, {ticket, TicketWatcher})
    refute Authorize.allowed?(user, :delete, {ticket, TicketWatcher})
  end

  test "project viewers are allowed to create and delete ticket watchers",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :viewer, project)

    assert Authorize.allowed?(user, :create, {ticket, TicketWatcher})
    assert Authorize.allowed?(user, :delete, {ticket, TicketWatcher})
  end

  test "project editors are allowed to create and delete ticket watchers",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :editor, project)

    assert Authorize.allowed?(user, :create, {ticket, TicketWatcher})
    assert Authorize.allowed?(user, :delete, {ticket, TicketWatcher})
  end

  test "project managers are allowed to create and delete ticket watchers",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :create, {ticket, TicketWatcher})
    assert Authorize.allowed?(user, :delete, {ticket, TicketWatcher})
  end

  test "admins are allowed to see, create and delete ticket watchers",
      %{user: user, ticket: ticket} do
    {:ok, user} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(user, :create, {ticket, TicketWatcher})
    assert Authorize.allowed?(user, :delete, {ticket, TicketWatcher})
  end
end
