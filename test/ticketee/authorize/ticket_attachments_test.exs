defmodule Ticketee.Authorize.TicketAttachmentsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)
    attachment = TestFactory.insert(:ticket_attachment, ticket: ticket)

    {:ok, user: user, project: project, ticket: ticket, attachment: attachment}
  end

  test "when not logged in no action is permitted", %{ticket: ticket, attachment: attachment} do
    refute Authorize.allowed?(nil, :show, {ticket, attachment})
    refute Authorize.allowed?(nil, :create, {ticket, TicketAttachment})
    refute Authorize.allowed?(nil, :delete, {ticket, attachment})
  end

  test "without roles no action is permitted", %{user: user, ticket: ticket, attachment: attachment} do
    refute Authorize.allowed?(user, :show, {ticket, attachment})
    refute Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    refute Authorize.allowed?(user, :delete, {ticket, attachment})
  end

  test "project viewers are allowed to see ticket attachments",
      %{user: user, ticket: ticket, project: project, attachment: attachment} do
    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    assert Authorize.allowed?(user, :show, {ticket, attachment})
    refute Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    refute Authorize.allowed?(user, :delete, {ticket, attachment})
  end

  test "project editors are allowed to see ticket attachments",
      %{user: user, ticket: ticket, project: project, attachment: attachment} do
    {:ok, _role} = Roles.grant_role(user, :editor, project)

    assert Authorize.allowed?(user, :show, {ticket, attachment})
    refute Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    refute Authorize.allowed?(user, :delete, {ticket, attachment})
  end

  test "project editors are allowed to create ticket attachments for own tickets",
      %{user: user, project: project, attachment: attachment} do
    {:ok, _role} = Roles.grant_role(user, :editor, project)
    ticket = TestFactory.insert(:ticket, author: user, project: project)

    assert Authorize.allowed?(user, :show, {ticket, attachment})
    assert Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    refute Authorize.allowed?(user, :delete, {ticket, attachment})
  end

  test "project managers are allowed to see, create and delete ticket attachments",
      %{user: user, ticket: ticket, project: project, attachment: attachment} do
    {:ok, _role} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :show, {ticket, attachment})
    assert Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    assert Authorize.allowed?(user, :delete, {ticket, attachment})
  end

  test "admins are allowed to see, create and delete ticket attachments",
      %{user: user, ticket: ticket, attachment: attachment} do
    {:ok, user} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(user, :show, {ticket, attachment})
    assert Authorize.allowed?(user, :create, {ticket, TicketAttachment})
    assert Authorize.allowed?(user, :delete, {ticket, attachment})
  end
end
