defmodule Ticketee.Authorize.TicketsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Projects.Ticket
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, user: user, project: project, ticket: ticket}
  end

  test "when not logged in no action is permitted",
      %{project: project, ticket: ticket} do
    refute Authorize.allowed?(nil, :show, {project, ticket})
    refute Authorize.allowed?(nil, :create, {project, Ticket})
    refute Authorize.allowed?(nil, :update, {project, ticket})
    refute Authorize.allowed?(nil, :delete, {project, ticket})
  end

  test "without roles on project no action is permitted",
      %{user: user, project: project, ticket: ticket} do
    refute Authorize.allowed?(user, :show, {project, ticket})
    refute Authorize.allowed?(user, :create, {project, Ticket})
    refute Authorize.allowed?(user, :update, {project, ticket})
    refute Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "project viewers are allowed to see tickets",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :viewer, project)

    assert Authorize.allowed?(user, :show, {project, ticket})
    refute Authorize.allowed?(user, :create, {project, Ticket})
    refute Authorize.allowed?(user, :update, {project, ticket})
    refute Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "project editors are allowed to see and create tickets",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :editor, project)

    assert Authorize.allowed?(user, :show, {project, ticket})
    assert Authorize.allowed?(user, :create, {project, Ticket})
    refute Authorize.allowed?(user, :update, {project, ticket})
    refute Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "project editors are allowed to update own tickets",
      %{user: user, project: project} do
    {:ok, _} = Roles.grant_role(user, :editor, project)
    ticket = TestFactory.insert(:ticket, author: user, project: project)

    assert Authorize.allowed?(user, :show, {project, ticket})
    assert Authorize.allowed?(user, :update, {project, ticket})
    refute Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "project managers are allowed to see, create, update, delete tickets",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :show, {project, ticket})
    assert Authorize.allowed?(user, :create, {project, Ticket})
    assert Authorize.allowed?(user, :update, {project, ticket})
    assert Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "managers of other projects are not permitted any action",
      %{user: user, project: project, ticket: ticket} do
    other_project = TestFactory.insert(:project)
    {:ok, _} = Roles.grant_role(user, :manager, other_project)

    refute Authorize.allowed?(user, :show, {project, ticket})
    refute Authorize.allowed?(user, :create, {project, Ticket})
    refute Authorize.allowed?(user, :update, {project, ticket})
    refute Authorize.allowed?(user, :delete, {project, ticket})
  end

  test "admins are allowed to see, create, update, delete tickets",
      %{user: user, project: project, ticket: ticket} do
    {:ok, user} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(user, :show, {project, ticket})
    assert Authorize.allowed?(user, :create, {project, Ticket})
    assert Authorize.allowed?(user, :update, {project, ticket})
    assert Authorize.allowed?(user, :delete, {project, ticket})
  end
end
