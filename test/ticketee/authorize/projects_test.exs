defmodule Ticketee.Authorize.ProjectsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)

    {:ok, user: user, project: project}
  end

  test "when not logged in no action is permitted", %{project: project} do
    refute Authorize.allowed?(nil, :show, project)
    refute Authorize.allowed?(nil, :update, project)
    refute Authorize.allowed?(nil, :delete, project)
  end

  test "without roles on project no action is permitted", %{user: user, project: project} do
    refute Authorize.allowed?(user, :show, project)
    refute Authorize.allowed?(user, :update, project)
    refute Authorize.allowed?(user, :delete, project)
  end

  test "viewers are allowed to see project", %{user: user, project: project} do
    {:ok, _} = Roles.grant_role(user, :viewer, project)

    assert Authorize.allowed?(user, :show, project)
    refute Authorize.allowed?(user, :update, project)
    refute Authorize.allowed?(user, :delete, project)
  end

  test "editors are allowed to see project", %{user: user, project: project} do
    {:ok, _} = Roles.grant_role(user, :editor, project)

    assert Authorize.allowed?(user, :show, project)
    refute Authorize.allowed?(user, :update, project)
    refute Authorize.allowed?(user, :delete, project)
  end

  test "managers are allowed to see and update project", %{user: user, project: project} do
    {:ok, _} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :show, project)
    assert Authorize.allowed?(user, :update, project)
    refute Authorize.allowed?(user, :delete, project)
  end

  test "managers of other projects are not allowed to perform any action", %{user: user, project: project} do
    other_project = TestFactory.insert(:project)
    {:ok, _} = Roles.grant_role(user, :manager, other_project)

    refute Authorize.allowed?(user, :show, project)
    refute Authorize.allowed?(user, :update, project)
    refute Authorize.allowed?(user, :delete, project)
  end

  test "admin can perform any action", %{user: user, project: project} do
    {:ok, admin} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(admin, :show, project)
    assert Authorize.allowed?(admin, :update, project)
    assert Authorize.allowed?(admin, :delete, project)
  end
end
