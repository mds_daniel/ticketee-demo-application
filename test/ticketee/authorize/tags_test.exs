defmodule Ticketee.Authorize.TagsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Tags.Tag
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)
    tag = TestFactory.insert(:tag)

    {:ok, user: user, project: project, ticket: ticket, tag: tag}
  end

  test "when not logged in no action is permitted", %{ticket: ticket, tag: tag} do
    refute Authorize.allowed?(nil, :index, {ticket, Tag})
    refute Authorize.allowed?(nil, :create, {ticket, Tag})
    refute Authorize.allowed?(nil, :delete, {ticket, tag})
  end

  test "without roles no action is permitted", %{user: user, ticket: ticket, tag: tag} do
    refute Authorize.allowed?(user, :index, {ticket, Tag})
    refute Authorize.allowed?(user, :create, {ticket, Tag})
    refute Authorize.allowed?(user, :delete, {ticket, tag})
  end

  test "project viewers are not permitted any actions",
      %{user: user, ticket: ticket, project: project, tag: tag} do
    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    refute Authorize.allowed?(user, :index, {ticket, Tag})
    refute Authorize.allowed?(user, :create, {ticket, Tag})
    refute Authorize.allowed?(user, :delete, {ticket, tag})
  end

  test "project editors are not permitted any actions",
      %{user: user, ticket: ticket, project: project, tag: tag} do
    {:ok, _role} = Roles.grant_role(user, :editor, project)

    refute Authorize.allowed?(user, :index, {ticket, Tag})
    refute Authorize.allowed?(user, :create, {ticket, Tag})
    refute Authorize.allowed?(user, :delete, {ticket, tag})
  end

  test "project managers are allowed to see, create and delete ticket tags",
      %{user: user, ticket: ticket, project: project, tag: tag} do
    {:ok, _role} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :index, {ticket, Tag})
    assert Authorize.allowed?(user, :create, {ticket, Tag})
    assert Authorize.allowed?(user, :delete, {ticket, tag})
  end

  test "admins are allowed to see, create and delete ticket tags",
      %{user: user, ticket: ticket, tag: tag} do
    {:ok, user} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(user, :index, {ticket, Tag})
    assert Authorize.allowed?(user, :create, {ticket, Tag})
    assert Authorize.allowed?(user, :delete, {ticket, tag})
  end
end
