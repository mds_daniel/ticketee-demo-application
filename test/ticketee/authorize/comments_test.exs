defmodule Ticketee.Authorize.CommentsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Roles
  alias Ticketee.Projects.Comment

  setup do
    user = TestFactory.insert(:user)
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)
    comment = TestFactory.insert(:comment, ticket: ticket)

    {:ok, user: user, project: project, ticket: ticket, comment: comment}
  end

  test "when not logged in no action is permitted",
      %{ticket: ticket, comment: comment} do
    refute Authorize.allowed?(nil, :index, {ticket, Comment})
    refute Authorize.allowed?(nil, :create, {ticket, Comment})

    refute Authorize.allowed?(nil, :show, {ticket, comment})
    refute Authorize.allowed?(nil, :update, {ticket, comment})
    refute Authorize.allowed?(nil, :delete, {ticket, comment})
  end

  test "without role on project no action is permitted",
      %{user: user, ticket: ticket, comment: comment} do
    refute Authorize.allowed?(user, :index, {ticket, Comment})
    refute Authorize.allowed?(user, :create, {ticket, Comment})

    refute Authorize.allowed?(user, :show, {ticket, comment})
    refute Authorize.allowed?(user, :update, {ticket, comment})
    refute Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "project viewers are allowed to see and create comments",
      %{user: user, project: project, ticket: ticket, comment: comment} do
    {:ok, _} = Roles.grant_role(user, :viewer, project)

    assert Authorize.allowed?(user, :index, {ticket, Comment})
    assert Authorize.allowed?(user, :create, {ticket, Comment})

    assert Authorize.allowed?(user, :show, {ticket, comment})
    refute Authorize.allowed?(user, :update, {ticket, comment})
    refute Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "project viewers are allowed to update and delete their own comments",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :viewer, project)
    comment = TestFactory.insert(:comment, ticket: ticket, user: user)

    assert Authorize.allowed?(user, :show, {ticket, comment})
    assert Authorize.allowed?(user, :update, {ticket, comment})
    assert Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "project editors are allowed to see and create comments",
      %{user: user, project: project, ticket: ticket, comment: comment} do
    {:ok, _} = Roles.grant_role(user, :editor, project)


    assert Authorize.allowed?(user, :index, {ticket, Comment})
    assert Authorize.allowed?(user, :create, {ticket, Comment})

    assert Authorize.allowed?(user, :show, {ticket, comment})
    refute Authorize.allowed?(user, :update, {ticket, comment})
    refute Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "project editors are allowed to update and delete their own comments",
      %{user: user, project: project, ticket: ticket} do
    {:ok, _} = Roles.grant_role(user, :editor, project)
    comment = TestFactory.insert(:comment, ticket: ticket, user: user)

    assert Authorize.allowed?(user, :show, {ticket, comment})
    assert Authorize.allowed?(user, :update, {ticket, comment})
    assert Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "project managers are allowed to se, create, update, delete comments",
      %{user: user, project: project, ticket: ticket, comment: comment} do
    {:ok, _} = Roles.grant_role(user, :manager, project)

    assert Authorize.allowed?(user, :index, {ticket, Comment})
    assert Authorize.allowed?(user, :create, {ticket, Comment})

    assert Authorize.allowed?(user, :show, {ticket, comment})
    assert Authorize.allowed?(user, :update, {ticket, comment})
    assert Authorize.allowed?(user, :delete, {ticket, comment})
  end

  test "admins are allowed to see, create, update, delete",
      %{user: user, ticket: ticket, comment: comment} do
    {:ok, user} = Accounts.promote_to_admin(user)

    assert Authorize.allowed?(user, :index, {ticket, Comment})
    assert Authorize.allowed?(user, :create, {ticket, Comment})

    assert Authorize.allowed?(user, :show, {ticket, comment})
    assert Authorize.allowed?(user, :update, {ticket, comment})
    assert Authorize.allowed?(user, :delete, {ticket, comment})
  end
end
