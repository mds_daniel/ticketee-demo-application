defmodule Ticketee.PermalinkTest do
  use ExUnit.Case, async: true
  alias Ticketee.Permalink

  describe "cast/1" do
    test "casts integer values correctly" do
      assert Permalink.cast(42) == {:ok, 42}
      assert Permalink.cast(-10) == {:ok, -10}
      assert Permalink.cast(0) == {:ok, 0}
    end

    test "casts binaries correctly" do
      assert Permalink.cast("42") == {:ok, 42}

      # Only casts positive values to integer
      assert Permalink.cast("-10") == :error
      assert Permalink.cast("0") == :error
    end

    test "casts slugs correctly" do
      assert Permalink.cast("42-the-answer-to-everything") == {:ok, 42}
      assert Permalink.cast("2-apples-cost-a-penny") == {:ok, 2}

      assert Permalink.cast("invalid") == :error

      # Id must precede slug text
      assert Permalink.cast("the-answer-to-everything-42") == :error
    end
  end

  test "slugify/1 converts text to slug" do
    assert Permalink.slugify("Dragons  are  awesome") == "dragons-are-awesome"
    assert Permalink.slugify("42 is the Answer to Everything") == "42-is-the-answer-to-everything"
    assert Permalink.slugify("apples") == "apples"
  end
end
