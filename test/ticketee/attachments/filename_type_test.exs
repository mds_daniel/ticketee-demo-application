defmodule Ticketee.Attachments.FilenameTypeTest do
  use ExUnit.Case, async: true
  import Ticketee.TestFixtureFiles, only: [fixture_file_upload: 1]
  alias Ticketee.Attachments.FilenameType

  test "cast/1 converts upload filename" do
    upload = fixture_file_upload("Something Good.TXT")
    assert FilenameType.cast(upload) == {:ok, "something_good.txt"}
  end

  test "cast/1 returns error when given invalid value" do
    assert FilenameType.cast(:invalid) == :error
  end

  test "normalize_filename/1" do
    assert FilenameType.normalize_filename("Some good File.TxT") == "some_good_file.txt"
    assert FilenameType.normalize_filename("a-nice_story.pdf") == "a-nice_story.pdf"
    assert FilenameType.normalize_filename("a/b/c/../d.pdf") == "d.pdf"
    assert FilenameType.normalize_filename("Dragons are awesome!!!.txt") == "dragons_are_awesome_.txt"
  end
end
