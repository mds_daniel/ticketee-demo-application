defmodule Ticketee.Attachments.HelpersTest do
  use ExUnit.Case, async: true
  alias Ticketee.Attachments.Helpers

  test "file_size_description/1 returns correct size description" do
    assert Helpers.file_size_description(204) == "204 B"
    assert Helpers.file_size_description(2048) == "2.0 KB"
    assert Helpers.file_size_description(7 * 1024 * 1024) == "7.0 MB"
    assert Helpers.file_size_description(5 * 1024 * 1024 * 1024) == "5.0 GB"

    assert Helpers.file_size_description(5000) == "4.88 KB"
    assert Helpers.file_size_description(80_000_000) == "76.29 MB"
    assert Helpers.file_size_description(4_000_000_000) == "3.73 GB"
  end
end
