defmodule Ticketee.RolesTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Roles
  alias Ticketee.Roles.Role
  alias Ticketee.Projects
  alias Ticketee.TestFactory

  describe "listing_roles" do
    setup do
      user_a = TestFactory.insert(:user, email: "user_a@#{__MODULE__}.com")
      user_b = TestFactory.insert(:user, email: "user_b@#{__MODULE__}.com")

      project_a = TestFactory.insert(:project, name: "Project A #{__MODULE__}")
      project_b = TestFactory.insert(:project, name: "Project B #{__MODULE__}")
      project_c = TestFactory.insert(:project, name: "Project C #{__MODULE__}")

      {:ok, _} = Roles.grant_role(user_a, :manager, project_a)
      {:ok, _} = Roles.grant_role(user_a, :viewer, project_c)

      {:ok, _} = Roles.grant_role(user_b, :viewer, project_a)
      {:ok, _} = Roles.grant_role(user_b, :editor, project_b)

      {:ok, users: [user_a, user_b], projects: [project_a, project_b, project_c]}
    end

    test "list_roles/0",
        %{users: [user_a, user_b],
          projects: [project_a, project_b, project_c]} do
      all_roles = Roles.list_roles()
        |> Roles.with_user()
        |> Roles.with_project()
        |> Enum.map(&{&1.user, &1.role, &1.project})
        |> Enum.sort()

      assert all_roles == Enum.sort([
        {user_a, :manager, project_a},
        {user_a, :viewer, project_c},
        {user_b, :viewer, project_a},
        {user_b, :editor, project_b},
      ])
    end

    test "roles_for_project/1",
        %{users: [user_a, user_b],
          projects: [project_a, project_b, project_c]} do
      assert (
          Roles.roles_for_project(project_a)
          |> pluck_user_id_and_role()
          |> Enum.sort()
        ) == Enum.sort([
          {user_a.id, :manager},
          {user_b.id, :viewer},
        ])

      assert (
          Roles.roles_for_project(project_b)
          |> pluck_user_id_and_role()
        ) == [{user_b.id, :editor}]

      assert (
          Roles.roles_for_project(project_c)
          |> pluck_user_id_and_role()
        ) == [{user_a.id, :viewer}]
    end

    test "roles_for_user/1",
        %{users: [user_a, user_b],
          projects: [project_a, project_b, project_c]} do
      assert (
          Roles.roles_for_user(user_a)
          |> pluck_project_id_and_role()
          |> Enum.sort()
        ) == Enum.sort([
          {project_a.id, :manager},
          {project_c.id, :viewer},
        ])

      assert (
          Roles.roles_for_user(user_b)
          |> pluck_project_id_and_role()
          |> Enum.sort()
        ) == Enum.sort([
          {project_a.id, :viewer},
          {project_b.id, :editor},
        ])
    end

    test "project_role_summary_for/1",
        %{users: [user_a, user_b],
          projects: [project_a, project_b, project_c]} do
      assert Roles.project_role_summary_for(user_a) == [
          %{project_id: project_a.id, project: project_a.name, role: :manager},
          %{project_id: project_c.id, project: project_c.name, role: :viewer},
        ]

      assert Roles.project_role_summary_for(user_b) == [
          %{project_id: project_a.id, project: project_a.name, role: :viewer},
          %{project_id: project_b.id, project: project_b.name, role: :editor},
        ]
    end
  end

  describe "grant_role/3" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      project = TestFactory.insert(:project, name: "Project #{__MODULE__}")

      {:ok, user: user, project: project}
    end

    test "grants user role on given project", %{user: user, project: project} do
      assert {:ok, %Role{role: :editor}} = Roles.grant_role(user, :editor, project)
      assert %Role{role: :editor} = Roles.role_on_project(user, project)
    end

    test "updates user role on given project when role exists", %{user: user, project: project} do
      # Precondition
      assert {:ok, %Role{role: :editor}} = Roles.grant_role(user, :editor, project)

      assert {:ok, %Role{role: :manager}} = Roles.grant_role(user, :manager, project)
      assert %Role{role: :manager} = Roles.role_on_project(user, project)
    end

    test "when role invalid returns errors", %{user: user, project: project} do
      assert {:error, changeset} = Roles.grant_role(user, :superhero, project)
      assert %{role: ["is invalid"]} = errors_on(changeset)
    end

    test "when user does not exist", %{user: user, project: project} do
      other_user = %{user | id: TestFactory.user_uuid()}

      assert {:error, changeset} = Roles.grant_role(other_user, :editor, project)
      assert %{user: ["does not exist"]} = errors_on(changeset)
    end

    test "when project does not exist anymore", %{user: user, project: project} do
      assert {:ok, _} = Projects.delete_project(project)

      assert {:error, changeset} = Roles.grant_role(user, :editor, project)
      assert %{project: ["does not exist"]} = errors_on(changeset)
    end
  end

  describe "revoke_role/2" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      project = TestFactory.insert(:project, name: "Project #{__MODULE__}")

      {:ok, user: user, project: project}
    end

    test "revokes user role on given project", %{user: user, project: project} do
      assert {:ok, %Role{role: :editor}} = Roles.grant_role(user, :editor, project)

      # Assert role revoked and returned
      assert {:ok, %Role{role: :editor}} = Roles.revoke_role(user, project)

      assert Roles.role_on_project(user, project) == nil
    end

    test "returns error when no role found", %{user: user, project: project} do
      assert {:error, :not_found} == Roles.revoke_role(user, project)
    end
  end

  ## Helpers

  defp pluck_user_id_and_role(roles) do
    Enum.map(roles, fn %Role{user_id: user_id, role: role} ->
      {user_id, role}
    end)
  end

  defp pluck_project_id_and_role(roles) do
    Enum.map(roles, fn %Role{project_id: project_id, role: role} ->
      {project_id, role}
    end)
  end
end
