defmodule Ticketee.Accounts.UserTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Accounts.User
  alias Ticketee.Passwords

  @valid_attrs %{
    "email" => "test_user@#{__MODULE__}.com",
  }

  describe "changeset/2" do
    test "with valid attributes returns valid changeset" do
      changeset = User.changeset(%User{}, @valid_attrs)

      assert changeset.valid?

      user = apply_changes(changeset)
      assert user.email == "test_user@#{__MODULE__}.com"
    end

    test "with invalid email returns errors" do
      invalid_attrs = Map.put(@valid_attrs, "email", "invalid")

      changeset = User.changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{email: ["has invalid format"]} == errors_on(changeset)
    end

    test "with missing email returns errors" do
      invalid_attrs = Map.put(@valid_attrs, "email", "    ")

      changeset = User.changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{email: ["can't be blank"]} == errors_on(changeset)
    end
  end

  describe "registration_changeset/2" do
    @registration_attrs Map.merge(@valid_attrs, %{
      "password" => "super secret",
      "password_confirmation" => "super secret",
    })

    test "with valid attributes returns valid changeset" do
      changeset = User.registration_changeset(%User{}, @registration_attrs)

      assert changeset.valid?

      user = apply_changes(changeset)
      assert user.email == "test_user@#{__MODULE__}.com"
      assert user.password == nil
      assert user.password_hash =~ "$argon2"
      assert Passwords.verify("super secret", user.password_hash)
    end

    test "when password is missing returns errors" do
      invalid_attrs = Map.put(@registration_attrs, "password", "    ")
      changeset = User.registration_changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{password: ["can't be blank"]} = errors_on(changeset)
    end

    test "when password is too short returns errors" do
      invalid_attrs = Map.put(@registration_attrs, "password", "apple")
      changeset = User.registration_changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{password: ["should be at least 6 character(s)"]} = errors_on(changeset)
    end

    test "when password confirmation is blank returns errors" do
      invalid_attrs = Map.put(@registration_attrs, "password_confirmation", "    ")
      changeset = User.registration_changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{password_confirmation: ["does not match confirmation"]} = errors_on(changeset)
    end

    test "when password confirmation does not match returns errors" do
      invalid_attrs = Map.put(@registration_attrs, "password_confirmation", "something else")
      changeset = User.registration_changeset(%User{}, invalid_attrs)

      refute changeset.valid?
      assert %{password_confirmation: ["does not match confirmation"]} = errors_on(changeset)
    end
  end

  describe "update_account_changeset/2" do
    setup do
      user = build_user("test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    @update_account_attrs %{
      "email" => "updated_user@#{__MODULE__}.com",
      "password" => "updated password",
      "password_confirmation" => "updated password",
    }

    test "with valid attributes returns valid update changeset", %{user: user} do
      changeset = User.update_account_changeset(user, @update_account_attrs)

      assert changeset.valid?

      user = apply_changes(changeset)
      assert user.email == "updated_user@#{__MODULE__}.com"
      assert Passwords.verify("updated password", user.password_hash)
    end

    test "when updated email is blank returns errors", %{user: user} do
      invalid_attrs = Map.put(@update_account_attrs, "email", "     ")

      changeset = User.update_account_changeset(user, invalid_attrs)

      refute changeset.valid?
      assert %{email: ["can't be blank"]} = errors_on(changeset)
    end

    test "when updated email is invalid returns errors", %{user: user} do
      invalid_attrs = Map.put(@update_account_attrs, "email", "invalid")

      changeset = User.update_account_changeset(user, invalid_attrs)

      refute changeset.valid?
      assert %{email: ["has invalid format"]} = errors_on(changeset)
    end

    test "when password is missing updates only email", %{user: user} do
      account_attrs = Map.merge(@update_account_attrs, %{
        "password" => "",
        "password_confirmation" => "",
      })

      changeset = User.update_account_changeset(user, account_attrs)

      assert %Ecto.Changeset{valid?: true} = changeset

      user = apply_changes(changeset)
      assert user.email == "updated_user@#{__MODULE__}.com"
      assert Passwords.verify("super secret", user.password_hash)
    end
  end

  ## Helpers

  defp build_user(email) do
     %User{
      email: email,
      password_hash: Passwords.hash("super secret"),
    }
  end
end
