defmodule Ticketee.Projects.StateTest do
  use Ticketee.DataCase, async: true
  alias Ticketee.Projects.State

  @valid_attrs %{
    "name" => "Awesome #{__MODULE__}",
    "color" => "#663300",
    "order_no" => 5,
  }

  test "changeset/2 with valid attrs returns valid changeset" do
    changeset = State.changeset(%State{}, @valid_attrs)

    assert changeset.valid?
    assert %State{
      name: "Awesome #{__MODULE__}",
      color: "#663300",
      order_no: 5,
    } == apply_changes(changeset)
  end

  test "changeset/2 with invalid name returns error" do
    invalid_attrs = Map.put(@valid_attrs, "name", "    ")

    changeset = State.changeset(%State{}, invalid_attrs)

    refute changeset.valid?
    assert %{name: ["can't be blank"]} = errors_on(changeset)
  end

  test "changeset/2 with invalid color returns error" do
    invalid_attrs = Map.put(@valid_attrs, "color", "    ")

    changeset = State.changeset(%State{}, invalid_attrs)

    refute changeset.valid?
    assert %{color: ["can't be blank"]} = errors_on(changeset)
  end
end
