defmodule Ticketee.Projects.SearchTicketsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Projects
  alias Ticketee.TestFactory

  setup do
    project = TestFactory.insert(:project)
    {:ok, project: project}
  end

  describe "searching by project_id" do
    setup %{project: project} do
      other_project = TestFactory.insert(:project)

      ticket_a = TestFactory.insert(:ticket, project: project, title: "Ticket A #{__MODULE__}")
      ticket_b = TestFactory.insert(:ticket, project: other_project, title: "Ticket B #{__MODULE__}")
      ticket_c = TestFactory.insert(:ticket, project: project, title: "Ticket C #{__MODULE__}")
      ticket_d = TestFactory.insert(:ticket, project: other_project, title: "Ticket D #{__MODULE__}")

      {:ok, other_project: other_project,
            ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d}
    end

    test "returns matching tickets",
        %{project: project, other_project: other_project,
          ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d} do
      assert_records(Projects.search_tickets(%{project_id: project.id}), [ticket_a, ticket_c])
      assert_records(Projects.search_tickets(%{project_id: other_project.id}), [ticket_b, ticket_d])
    end

    test "returns matching tickets when searcing by permalink",
        %{project: project, other_project: other_project,
          ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d} do
      project_permalink = Phoenix.Param.to_param(project)
      other_permalink = "#{other_project.id}-other-project"

      assert_records(Projects.search_tickets(%{project_id: project_permalink}), [ticket_a, ticket_c])
      assert_records(Projects.search_tickets(%{project_id: other_permalink}), [ticket_b, ticket_d])
    end
  end

  describe "searching by ticket title" do
    test "returns matching tickets", %{project: project} do
      ticket_a = TestFactory.insert(:ticket, project: project, title: "Ticket A is Awesome #{__MODULE__}")
      ticket_b = TestFactory.insert(:ticket, project: project, title: "Ticket B is Neat #{__MODULE__}")
      ticket_c = TestFactory.insert(:ticket, project: project, title: "Ticket C is Awe inspiring #{__MODULE__}")
      ticket_d = TestFactory.insert(:ticket, project: project, title: "Ticket D is Helpful #{__MODULE__}")

      assert_records(Projects.search_tickets(%{ticket_title: "awe"}), [ticket_a, ticket_c])
      assert_records(Projects.search_tickets(%{ticket_title: "Helpful"}), [ticket_d])
      assert_records(Projects.search_tickets(%{ticket_title: "is neat"}), [ticket_b])
    end

    test "uses fulltext search", %{project: project} do
      ticket_a = TestFactory.insert(:ticket, project: project, title: "Blue Elephant #{__MODULE__}")
      ticket_b = TestFactory.insert(:ticket, project: project, title: "Blue Whale #{__MODULE__}")

      assert_records(Projects.search_tickets(%{ticket_title: "elephants"}), [ticket_a])
      assert_records(Projects.search_tickets(%{ticket_title: "whales"}), [ticket_b])
      assert_records(Projects.search_tickets(%{ticket_title: "blue"}), [ticket_a, ticket_b])
    end
  end

  describe "searching by state" do
    setup %{project: project} do
      state_opened = TestFactory.insert(:state, name: "Opened")
      state_closed = TestFactory.insert(:state, name: "Closed")

      ticket_a = TestFactory.insert(:ticket, project: project, title: "Ticket A #{__MODULE__}", state: state_opened)
      ticket_b = TestFactory.insert(:ticket, project: project, title: "Ticket B #{__MODULE__}", state: state_closed)
      ticket_c = TestFactory.insert(:ticket, project: project, title: "Ticket C #{__MODULE__}", state: state_closed)
      ticket_d = TestFactory.insert(:ticket, project: project, title: "Ticket D #{__MODULE__}", state: state_opened)

      {:ok, state_opened: state_opened,
            state_closed: state_closed,
            ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d}
    end

    test "returns matching tickets when searching by state_id",
        %{state_opened: state_opened, state_closed: state_closed,
          ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d} do
      assert_records(Projects.search_tickets(%{state_id: state_opened.id}), [ticket_a, ticket_d])
      assert_records(Projects.search_tickets(%{state_id: state_closed.id}), [ticket_b, ticket_c])
    end

    test "returns matching tickets when searching by state_name",
        %{ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c, ticket_d: ticket_d} do
      assert_records(Projects.search_tickets(%{state_name: "Opened"}), [ticket_a, ticket_d])
      assert_records(Projects.search_tickets(%{state_name: "Closed"}), [ticket_b, ticket_c])
    end
  end

  describe "searching by tag_name" do
    test "returns matching records", %{project: project} do
      tag_awesome = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      tag_important = TestFactory.insert(:tag, name: "Important #{__MODULE__}")

      ticket_a = TestFactory.insert(:ticket, project: project, title: "Ticket A #{__MODULE__}",
        tags: [tag_awesome])

      ticket_b = TestFactory.insert(:ticket, project: project, title: "Ticket B #{__MODULE__}",
        tags: [tag_awesome, tag_important])

      ticket_c = TestFactory.insert(:ticket, project: project, title: "Ticket C #{__MODULE__}",
        tags: [tag_important])

      # No tags
      _ticket_d = TestFactory.insert(:ticket, project: project, title: "Ticket D #{__MODULE__}")

      assert_records(Projects.search_tickets(%{tag_name: "Awesome #{__MODULE__}"}), [ticket_a, ticket_b])
      assert_records(Projects.search_tickets(%{tag_name: "Important #{__MODULE__}"}), [ticket_b, ticket_c])
      assert_records(Projects.search_tickets(%{tag_name: "Not Found #{__MODULE__}"}), [])
    end
  end

  defp assert_records(actual, expected) do
    assert pluck_id_and_title(actual) == pluck_id_and_title(expected)
  end

  # Asserting on `id` and `title` makes failure messages more helpful
  defp pluck_id_and_title(records) do
    records
    |> Enum.map(fn record -> {record.id, record.title} end)
    |> Enum.sort()
  end
end
