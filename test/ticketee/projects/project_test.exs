defmodule Ticketee.Projects.ProjectTest do
  use Ticketee.DataCase, async: true
  alias Ticketee.Projects.Project

  @valid_attrs %{
    "name" => "Sublime Text 3",
    "description" => "A text editor for everyone",
  }

  test "changeset/2 with valid attrs returns valid changeset" do
    changeset = Project.changeset(%Project{}, @valid_attrs)

    assert changeset.valid?
    assert %Project{
        name: "Sublime Text 3",
        description: "A text editor for everyone",
        # Assert slug for `name` has been generated
        slug: "sublime-text-3",
      } = apply_changes(changeset)
  end

  test "changeset/2 with invalid name returns error" do
    invalid_attrs = Map.put(@valid_attrs, "name", "    ")

    changeset = Project.changeset(%Project{}, invalid_attrs)

    refute changeset.valid?
    assert %{name: ["can't be blank"]} = errors_on(changeset)
  end

  test "changeset/2 with invalid description returns error" do
    invalid_attrs = Map.put(@valid_attrs, "description", "    ")

    changeset = Project.changeset(%Project{}, invalid_attrs)

    refute changeset.valid?
    assert %{description: ["can't be blank"]} = errors_on(changeset)
  end

  test "changeset/2 returns error when project name is too long" do
    long_name = String.duplicate("a", 256)
    invalid_attrs = Map.put(@valid_attrs, "name", long_name)

    changeset = Project.changeset(%Project{}, invalid_attrs)

    refute changeset.valid?
    assert %{name: ["should be at most 255 character(s)"]} = errors_on(changeset)
  end
end
