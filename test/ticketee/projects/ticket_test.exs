defmodule Ticketee.Projects.TicketTest do
  use Ticketee.DataCase, async: true
  alias Ticketee.Projects.Ticket

  @valid_attrs %{
    "title" => "Do the dishes",
    "description" => "Dishes should be washed after every meal",
  }

  test "changeset/2 with valid attrs returns valid changeset" do
    changeset = Ticket.changeset(%Ticket{}, @valid_attrs)

    assert changeset.valid?
    assert %Ticket{
        title: "Do the dishes",
        description: "Dishes should be washed after every meal",
      } = apply_changes(changeset)
  end

  test "changeset/2 with invalid title returns error" do
    invalid_attrs = Map.put(@valid_attrs, "title", "   ")

    changeset = Ticket.changeset(%Ticket{}, invalid_attrs)

    refute changeset.valid?
    assert %{title: ["can't be blank"]} = errors_on(changeset)
  end

  test "changeset/2 with invalid description returns error" do
    invalid_attrs = Map.put(@valid_attrs, "description", "   ")

    changeset = Ticket.changeset(%Ticket{}, invalid_attrs)

    refute changeset.valid?
    assert %{description: ["can't be blank"]} = errors_on(changeset)
  end

  test "changeset/2 returns error when ticket title is too long" do
    long_title = String.duplicate("a", 256)
    invalid_attrs = Map.put(@valid_attrs, "title", long_title)

    changeset = Ticket.changeset(%Ticket{}, invalid_attrs)

    refute changeset.valid?
    assert %{title: ["should be at most 255 character(s)"]} = errors_on(changeset)
  end
end
