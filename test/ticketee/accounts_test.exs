defmodule Ticketee.AccountsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Accounts
  alias Ticketee.Accounts.User
  alias Ticketee.TestFactory
  alias Ticketee.Passwords

  describe "list_users/0" do
    @user_a_email "user_a@#{__MODULE__}.com"
    @user_b_email "user_b@#{__MODULE__}.com"

    setup do
      user_a = TestFactory.insert(:user, email: @user_a_email)
      user_b = TestFactory.insert(:user, email: @user_b_email)

      {:ok, users: [user_a, user_b]}
    end

    test "lists all users in the database" do
      assert [
          %User{email: first_email},
          %User{email: second_email},
        ] = Accounts.list_users()

      assert Enum.sort([first_email, second_email]) == [@user_a_email, @user_b_email]
    end
  end

  @valid_user_attrs %{
    "email" => "test_user@#{__MODULE__}.com",
  }

  describe "create_user/1" do
    test "with valid attributes creates new user" do
      assert {:ok, user} = Accounts.create_user(@valid_user_attrs)

      assert user.email == "test_user@#{__MODULE__}.com"
    end

    test "with invalid attributes returns errors" do
      invalid_attrs = Map.put(@valid_user_attrs, "email", "   ") # blank email

      assert {:error, changeset} = Accounts.create_user(invalid_attrs)

      assert %{email: ["can't be blank"]} = errors_on(changeset)
    end

    test "when email is already taken returns errors" do
      assert {:ok, _existing_user} = Accounts.create_user(@valid_user_attrs)

      assert {:error, changeset} = Accounts.create_user(@valid_user_attrs)

      assert %{email: ["has already been taken"]} = errors_on(changeset)
    end
  end

  @test_password "super secret"

  @valid_registration_attrs Map.merge(@valid_user_attrs, %{
    "password" => @test_password,
    "password_confirmation" => @test_password,
  })

  describe "register_user/1" do
    test "with valid attributes creates new user" do
      assert {:ok, user} = Accounts.register_user(@valid_registration_attrs)

      assert user.email == "test_user@#{__MODULE__}.com"
      assert Passwords.verify(@test_password, user.password_hash)
    end

    test "with invalid attributes returns errors" do
      invalid_attrs = Map.put(@valid_registration_attrs, "password", "   ") # blank password

      assert {:error, changeset} = Accounts.register_user(invalid_attrs)

      assert %{password: ["can't be blank"]} = errors_on(changeset)
    end

    test "when email is already taken returns errors" do
      assert {:ok, _existing_user} = Accounts.register_user(@valid_registration_attrs)

      assert {:error, changeset} = Accounts.register_user(@valid_registration_attrs)

      assert %{email: ["has already been taken"]} = errors_on(changeset)
    end
  end

  describe "update_user_account/2" do
    @update_account_attrs %{
      "email" => "updated_user@#{__MODULE__}.com",
      "password" => "updated password",
      "password_confirmation" => "updated password",
    }

    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "with valid attributes updates user", %{user: user} do
      assert {:ok, user} = Accounts.update_user_account(user, @update_account_attrs)

      assert user.email == "updated_user@#{__MODULE__}.com"
      assert Passwords.verify("updated password", user.password_hash)
    end

    test "with invalid attributes returns errors", %{user: user} do
      invalid_attrs = Map.put(@update_account_attrs, "email", "   ") # blank email

      assert {:error, changeset} =  Accounts.update_user_account(user, invalid_attrs)

      assert %{email: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "authenticate/2" do
    setup do
      {:ok, user} = Accounts.register_user(@valid_registration_attrs)
      {:ok, user: user}
    end

    test "with valid email and password returns user", %{user: user} do
      assert {:ok, returned_user} = Accounts.authenticate(user.email, @test_password)

      assert user == returned_user
    end

    test "with invalid email returns error" do
      assert {:error, :not_found} = Accounts.authenticate("invalid@example.com", @test_password)
    end

    test "with invalid password returns error", %{user: user} do
      assert {:error, :unauthorized} = Accounts.authenticate(user.email, "wrong password")
    end
  end

  describe "archive_user/1" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "sets archived_at on user record", %{user: user} do
      # Assert precondition
      refute User.archived?(user)

      assert {:ok, user} = Accounts.archive_user(user)

      assert User.archived?(user)
    end

    test "when record already archived returns same record", %{user: user} do
      assert {:ok, archived_user} = Accounts.archive_user(user)

      assert {:ok, archived_twice} = Accounts.archive_user(archived_user)

      assert archived_user == archived_twice
    end
  end

  describe "restore_archived_user/1" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "restores user to non-archived state", %{user: user} do
      # Assert precondition
      assert {:ok, user} = Accounts.archive_user(user)
      assert User.archived?(user)

      assert {:ok, user} = Accounts.restore_archived_user(user)

      refute User.archived?(user)
    end

    test "returns non-archived user unchanged", %{user: user} do
      # Assert precondition
      refute User.archived?(user)

      assert {:ok, restored_user} = Accounts.restore_archived_user(user)
      assert restored_user == user
    end
  end
end
