defmodule Ticketee.TagsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Tags

  describe "list_tags/0" do
    test "lists tags alphabetically" do
      TestFactory.insert(:tag, name: "Dragons #{__MODULE__}")
      TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      TestFactory.insert(:tag, name: "There Be #{__MODULE__}")

      assert [tag1, tag2, tag3] = Tags.list_tags()
      assert tag1.name == "Awesome #{__MODULE__}"
      assert tag2.name == "Dragons #{__MODULE__}"
      assert tag3.name == "There Be #{__MODULE__}"
    end
  end

  describe "create_tags/1" do
    test "creates list of tags by name" do
      assert [tag1, tag2, tag3] = Tags.create_tags([
        "Awesome #{__MODULE__}",
        "Dragons #{__MODULE__}",
        "There Be #{__MODULE__}",
      ])

      assert tag1.name == "Awesome #{__MODULE__}"
      assert tag2.name == "Dragons #{__MODULE__}"
      assert tag3.name == "There Be #{__MODULE__}"
    end

    test "when some tags already exist returns them" do
      existing_tag_a = TestFactory.insert(:tag, name: "Dragons #{__MODULE__}")
      existing_tag_b = TestFactory.insert(:tag, name: "There Be #{__MODULE__}")

      assert [tag1, tag2, tag3] = Tags.create_tags([
        "Awesome #{__MODULE__}",
        "Dragons #{__MODULE__}",
        "There Be #{__MODULE__}",
      ])

      assert tag1.name == "Awesome #{__MODULE__}"
      assert tag2 == existing_tag_a
      assert tag3 == existing_tag_b
    end

    test "with duplicate tag names returns tags without duplicates" do
      assert [tag1, tag2] = Tags.create_tags([
        "Awesome #{__MODULE__}",
        "Dragons #{__MODULE__}",
        "Awesome #{__MODULE__}",
        "Dragons #{__MODULE__}",
        "Dragons #{__MODULE__}",
      ])

      assert tag1.name == "Awesome #{__MODULE__}"
      assert tag2.name == "Dragons #{__MODULE__}"
    end

    test "removes empty tag names and strips whitespace" do
      assert [tag1, tag2] = Tags.create_tags([
        "             ",
        "Awesome #{__MODULE__}        ",
        " ",
        "           Dragons #{__MODULE__}  ",
        "",
      ])

      assert tag1.name == "Awesome #{__MODULE__}"
      assert tag2.name == "Dragons #{__MODULE__}"
    end
  end

  @valid_tag_attrs %{
    "name" => "Awesome #{__MODULE__}",
  }

  describe "create_tag/1" do
    test "with valid attributes creates new tag" do
      assert {:ok, tag} = Tags.create_tag(@valid_tag_attrs)

      assert tag.name == "Awesome #{__MODULE__}"
    end

    test "with invalid attributes returns errors" do
      invalid_attrs = Map.put(@valid_tag_attrs, "name", "    ") # blank name

      assert {:error, changeset} = Tags.create_tag(invalid_attrs)

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end

    test "when name is already taken returns errors" do
      assert {:ok, _existing_tag} = Tags.create_tag(@valid_tag_attrs)

      assert {:error, changeset} = Tags.create_tag(@valid_tag_attrs)

      assert %{name: ["has already been taken"]} = errors_on(changeset)
    end
  end

  describe "update_tag/2" do
    @update_tag_attrs %{
      "name" => "Updated Awesome #{__MODULE__}",
    }

    setup do
      tag = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      {:ok, tag: tag}
    end

    test "with valid attributes updates tag", %{tag: tag} do
      assert {:ok, tag} = Tags.update_tag(tag, @update_tag_attrs)

      assert tag.name == "Updated Awesome #{__MODULE__}"
    end

    test "with invalid attributes returns errors", %{tag: tag} do
      invalid_attrs = Map.put(@update_tag_attrs, "name", "    ") # blank name

      assert {:error, changeset} = Tags.update_tag(tag, invalid_attrs)

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "delete_tag/1" do
    setup do
      tag = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      {:ok, tag: tag}
    end

    test "removes tag", %{tag: tag} do
      assert {:ok, _tag} = Tags.delete_tag(tag)

      assert [] == Tags.list_tags()
    end
  end
end
