defmodule Ticketee.AttachmentsTest do
  use Ticketee.DataCase, async: true
  import Ticketee.TestFixtureFiles, only: [fixture_file: 1, fixture_file_upload: 1]

  alias Ticketee.Attachments
  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.TestFactory

  ## Ticket Attachments

  describe "get_ticket_attachment/2" do
    setup :create_test_project

    test "returns correct attachment based on filename", %{project: project} do
      ticket = create_ticket(project, "Test ticket")
      attachment_speed = create_attachment(ticket, "speed.txt")
      attachment_gradient = create_attachment(ticket, "gradient.txt")

      assert Attachments.get_ticket_attachment(ticket, "speed.txt") == attachment_speed
      assert Attachments.get_ticket_attachment(ticket, "gradient.txt") == attachment_gradient
      assert Attachments.get_ticket_attachment(ticket, "missing.txt") == nil
    end
  end

  describe "create_ticket_attachment/2" do
    setup :create_test_project

    setup %{project: project} do
      ticket = create_ticket(project, "Test ticket")
      {:ok, ticket: ticket}
    end

    @test_filename "Speed is neat.TXT"
    @test_fixture_upload fixture_file_upload(@test_filename)
    @valid_attachment_attrs %{
        "file" => @test_fixture_upload,
        "description" => "A useful file about the speed attribute",
      }

    test "with valid attributes creates attachment", %{ticket: ticket} do
      assert {:ok, attachment} = Attachments.create_ticket_attachment(ticket, @valid_attachment_attrs)

      assert attachment.file == "speed_is_neat.txt"
      assert attachment.size == File.stat!(fixture_file(@test_filename)).size
      assert attachment.description == "A useful file about the speed attribute"

      contents = File.read!(fixture_file(@test_filename))
      assert File.read!(TicketAttachment.attachment_path(attachment)) == contents
    end

    test "when filename already taken returns error", %{ticket: ticket} do
      assert {:ok, _attachment} = Attachments.create_ticket_attachment(ticket, @valid_attachment_attrs)

      assert {:error, changeset} = Attachments.create_ticket_attachment(ticket, @valid_attachment_attrs)

      error_message = "filename must be unique for each one of this ticket's attachments"
      assert %{file: [^error_message]} = errors_on(changeset)
    end

    test "when file not sent returns error", %{ticket: ticket} do
      invalid_attrs = Map.delete(@valid_attachment_attrs, "file")

      assert {:error, changeset} = Attachments.create_ticket_attachment(ticket, invalid_attrs)

      assert %{file: ["can't be blank"]} = errors_on(changeset)
    end

    test "when file has invalid extension", %{ticket: ticket} do
      invalid_file = fixture_file_upload("something.invalid")
      invalid_attrs = Map.put(@valid_attachment_attrs, "file", invalid_file)

      assert {:error, changeset} = Attachments.create_ticket_attachment(ticket, invalid_attrs)

      assert %{file: ["invalid file extension"]} = errors_on(changeset)
    end
  end

  describe "delete_ticket_attachment/1" do
    setup :create_test_project

    setup %{project: project} do
      ticket = create_ticket(project, "Test ticket")
      attachment = create_attachment(ticket, "speed.txt")

      {:ok, ticket: ticket, attachment: attachment}
    end

    test "deletes attachment and file", %{ticket: ticket, attachment: attachment} do
      # Assert Preconditions
      assert Attachments.get_ticket_attachment(ticket, attachment.file)
      assert File.exists?(TicketAttachment.attachment_path(attachment))

      assert {:ok, deleted_attachment} = Attachments.delete_ticket_attachment(attachment)

      assert deleted_attachment.id == attachment.id
      refute Attachments.get_ticket_attachment(ticket, attachment.file)
      refute File.exists?(TicketAttachment.attachment_path(attachment))
    end

    @tag capture_log: true
    test "when file already deleted returns error", %{attachment: attachment} do
      File.rm(TicketAttachment.attachment_path(attachment))

      assert {:error, :enoent} = Attachments.delete_ticket_attachment(attachment)
    end

    ## Helpers

    def create_test_project(_context) do
      project = TestFactory.insert(:project, name: "Some fancy new project #{__MODULE__}")
      {:ok, project: project}
    end

    def create_ticket(project, title) do
      TestFactory.insert(:ticket, %{
        project_id: project.id,
        title: title,
        description: "Some neat ticket description for #{title}",
      })
    end

    def create_attachment(ticket, file) when is_binary(file) do
      upload = fixture_file_upload(file)
      assert {:ok, attachment} = Attachments.create_ticket_attachment(ticket, %{"file" => upload})

      attachment
    end

    def create_test_user(_context) do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end
  end
end
