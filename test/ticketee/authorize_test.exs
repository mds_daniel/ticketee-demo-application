defmodule Ticketee.AuthorizeTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Authorize
  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Roles

  test "without user does not allow any action" do
    refute Authorize.allowed?(nil, :any_action, :any_resource)
  end

  test "when regular user does not allow any action" do
    user = TestFactory.build(:user)

    refute Authorize.allowed?(user, :any_action, :any_resource)
  end

  test "when admin user allows any action" do
    user = TestFactory.build(:user)
    admin = %{user | admin: true}

    assert Authorize.allowed?(admin, :any_action, :any_resource)
  end

  test "when archived does not allow any action" do
    user = TestFactory.build(:user)
    archived_user = %{user | archived_at: DateTime.utc_now()}
    archived_admin = %{user | admin: true, archived_at: DateTime.utc_now()}

    refute Authorize.allowed?(archived_user, :any_action, :any_resource)
    refute Authorize.allowed?(archived_admin, :any_action, :any_resource)
  end

  describe "list_projects_for/1" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      project_a = TestFactory.insert(:project, name: "Project Manhattan #{__MODULE__}")
      project_b = TestFactory.insert(:project, name: "Project Orion #{__MODULE__}")

      {:ok, user: user,
            project_a: project_a,
            project_b: project_b}
    end

    test "with no roles assigned returns no projects", %{user: user} do
      assert Authorize.list_projects_for(user) == []
    end

    test "when admin returns all projects", %{user: user,
                                              project_a: project_a,
                                              project_b: project_b} do
      {:ok, admin} = Accounts.promote_to_admin(user)

      assert Authorize.list_projects_for(admin) == [project_a, project_b]
    end

    test "with role assigned returns matching projects", %{user: user,
                                                           project_a: project_a,
                                                           project_b: project_b} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project_a)

      other_user = TestFactory.insert(:user)
      {:ok, _role} = Roles.grant_role(other_user, :viewer, project_b)

      assert Authorize.list_projects_for(user) == [project_a]
      assert Authorize.list_projects_for(other_user) == [project_b]
    end
  end

  describe "search_tickets_for/2" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")

      project_a = TestFactory.insert(:project)
      ticket_a = TestFactory.insert(:ticket, project_id: project_a.id, title: "Ticket A #{__MODULE__}")

      project_b = TestFactory.insert(:project)
      ticket_b = TestFactory.insert(:ticket, project_id: project_b.id, title: "Ticket B #{__MODULE__}")

      {:ok, user: user,
            project_a: project_a,
            project_b: project_b,
            ticket_a: ticket_a,
            ticket_b: ticket_b}
    end

    test "with no roles assigned returns no tickets", %{user: user} do
      assert Authorize.search_tickets_for(user) == []
    end

    test "when admin returns all tickets", %{user: user,
                                             ticket_a: ticket_a,
                                             ticket_b: ticket_b} do
      {:ok, admin} = Accounts.promote_to_admin(user)

      assert Authorize.search_tickets_for(admin) == [ticket_a, ticket_b]
    end

    test "with role assigned returns matching tickets", %{user: user,
                                                          project_a: project_a,
                                                          project_b: project_b,
                                                          ticket_a: ticket_a,
                                                          ticket_b: ticket_b} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project_a)

      other_user = TestFactory.insert(:user)
      {:ok, _role} = Roles.grant_role(other_user, :viewer, project_b)

      assert Authorize.search_tickets_for(user) == [ticket_a]
      assert Authorize.search_tickets_for(other_user) == [ticket_b]
    end
  end
end
