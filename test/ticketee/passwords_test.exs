defmodule Ticketee.PasswordsTest do
  use ExUnit.Case, async: true
  alias Ticketee.Passwords

  test "hash/1 hashes password using argon algorithm" do
    assert Passwords.hash("super secret") =~ "$argon2id"
  end

  describe "verify/2" do
    @password "super secret"

    test "when password matches returns true" do
      digest = Passwords.hash(@password)

      assert Passwords.verify(@password, digest)
    end

    test "when password does not match returns false" do
      digest = Passwords.hash(@password)

      refute Passwords.verify("wrong password", digest)
    end

    test "when digest is nil returns false" do
      refute Passwords.verify(@password, nil)
    end
  end
end
