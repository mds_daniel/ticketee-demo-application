defmodule Ticketee.ProjectsTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.Projects
  alias Ticketee.Projects.{Project, Ticket, State}
  alias Ticketee.TestFactory

  describe "list_projects/0" do
    setup do
      project_a = create_project("Project A")
      project_b = create_project("Project B")

      {:ok, projects: [project_a, project_b]}
    end

    test "lists all projects in the database" do
      assert [
          %Project{name: "Project A"},
          %Project{name: "Project B"},
        ]= Projects.list_projects()
    end
  end

  describe "fetch_project/1" do
    test "returns project with requested id" do
      project = create_project("Manhattan #{__MODULE__}")
      project_id = project.id

      assert {:ok, %Project{id: ^project_id}} = Projects.fetch_project(project_id)
    end

    test "returns error when project not found" do
      assert {:error, :not_found} = Projects.fetch_project(0)
    end

    test "returns error when project id is invalid" do
      assert {:error, :invalid} = Projects.fetch_project("dragons")
    end
  end

  @valid_project_attrs %{
    "name" => "Sublime Text 3 #{__MODULE__}",
    "description" => "A text editor for everyone",
  }

  describe "create_project/1" do
    test "with valid attributes creates new project" do
      assert {:ok, project} = Projects.create_project(@valid_project_attrs)

      assert project.name == "Sublime Text 3 #{__MODULE__}"
      assert project.description == "A text editor for everyone"
    end

    test "with invalid attributes returns errors" do
      invalid_attrs = Map.put(@valid_project_attrs, "name", "   ")

      assert {:error, changeset} = Projects.create_project(invalid_attrs)

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end

    test "when name is already taken returns errors" do
      assert {:ok, _existing_project} = Projects.create_project(@valid_project_attrs)

      assert {:error, changeset} = Projects.create_project(@valid_project_attrs)

      assert %{name: ["has already been taken"]} = errors_on(changeset)
    end

    ## Edge cases

    test "allows saving a project with a really long description" do
      long_text = String.duplicate("a", 2048)
      attrs = Map.put(@valid_project_attrs, "description", long_text)

      assert {:ok, project} = Projects.create_project(attrs)

      assert project.description == long_text
    end
  end

  describe "update_project/2" do
    setup :create_test_project

    test "with valid attributes updates new project", %{project: project} do
      updated_name = "Some updated fancy project #{__MODULE__}"

      assert {:ok, project} = Projects.update_project(project, %{
        "name" => updated_name,
        "description" => "Brand new updated description",
      })

      assert project.name == updated_name
      assert project.description ==  "Brand new updated description"

      # Test name slug has been updated
      assert project.slug == Ticketee.Permalink.slugify(updated_name)
    end

    test "with invalid attributes returns errors", %{project: project} do
      assert {:error, changeset} = Projects.update_project(project, %{
        "name" => "      ", # name is blank
        "description" => "Brand new updated description",
      })

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end
  end

  @valid_ticket_attrs %{
    "title" => "Do the dishes #{__MODULE__}",
    "description" => "Dishes should be washed after every meal",
  }

  describe "create_ticket_for/3" do
    setup :create_test_user
    setup :create_test_project

    test "with valid attributes creates new ticket for user", %{project: project, user: user} do
      assert {:ok, ticket} = Projects.create_ticket_for(user, project, @valid_ticket_attrs)

      assert ticket.title == "Do the dishes #{__MODULE__}"
      assert ticket.description ==  "Dishes should be washed after every meal"
      assert ticket.project_id == project.id

      ticket = Projects.with_ticket_author(ticket)
      assert ticket.author == user
    end

    test "with invalid attributes returns errors", %{project: project, user: user} do
      invalid_attrs = Map.put(@valid_ticket_attrs, "title", "   ")

      assert {:error, changeset} = Projects.create_ticket_for(user, project, invalid_attrs)

      assert %{title: ["can't be blank"]} == errors_on(changeset)
    end
  end

  describe "create_ticket_with_default_state_for/3" do
    setup :create_test_user
    setup :create_test_project

    setup do
      state = TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true)
      {:ok, state: state}
    end

    test "with valid attributes creates new ticket with default state", %{project: project, user: user, state: state} do
      assert {:ok, ticket} = Projects.create_ticket_with_default_state_for(user, project, @valid_ticket_attrs)

      assert ticket.title == "Do the dishes #{__MODULE__}"

      # Assert default state
      ticket = Projects.with_state(ticket)
      assert ticket.state == state
    end
  end

  describe "create_ticket/2" do
    setup :create_test_project

    test "with valid attributes creates new ticket", %{project: project} do
      assert {:ok, ticket} = Projects.create_ticket(project, @valid_ticket_attrs)

      assert ticket.title == "Do the dishes #{__MODULE__}"
      assert ticket.description ==  "Dishes should be washed after every meal"
      assert ticket.project_id == project.id
    end

    test "with invalid attributes returns errors", %{project: project} do
      invalid_attrs = Map.put(@valid_ticket_attrs, "title", "   ")

      assert {:error, changeset} = Projects.create_ticket(project, invalid_attrs)

      assert %{title: ["can't be blank"]} == errors_on(changeset)
    end

    ## Edge cases

    test "when associated project does not exist anymore", %{project: project} do
      assert {:ok, _} = Projects.delete_project(project)

      assert {:error, changeset} = Projects.create_ticket(project, @valid_ticket_attrs)

      assert %{project: ["does not exist"]} = errors_on(changeset)
    end

    test "allows saving a ticket with a really long description", %{project: project} do
      long_text = String.duplicate("a", 2048)
      attrs = Map.put(@valid_ticket_attrs, "description", long_text)

      assert {:ok, ticket} = Projects.create_ticket(project, attrs)

      assert ticket.description == long_text
    end
  end

  describe "update_ticket/2" do
    setup :create_test_project

    setup %{project: project} do
      ticket = create_ticket(project, "Test ticket #{__MODULE__}")
      {:ok, ticket: ticket}
    end

    @update_ticket_params %{
      "title" => "Updated ticket title #{__MODULE__}",
      "description" => "Updated ticket description #{__MODULE__}",
    }

    test "with valid attributes updates ticket", %{ticket: ticket} do
      assert {:ok, ticket} = Projects.update_ticket(ticket, @update_ticket_params)

      assert ticket.title == "Updated ticket title #{__MODULE__}"
      assert ticket.description == "Updated ticket description #{__MODULE__}"
    end

    test "with invalid attributes returns errors", %{ticket: ticket} do
      invalid_attrs = Map.put(@update_ticket_params, "title", "   ") # blank title

      assert {:error, changeset} = Projects.update_ticket(ticket, invalid_attrs)

      assert %{title: ["can't be blank"]} == errors_on(changeset)
    end
  end

  ## Comments

  @valid_comment_attrs %{
    "description" => "A nice comment about the current topic",
  }

  describe "create_comment_for/3" do
    setup :create_test_project
    setup :create_test_user

    setup %{project: project} do
      ticket = create_ticket(project, "Test ticket for #{__MODULE__}")
      {:ok, ticket: ticket}
    end

    test "with valid attributes creates new comment", %{user: user, ticket: ticket} do
      assert {:ok, comment} = Projects.create_comment_for(user, ticket, @valid_comment_attrs)

      assert comment.description == "A nice comment about the current topic"
      assert comment.ticket_id == ticket.id
      assert comment.user_id == user.id
    end

    test "with invalid attributes returns errors", %{user: user, ticket: ticket} do
      invalid_attrs = Map.put(@valid_comment_attrs, "description", "    ") # blank description

      assert {:error, changeset} = Projects.create_comment_for(user, ticket, invalid_attrs)

      assert %{description: ["can't be blank"]} = errors_on(changeset)
    end

    test "updates ticket state when comment state changes", %{user: user, ticket: ticket} do
      open_state = TestFactory.insert(:state, name: "Open #{__MODULE__}")
      in_progress_state = TestFactory.insert(:state, name: "In Progress #{__MODULE__}")

      # Switch state to "Open"
      comment_attrs = Map.put(@valid_comment_attrs, "state_id", open_state.id)
      assert {:ok, first_comment} = Projects.create_comment_for(user, ticket, comment_attrs)

      assert first_comment.previous_state_id == nil
      assert first_comment.state_id == open_state.id

      ticket = Projects.get_ticket(ticket.id)
      assert ticket.state_id == open_state.id

      # Switch state to "In progress"
      comment_attrs = Map.put(@valid_comment_attrs, "state_id", in_progress_state.id)
      assert {:ok, second_comment} = Projects.create_comment_for(user, ticket, comment_attrs)

      assert second_comment.previous_state_id == open_state.id
      assert second_comment.state_id == in_progress_state.id

      ticket = Projects.get_ticket(ticket.id)
      assert ticket.state_id == in_progress_state.id
    end

    test "maintains comment_count", %{user: user, ticket: ticket} do
      # Assert precondition
      assert %Ticket{comment_count: 0} = ticket

      # Create first comment
      assert {:ok, _comment} = Projects.create_comment_for(user, ticket, @valid_comment_attrs)
      assert %Ticket{comment_count: 1} = Projects.get_ticket(ticket.id)

      # Create second comment
      assert {:ok, comment} = Projects.create_comment_for(user, ticket, @valid_comment_attrs)
      assert %Ticket{comment_count: 2} = Projects.get_ticket(ticket.id)

      # Delete second comment
      assert {:ok, _deleted_comment} = Projects.delete_comment(comment)
      assert %Ticket{comment_count: 1} = Projects.get_ticket(ticket.id)
    end
  end

  describe "list_ticket_comments/1" do
    setup :create_test_project

    test "returns comments in order they were inserted", %{project: project} do
      ticket = create_ticket(project, "Test ticket for #{__MODULE__}")
      comment_c = TestFactory.insert(:comment, ticket_id: ticket.id, description: "Comment C")
      comment_a = TestFactory.insert(:comment, ticket_id: ticket.id, description: "Comment A")
      comment_b = TestFactory.insert(:comment, ticket_id: ticket.id, description: "Comment B")

      assert Projects.list_ticket_comments(ticket) == [comment_c, comment_a, comment_b]
    end
  end

  ## States

  describe "list_states/0" do
    test "returns list of all available states sorted by order_no" do
      TestFactory.insert(:state, name: "Awesome", order_no: 2)
      TestFactory.insert(:state, name: "Open", order_no: 0)
      TestFactory.insert(:state, name: "Closed", order_no: 3)
      TestFactory.insert(:state, name: "In Progress", order_no: 1)

      assert [
        %State{name: "Open"},
        %State{name: "In Progress"},
        %State{name: "Awesome"},
        %State{name: "Closed"},
      ] = Projects.list_states()
    end
  end

  describe "get_state_by/1" do
    test "returns state with given attrs" do
      TestFactory.insert(:state, name: "Open", order_no: 0)
      TestFactory.insert(:state, name: "Awesome", order_no: 2)

      assert %State{name: "Open"} = Projects.get_state_by(name: "Open")
      assert %State{name: "Awesome"} = Projects.get_state_by(order_no: 2)
      refute Projects.get_state_by(name: "Open", order_no: 2)
    end
  end

  @valid_state_attrs %{
    "name" => "Awesome #{__MODULE__}",
    "color" => "#663300",
    "order_no" => 2,
  }

  describe "create_state/1" do
    test "with valid attrs creates new state" do
      assert {:ok, state} = Projects.create_state(@valid_state_attrs)

      assert state.name == "Awesome #{__MODULE__}"
      assert state.color == "#663300"
      assert state.order_no == 2
    end

    test "with invalid attrs returns errors" do
      invalid_attrs = Map.put(@valid_state_attrs, "name", "    ") # blank name

      assert {:error, changeset} = Projects.create_state(invalid_attrs)

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end

    test "with duplicate name returns errors" do
      assert {:ok, _existing_state} = Projects.create_state(@valid_state_attrs)

      assert {:error, changeset} = Projects.create_state(@valid_state_attrs)

      assert %{name: ["has already been taken"]} = errors_on(changeset)
    end
  end

  describe "update_state/1" do
    setup do
      state = TestFactory.insert(:state)
      {:ok, state: state}
    end

    @update_state_attrs %{
      "name" => "Updated Awesome",
      "color" => "orange",
      "order_no" => 42,
    }

    test "with valid attrs updates state", %{state: state} do
      assert {:ok, state} = Projects.update_state(state, @update_state_attrs)

      assert state.name == "Updated Awesome"
      assert state.color == "orange"
      assert state.order_no == 42
    end

    test "with invalid attrs returns errors", %{state: state} do
      invalid_attrs = Map.put(@update_state_attrs, "name", "    ") # blank name

      assert {:error, changeset} = Projects.update_state(state, invalid_attrs)

      assert %{name: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "delete_state/1" do
    setup do
      state = TestFactory.insert(:state)
      {:ok, state: state}
    end

    test "removes state from db", %{state: state} do
      assert {:ok, deleted_state} = Projects.delete_state(state)

      assert deleted_state.id == state.id
      refute Projects.get_state_by(name: deleted_state.name)
    end

    test "when tickets are assigned to state restricts deletion", %{state: state} do
      project = TestFactory.insert(:project)
      TestFactory.insert(:ticket, project: project, state: state)

      assert {:error, changeset} = Projects.delete_state(state)

      assert %{name: ["cannot delete while tickets have this state"]} == errors_on(changeset)
    end

    test "when comments are assigned to state restricts deletion", %{state: state} do
      project = TestFactory.insert(:project)
      ticket = TestFactory.insert(:ticket, project: project)
      TestFactory.insert(:comment, ticket: ticket, previous_state: state)

      assert {:error, changeset} = Projects.delete_state(state)

      assert %{name: ["cannot delete while comments have this previous state"]} == errors_on(changeset)
    end
  end

  describe "set_default_state/1" do
    test "sets state as default and all other states to default false" do
      state_new = TestFactory.insert(:state, name: "New #{__MODULE__}")
      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}")
      state_closed = TestFactory.insert(:state, name: "Closed #{__MODULE__}", default: true)

      {:ok, state_new} = Projects.set_default_state(state_new)
      assert state_new.default

      # "Closed" state set to default false
      state_closed = Projects.get_state(state_closed.id)
      refute state_closed.default

      {:ok, state_open} = Projects.set_default_state(state_open)
      assert state_open.default

      # "New" state set to default false
      state_new = Projects.get_state(state_new.id)
      refute state_new.default

      # "Open" remains current default
      state_open = Projects.get_state(state_open.id)
      assert state_open.default
    end
  end

  ## Helpers

  def create_test_project(_context) do
    project = create_project("Some fancy new project #{__MODULE__}")
    {:ok, project: project}
  end

  def create_project(name) do
    assert {:ok, project} = Projects.create_project(%{
      "name" => name,
      "description" => "Some neat description for #{name}",
    })

    project
  end

  def create_ticket(project, title) do
    assert {:ok, ticket} = Projects.create_ticket(project, %{
      "title" => title,
      "description" => "Some neat ticket description for #{title}",
    })

    ticket
  end

  def create_test_user(_context) do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, user: user}
  end
end
