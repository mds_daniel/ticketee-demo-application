defmodule Ticketee.Roles.RoleTest do
  use Ticketee.DataCase, async: true
  alias Ticketee.Roles.Role

  @valid_attrs %{
    "role" => "manager",
  }

  test "changeset/2 with valid attrs returns valid changeset" do
    changeset = Role.changeset(%Role{}, @valid_attrs)

    assert changeset.valid?
    assert %Role{
        role: :manager
      } = apply_changes(changeset)
  end

  test "changeset/2 with invalid roles returns error" do
    invalid_attrs = Map.put(@valid_attrs, "role", "invalid")

    changeset = Role.changeset(%Role{}, invalid_attrs)

    refute changeset.valid?
    assert %{role: ["is invalid"]} = errors_on(changeset)
  end
end
