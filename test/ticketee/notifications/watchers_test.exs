defmodule Ticketee.Notifications.WatchersTest do
  use Ticketee.DataCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Notifications.Watchers
  alias Ticketee.Accounts.User
  alias Ticketee.Projects.Ticket

  setup do
    user = TestFactory.insert(:user, email: "user_a@#{__MODULE__}.com")
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, user: user, ticket: ticket, project: project}
  end

  describe "create_ticket_watcher/2" do
    test "adds watcher", %{user: user, ticket: ticket} do
      assert {:ok, watcher} = Watchers.create_ticket_watcher(ticket, user)
      assert watcher.ticket_id == ticket.id
      assert watcher.user_id == user.id
    end

    test "when watcher already exists returns error", %{user: user, ticket: ticket} do
      assert {:ok, _existing_watcher} = Watchers.create_ticket_watcher(ticket, user)

      assert {:error, changeset} = Watchers.create_ticket_watcher(ticket, user)
      assert %{ticket_id: ["is already beeing watched"]} = errors_on(changeset)
    end

    test "when watcher already exists returns success if on_conflict set to :nothing",
        %{user: user, ticket: ticket} do
      assert {:ok, _existing_watcher} = Watchers.create_ticket_watcher(ticket, user)

      assert {:ok, watcher} = Watchers.create_ticket_watcher(ticket, user, :nothing)

      # for non-inserted watcher records `id` field will be nil
      assert watcher.id == nil
    end

    test "when user does not exist returns error", %{ticket: ticket} do
      non_existent_user = %User{id: Ecto.UUID.generate()}

      assert {:error, changeset} = Watchers.create_ticket_watcher(ticket, non_existent_user)
      assert %{user: ["does not exist"]} = errors_on(changeset)
    end

    test "when ticket does not exist returns error", %{user: user} do
      non_existent_ticket = %Ticket{id: 0}

      assert {:error, changeset} = Watchers.create_ticket_watcher(non_existent_ticket, user)
      assert %{ticket: ["does not exist"]} = errors_on(changeset)
    end

    test "when user_id or ticket_id is missing returns error", %{user: user, ticket: ticket} do
      assert {:error, changeset} = Watchers.create_ticket_watcher(ticket, %User{id: nil})
      assert %{user_id: ["can't be blank"]} = errors_on(changeset)

      assert {:error, changeset} = Watchers.create_ticket_watcher(%Ticket{id: nil}, user)
      assert %{ticket_id: ["can't be blank"]} = errors_on(changeset)
    end
  end

  describe "get_ticket_watcher" do
    setup %{ticket: ticket, user: user} do
      {:ok, watcher} = Watchers.create_ticket_watcher(ticket, user)
      {:ok, watcher: watcher}
    end

    test "returns ticket watcher", %{ticket: ticket, user: user, watcher: watcher} do
      assert Watchers.get_ticket_watcher(ticket, user) == watcher
    end

    test "when no watcher present returns nil", %{user: user, project: project} do
      other_ticket = TestFactory.insert(:ticket, project: project)

      assert Watchers.get_ticket_watcher(other_ticket, user) == nil
    end
  end

  describe "delete_ticket_watcher/2" do
    setup %{ticket: ticket, user: user} do
      {:ok, watcher} = Watchers.create_ticket_watcher(ticket, user)
      {:ok, watcher: watcher}
    end

    test "deletes watcher", %{ticket: ticket, user: user} do
      assert {:ok, watcher} = Watchers.delete_ticket_watcher(ticket, user)
      assert watcher.ticket_id == ticket.id
      assert watcher.user_id == user.id
    end

    test "when already deleted returns error", %{ticket: ticket, user: user} do
      assert {:ok, _watcher} = Watchers.delete_ticket_watcher(ticket, user)

      assert {:error, :not_found} == Watchers.delete_ticket_watcher(ticket, user)
    end
  end

  describe "list_ticket_watchers/1" do
    test "returns watchers for ticket", %{ticket: ticket, user: user_a} do
      _user_b = TestFactory.insert(:user)
      user_c = TestFactory.insert(:user)

      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_a)
      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_c)

      watchers = Watchers.list_ticket_watchers(ticket)
      assert pluck_ids(watchers) == pluck_ids([user_a, user_c])
    end
  end

  describe "list_ticket_watcher_addresses/1" do
    test "returns id and email for ticket watchers", %{ticket: ticket, user: user_a} do
      _user_b = TestFactory.insert(:user, email: "user_b@#{__MODULE__}.com")
      user_c = TestFactory.insert(:user, email: "user_c@#{__MODULE__}.com")

      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_a)
      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_c)

      addresses = Watchers.list_ticket_watcher_addresses(ticket)

      assert Enum.sort(addresses) == Enum.sort([
          {user_a.id, user_a.email},
          {user_c.id, user_c.email},
        ])
    end

    test "returns id and email for ticket watchers excluding given ids",
        %{ticket: ticket, user: user_a} do
      _user_b = TestFactory.insert(:user, email: "user_b@#{__MODULE__}.com")
      user_c = TestFactory.insert(:user, email: "user_c@#{__MODULE__}.com")

      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_a)
      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user_c)

      addresses = Watchers.list_ticket_watcher_addresses(ticket, [])

      assert Enum.sort(addresses) == Enum.sort([
          {user_a.id, user_a.email},
          {user_c.id, user_c.email},
        ])

      assert Watchers.list_ticket_watcher_addresses(ticket, exclude_ids: [user_a.id]) == [
          {user_c.id, user_c.email},
        ]

      assert Watchers.list_ticket_watcher_addresses(ticket, exclude_ids: [user_c.id]) == [
          {user_a.id, user_a.email},
        ]
    end
  end

  defp pluck_ids(records) do
    records
    |> Enum.map(&(&1.id))
    |> Enum.sort()
  end
end
