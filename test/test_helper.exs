ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(Ticketee.Repo, :manual)

# Start wallaby application for feature tests
{:ok, _} = Application.ensure_all_started(:wallaby)
Application.put_env(:wallaby, :base_url, TicketeeWeb.Endpoint.url())

# Start ExMachina for test data generation
{:ok, _} = Application.ensure_all_started(:ex_machina)
