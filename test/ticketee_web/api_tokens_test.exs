defmodule TicketeeWeb.APITokensTest do
  use ExUnit.Case, async: true

  alias TicketeeWeb.APITokens

  test "verifies signed tokens" do
    user_id = Ecto.UUID.generate()
    token = APITokens.sign(user_id)

    assert {:ok, ^user_id} = APITokens.verify(token)
  end

  test "returns error for invalid tokens" do
    assert {:error, :invalid} = APITokens.verify("invalid")
    assert {:error, :invalid} = APITokens.verify(Ecto.UUID.generate())
  end
end
