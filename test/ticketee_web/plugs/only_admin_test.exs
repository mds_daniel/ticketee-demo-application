defmodule TicketeeWeb.Plugs.OnlyAdminTest do
  use TicketeeWeb.ConnCase, async: true

  alias TicketeeWeb.Plugs.OnlyAdmin
  alias TicketeeWeb.Plugs.Auth

  alias Ticketee.TestFactory
  alias Ticketee.Accounts.User

  describe "call/2" do
    setup %{conn: conn} do
      user = TestFactory.build(:user)
      conn = conn
        |> bypass_through(TicketeeWeb.Router, [:browser])
        |> get("/")

      {:ok, user: user, conn: conn}
    end

    test "when current_user is admin returns conn", %{conn: initial_conn, user: user} do
      admin = %User{user | admin: true}
      initial_conn = Auth.login(initial_conn, admin)

      conn = OnlyAdmin.call(initial_conn, [])

      refute conn.halted
      assert conn == initial_conn
    end

    test "when current user is not admin redirects", %{conn: initial_conn, user: user} do
      initial_conn = Auth.login(initial_conn, user)

      conn = OnlyAdmin.call(initial_conn, [])

      assert conn.halted
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
      assert redirected_to(conn) == Routes.project_path(conn, :index)
    end

    test "when user is not logged in redirects", %{conn: initial_conn} do
      conn = OnlyAdmin.call(initial_conn, [])

      assert conn.halted
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
      assert redirected_to(conn) == Routes.project_path(conn, :index)
    end
  end
end
