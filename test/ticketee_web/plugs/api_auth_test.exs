defmodule TicketeeWeb.Plugs.APIAuthTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias TicketeeWeb.Plugs.APIAuth
  alias TicketeeWeb.APITokens

  describe "call/2" do
    setup do
      user = TestFactory.insert(:user)
      {:ok, user: user}
    end

    test "with valid token assigns current_user", %{conn: conn, user: user} do
      token = APITokens.sign(user.id)
      conn = Plug.Conn.put_req_header(conn, "authorization", "Token token=#{token}")

      conn = APIAuth.call(conn, [])

      refute conn.halted
      assert conn.assigns.current_user == user
    end

    test "without token returns unauthorized", %{conn: conn} do
      conn = APIAuth.call(conn, [])

      assert conn.halted
      refute conn.assigns[:current_user]

      assert conn.status == 401
      assert response_content_type_header(conn) == jsonapi_mime_type()
      assert_error_body_unauthorized(conn)
    end

    test "with invalid token returns unauthorized", %{conn: conn} do
      conn = Plug.Conn.put_req_header(conn, "authorization", "Token token=invalidtoken")

      conn = APIAuth.call(conn, [])

      assert conn.halted
      refute conn.assigns[:current_user]

      assert conn.status == 401
      assert response_content_type_header(conn) == jsonapi_mime_type()
      assert_error_body_unauthorized(conn)
    end

    test "with invalid user_id returns unauthorized", %{conn: conn} do
      invalid_user_id = Ecto.UUID.generate()
      token = APITokens.sign(invalid_user_id)
      conn = Plug.Conn.put_req_header(conn, "authorization", "Token token=#{token}")

      conn = APIAuth.call(conn, [])

      assert conn.halted
      refute conn.assigns[:current_user]

      assert conn.status == 401
      assert response_content_type_header(conn) == jsonapi_mime_type()
      assert_error_body_unauthorized(conn)
    end
  end

  defp assert_error_body_unauthorized(conn) do
    assert conn.resp_body == Jason.encode!(%{
      errors: [
        %{title: "Unauthorized",
          detail: "Unauthorized",
          status: "401"}
      ]
    })
  end
end
