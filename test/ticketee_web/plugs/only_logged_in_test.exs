defmodule TicketeeWeb.Plugs.OnlyLoggedInTest do
  use TicketeeWeb.ConnCase, async: true

  alias TicketeeWeb.Plugs.OnlyLoggedIn
  alias TicketeeWeb.Plugs.Auth
  alias Ticketee.TestFactory


  describe "call/2" do
    setup %{conn: conn} do
      user = TestFactory.build(:user)
      conn = conn
        |> bypass_through(TicketeeWeb.Router, [:browser])
        |> get("/")

      {:ok, user: user, conn: conn}
    end

    test "when current_user is assigned returns conn", %{conn: initial_conn, user: user} do
      initial_conn = Auth.login(initial_conn, user)

      conn = OnlyLoggedIn.call(initial_conn, [])

      refute conn.halted
      assert conn == initial_conn
    end

    test "when current_user is missing redirects", %{conn: initial_conn} do
      conn = OnlyLoggedIn.call(initial_conn, [])

      assert conn.halted
      assert get_flash(conn, :error) == "You must sign in first!"
      assert redirected_to(conn) == Routes.session_path(conn, :new)
    end
  end
end
