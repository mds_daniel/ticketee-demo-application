defmodule TicketeeWeb.Plugs.PreventArchivedUsersTest do
  use TicketeeWeb.ConnCase, async: true

  alias TicketeeWeb.Plugs.PreventArchivedUsers
  alias TicketeeWeb.Plugs.Auth

  alias Ticketee.TestFactory
  alias Ticketee.Accounts.User

  describe "call/2" do
    setup %{conn: conn} do
      user = TestFactory.build(:user)
      conn = conn
        |> Plug.Test.init_test_session(%{})
        |> bypass_through(TicketeeWeb.Router, [:browser])
        |> get("/")

      {:ok, user: user, conn: conn}
    end

    test "allows the conn to pass when no user signed in", %{conn: initial_conn} do
      conn = PreventArchivedUsers.call(initial_conn, [])

      refute conn.halted
      assert initial_conn == conn
    end

    test "allows the conn to pass when current_user not archived", %{conn: initial_conn, user: user} do
      signed_in_conn = Auth.login(initial_conn, user)

      conn = PreventArchivedUsers.call(signed_in_conn, [])

      refute conn.halted
      assert signed_in_conn == conn
    end

    test "halts conn when current_user is archived", %{conn: initial_conn, user: user} do
      archived_at = DateTime.utc_now()
      signed_in_conn = Auth.login(initial_conn, %User{user | archived_at: archived_at})

      conn = PreventArchivedUsers.call(signed_in_conn, [])

      assert conn.halted
      assert redirected_to(conn) == Routes.session_path(conn, :new)
      assert get_flash(conn, :error) == "Your account has been archived!"
    end
  end
end
