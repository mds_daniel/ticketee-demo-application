defmodule TicketeeWeb.Plugs.AuthTest do
  use TicketeeWeb.ConnCase, async: true

  alias TicketeeWeb.Plugs.Auth
  alias Ticketee.TestFactory
  alias Ticketee.Accounts.User

  setup %{conn: conn} do
    conn = Plug.Test.init_test_session(conn, %{})

    {:ok, conn: conn}
  end

  describe "call/2" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "places currrent user from session into assigns", %{conn: conn, user: user} do
      conn = put_session(conn, :user_id, user.id)

      conn = Auth.call(conn, [])

      assert conn.assigns.current_user == user
    end

    test "sets current user to nil when user_id not in session", %{conn: conn} do
      conn = Auth.call(conn, [])

      assert conn.assigns.current_user == nil
    end
  end

  describe "login/2" do
    setup :build_mock_user

    test "puts the user_id in the session", %{conn: conn, user: user} do
      conn = Auth.login(conn, user)

      assert conn.assigns.current_user == user

      # Session is renewed, and must be fetched on next `GET`
      next_conn = get(conn, "/")
      assert get_session(next_conn, :user_id) == user.id
    end
  end

  describe "logout/1" do
    setup :build_mock_user

    test "drops session removing user_id", %{conn: conn, user: user} do
      conn = put_session(conn, :user_id, user.id)

      conn = Auth.logout(conn)

      assert conn.assigns.current_user == nil
      assert get_session(conn, :user_id) == nil
      assert session_dropped?(conn)
    end
  end

  ## Helpers

  defp build_mock_user(_context) do
    user = %User{
      id: TestFactory.user_uuid(),
      email: "test_user@#{__MODULE__}.com"
    }

    {:ok, user: user}
  end
end
