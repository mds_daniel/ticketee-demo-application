defmodule TicketeeWeb.Features.UsersTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Accounts

  setup %{session: session} do
    admin = TestFactory.insert(:user, admin: true, email: "admin@#{__MODULE__}.com")
    sign_in_with(session, admin.email)

    session
    |> click(link("Admin Area"))

    {:ok, user: admin}
  end

  @users_admin_link css(".admin-nav-links a.nav-link", text: "Users")

  describe "viewing users" do
    setup do
      TestFactory.insert(:user, email: "carol@#{__MODULE__}.com")
      TestFactory.insert(:user, email: "alice@#{__MODULE__}.com")
      TestFactory.insert(:user, email: "bob@#{__MODULE__}.com")
      :ok
    end

    feature "admins can view all users", %{session: session} do
      session
      |> click(@users_admin_link)

      # Users must be ordered alphabetically
      session
      |> assert_has(css("h1", text: "Users"))
      |> assert_has(user_row_nth("admin@#{__MODULE__}.com", 1))
      |> assert_has(user_row_nth("alice@#{__MODULE__}.com", 2))
      |> assert_has(user_row_nth("bob@#{__MODULE__}.com", 3))
      |> assert_has(user_row_nth("carol@#{__MODULE__}.com", 4))
    end
  end

  # There should be 2 "Password" fields, choose the first
  @password_field text_field("Password", count: 2, at: 0)
  @password_confirmation text_field("Password confirmation")

  describe "creating users" do
    setup %{session: session} do
      session
      |> click(@users_admin_link)
      |> click(link("New User"))

      :ok
    end

    feature "admin users can create new users", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "new_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "super secret")
      |> fill_in(@password_confirmation, with: "super secret")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "User has been successfully created!"))
      |> assert_has(user_row("new_user@#{__MODULE__}.com Regular User"))
    end

    feature "admin users can create new admin users", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "new_admin@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "super secret")
      |> fill_in(@password_confirmation, with: "super secret")
      |> click(checkbox("Is admin?"))
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "User has been successfully created!"))
      |> assert_has(user_row("new_admin@#{__MODULE__}.com Admin"))
    end

    feature "with invalid user params returns errors", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "new_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "   ") # blank password
      |> fill_in(@password_confirmation, with: "   ")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create user!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "promoting users" do
    setup do
      user = TestFactory.insert(:user, email: "ticketee_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    feature "admins can promote other users to admin status", %{session: session, user: user} do
      session
      |> click(@users_admin_link)
      |> assert_has(user_row("#{user.email} Regular User"))

      session
      |> click_confirmation(link("Promote to Admin"), "Are you sure you want to promote this user to an admin?")

      session
      |> assert_has(css(".alert", text: "User has been successfully promoted to admin!"))
      |> assert_has(user_row("#{user.email} Admin"))
    end
  end

  describe "archiving users" do
    setup do
      user = TestFactory.insert(:user, email: "ticketee_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    feature "admins can archive other users", %{session: session, user: user} do
      session
      |> click(@users_admin_link)
      |> assert_has(user_row("#{user.email} Regular User"))

      session
      |> click_confirmation(link("Archive"), "Are you sure you want to archive this user?")

      session
      |> assert_has(css(".alert", text: "User has been successfully archived!"))
      |> assert_has(user_row("#{user.email} Archived"))
    end

    feature "admins can restore other archived users", %{session: session, user: user} do
      assert {:ok, user} = Accounts.archive_user(user)

      session
      |> click(@users_admin_link)
      |> assert_has(user_row("#{user.email} Archived"))

      session
      |> click_confirmation(link("Restore"), "Are you sure you want to restore this user?")

      session
      |> assert_has(css(".alert", text: "User has been successfully restored!"))
      |> assert_has(user_row("#{user.email} Regular User"))
    end
  end

  ## Helpers

  defp user_row(email) do
    css(".users-table .user-row", text: email)
  end

  # Note: table rows are `1 indexed`
  defp user_row_nth(email, n) do
    css(".users-table .user-row:nth-of-type(#{n})", text: email)
  end
end
