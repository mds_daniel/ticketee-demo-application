defmodule TicketeeWeb.Features.TicketNotificationsTest do
  use TicketeeWeb.FeatureCase, async: false
  use Bamboo.Test, shared: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles

  @new_comment_field text_field("comment[description]")

  @sessions 2
  feature "commenting on tickets notifies watchers", %{sessions: [session_alice, session_bob]} do
    alice_email = "alice@#{__MODULE__}.com"
    bob_email = "bob@#{__MODULE__}.com"

    alice = TestFactory.insert(:user, email: alice_email)
    bob = TestFactory.insert(:user, email: bob_email)

    project = TestFactory.insert(:project)
    {:ok, _role} = Roles.grant_role(alice, :manager, project)
    {:ok, _role} = Roles.grant_role(bob, :editor, project)

    TestFactory.insert(:state, name: "Open", default: true)
    TestFactory.insert(:state, name: "In Progress")

    # Alice creates a new ticket
    session_alice
    |> sign_in_with(alice_email)
    |> click(link(project.name))
    |> click(link("New Ticket"))
    |> fill_in(text_field("Title"), with: "Study more #{__MODULE__}")
    |> fill_in(text_field("Description"), with: "More research is required")
    |> click(button("Save"))

    session_alice
    |> assert_has(css(".alert", text: "Ticket has been successfully created!"))

    assert [ticket] = Projects.list_tickets_for_index(project)
    ticket_comments_url = Routes.ticket_comment_url(Endpoint, :index, ticket)

    # Bob comments on ticket
    session_bob
    |> sign_in_with(bob_email)
    |> click(link(project.name))
    |> click(link("Study more #{__MODULE__}"))
    |> click(link("Comments"))
    |> fill_in(@new_comment_field, with: "I'll start working on it")
    |> click(button("Save"))

    session_bob
    |> assert_has(css(".alert", text: "Comment has been successfully created!"))

    # Assert Alice receives email about Bob's comment
    assert_delivered_email_matches(%{
      to: [{_, ^alice_email}],
      text_body: text_body,
      html_body: html_body,
    })

    assert text_body =~ "A new comment has been added for the ticket: Study more #{__MODULE__}"
    assert text_body =~ "I'll start working on it"
    assert text_body =~ ticket_comments_url

    assert html_body =~ "Study more #{__MODULE__}"
    assert html_body =~ format_description("I'll start working on it")
    assert html_body =~ ticket_comments_url

    # Alice comments back on ticket
    session_alice
    |> click(link("Comments"))
    |> fill_in(@new_comment_field, with: "I'm setting the status to 'In Progress'")
    |> click(state_select_option("In Progress"))
    |> click(button("Save"))

    session_alice
    |> assert_has(css(".alert", text: "Comment has been successfully created!"))

    # Assert Bob receives email about Alice's comment
    assert_delivered_email_matches(%{
      to: [{_, ^bob_email}],
      text_body: text_body,
      html_body: html_body,
    })

    assert text_body =~ "A new comment has been added for the ticket: Study more #{__MODULE__}"
    assert text_body =~ "I'm setting the status to 'In Progress'"
    assert text_body =~ ticket_comments_url

    assert html_body =~ "Study more #{__MODULE__}"
    assert html_body =~ format_description("I'm setting the status to 'In Progress'")
    assert html_body =~ ticket_comments_url
  end

  def state_select_option(label) do
    css("select#comment_state_id option", text: label)
  end

  defp format_description(description) do
    description
    |> Phoenix.HTML.Format.text_to_html()
    |> Phoenix.HTML.safe_to_string()
  end
end
