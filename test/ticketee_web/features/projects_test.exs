defmodule TicketeeWeb.Features.ProjectsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup %{session: session} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  feature "users can view projects they were assigned roles to", %{session: session, user: user} do
    project1 = TestFactory.insert(:project, name: "Manhattan Project")
    _project2 = TestFactory.insert(:project, name: "Orion Project")

    assert {:ok, _role} = Roles.grant_role(user, :viewer, project1)

    session
    |> visit("/")
    |> assert_has(project_row("Manhattan Project"))
    |> refute_has(project_row("Orion Project"))

    # Visiting project link
    session
    |> click(link("Manhattan Project"))

    assert page_title(session) == "Ticketee | Projects | Manhattan Project"
    assert current_path(session) == project_path(project1)
  end

  feature "admins can see all projects", %{session: session, user: user} do
    assert {:ok, _} = Accounts.promote_to_admin(user)

    _project1 = TestFactory.insert(:project, name: "Manhattan Project")
    _project2 = TestFactory.insert(:project, name: "Orion Project")

    session
    |> visit("/")
    |> assert_has(project_row("Manhattan Project"))
    |> assert_has(project_row("Orion Project"))
  end


  describe "creating projects (when admin)" do
    setup %{user: user} do
      {:ok, user} = Accounts.promote_to_admin(user)
      {:ok, user: user}
    end

    feature "admins can create new projects", %{session: session} do
      session
      |> visit("/")
      |> click(link("New Project"))

      session
      |> fill_in(text_field("Name"), with: "Sublime Text 3")
      |> fill_in(text_field("Description"), with: "A text editor for everyone")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Project has been successfully created!"))
      |> assert_has(css(".project-name", text: "Sublime Text 3"))

      project = Projects.get_project_by_name("Sublime Text 3")

      assert page_title(session) == "Ticketee | Projects | Sublime Text 3"
      assert current_path(session) == project_path(project)
    end

    feature "with invalid project params on create returns errors", %{session: session} do
      session
      |> visit("/")
      |> click(link("New Project"))

      session
      |> fill_in(text_field("Name"), with: "    ") # empty name field
      |> fill_in(text_field("Description"), with: "A text editor for everyone")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create project!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "updating projects" do
    setup %{session: session, user: user} do
      project = TestFactory.insert(:project, name: "Colonizing Mars")
      {:ok, _role} = Roles.grant_role(user, :manager, project)

      session
      |> visit("/")
      |> click(link(project.name))
      |> click(link("Edit"))

      {:ok, project: project}
    end

    feature "users can edit existing projects", %{session: session, project: project} do
      session
      |> fill_in(text_field("Name"), with: "Colonizing The Red Planet")
      |> click(button("Save"))

      updated_project = Projects.get_project(project.id)

      assert updated_project.name == "Colonizing The Red Planet"
      assert current_path(session) == project_path(updated_project)

      session
      |> assert_has(css(".alert", text: "Project has been successfully updated!"))
      |> assert_has(css(".project-name", text: "Colonizing The Red Planet"))
    end

    feature "with invalid params returns errors", %{session: session} do
      session
      |> fill_in(text_field("Name"), with: "           ")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to update project!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  feature "admins can delete projects", %{session: session, user: user} do
    {:ok, _user} = Accounts.promote_to_admin(user)
    project = TestFactory.insert(:project, name: "Manhattan Project")

    session
    |> visit("/")
    |> click(link(project.name))

    session
    |> click_confirmation(link("Delete"), "Are you sure?")

    session
    |> assert_has(css(".alert", text: "Project has been successfully deleted!"))
    |> refute_has(css(".projects-table .project-row", text: project.name))

    assert current_path(session) == Routes.project_path(Endpoint, :index)
  end

  ## Helpers

  def project_row(name) do
    css(".projects-table .project-row", text: name)
  end
end
