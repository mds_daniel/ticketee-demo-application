defmodule TicketeeWeb.Features.SearchTicketsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Roles

  setup %{session: session} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  @search_tickets link("Search Tickets")
  @search_button button("Search")

  feature "searching for tickets", %{session: session, user: user} do
    project_a = TestFactory.insert(:project)
    project_b = TestFactory.insert(:project)
    {:ok, _role} = Roles.grant_role(user, :viewer, project_a)
    {:ok, _role} = Roles.grant_role(user, :viewer, project_b)

    state_opened = TestFactory.insert(:state, name: "Open")
    state_closed = TestFactory.insert(:state, name: "Closed")
    tag_cooking = TestFactory.insert(:tag, name: "Cooking")

    TestFactory.insert(:ticket,
      project: project_a,
      title: "Wash dishes",
      state: state_opened)

    TestFactory.insert(:ticket,
      project: project_b,
      title: "Cook dinner",
      tags: [tag_cooking],
      state: state_closed)

    # Assert all tickets visible at first
    session
    |> click(@search_tickets)
    |> click(@search_button)
    |> assert_has(ticket_row("Wash dishes"))
    |> assert_has(ticket_row("Cook dinner"))

    # Searching for project
    session
    |> click(@search_tickets)
    |> click(project_option(project_a.name))
    |> click(@search_button)
    |> assert_has(ticket_row("Wash dishes"))
    |> refute_has(ticket_row("Cook dinner"))

    # Searching for title
    session
    |> click(@search_tickets)
    |> fill_in(text_field("Title"), with: "dinner")
    |> click(@search_button)
    |> assert_has(ticket_row("Cook dinner"))
    |> refute_has(ticket_row("Wash dishes"))

    # Searching for state
    session
    |> click(@search_tickets)
    |> click(state_option("Open"))
    |> click(@search_button)
    |> assert_has(ticket_row("Wash dishes"))
    |> refute_has(ticket_row("Cook dinner"))

    # Searching for tag
    session
    |> click(@search_tickets)
    |> click(tag_option("Cooking"))
    |> click(@search_button)
    |> assert_has(ticket_row("Cook dinner"))
    |> refute_has(ticket_row("Wash dishes"))
  end

  defp ticket_row(title) do
    css(".tickets-table .ticket-row .ticket-title", text: title)
  end

  defp state_option(name) do
    css("#search_state_name option", text: name)
  end

  defp project_option(name) do
    css("#search_project_id option", text: name)
  end

  defp tag_option(name) do
    css("#search_tag_name option", text: name)
  end
end
