defmodule TicketeeWeb.Features.TicketsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles

  setup do
    project = TestFactory.insert(:project)
    {:ok, project: project}
  end

  setup %{session: session, project: project} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, _role} = Roles.grant_role(user, :editor, project)

    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  feature "viewing tickets", %{session: session, project: project} do
    ticket = TestFactory.insert(:ticket, project: project, title: "Wash the car")

    session
    |> visit(project_path(project))
    |> assert_has(project_ticket_row("Wash the car"))

    session
    |> click(link(ticket.title))
    |> assert_has(css(".project-name", text: project.name))
    |> assert_has(css(".ticket-title", text: ticket.title))
  end

  describe "creating tickets" do
    setup %{session: session, project: project} do
      session
      |> visit("/")
      |> click(link(project.name))
      |> click(link("New Ticket"))

      :ok
    end

    feature "users can create new tickets",
        %{session: session, project: project, user: user} do
      session
      |> fill_in(text_field("Title"), with: "Wash dishes")
      |> fill_in(text_field("Description"), with: "Dishes must be washed after every meal")
      |> click(button("Save"))

      assert current_path(session) == project_ticket_path(project, latest_ticket(project))

      session
      |> assert_has(css(".alert", text: "Ticket has been successfully created!"))
      |> assert_has(css(".ticket-title", text: "Wash dishes"))
      |> assert_has(css(".project-name", text: project.name))
      |> assert_has(css("#ticket-attributes .author-info", text: user.email))

      session
      |> visit(project_path(project))
      |> assert_has(project_ticket_row("Wash dishes"))
    end

    feature "when providing invalid attributes returns errors", %{session: session} do
      session
      |> fill_in(text_field("Title"), with: "   ") # empty title
      |> fill_in(text_field("Description"), with: "Dishes must be washed after every meal")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create ticket!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "editing tickets" do
    setup %{session: session, project: project, user: user} do
      ticket = TestFactory.insert(:ticket,
        author: user,
        project: project,
        title: "Some neat ticket")

      session
      |> visit(project_path(project))
      |> click(link(ticket.title))
      |> click(link("Edit"))

      {:ok, ticket: ticket}
    end

    feature "users can edit existing tickets",
        %{session: session, project: project, ticket: ticket} do
      session
      |> fill_in(text_field("Title"), with: "Some updated neat title")
      |> fill_in(text_field("Description"), with: "Some updated neat Description")
      |> click(button("Save"))

      assert current_path(session) == project_ticket_path(project, ticket)

      session
      |> assert_has(css(".alert", text: "Ticket has been successfully updated!"))
      |> assert_has(css(".ticket-title", text: "Some updated neat title"))
      |> assert_has(css(".ticket-description", text: "Some updated neat Description"))
    end

    feature "when providing invalid attributes returns errors", %{session: session} do
      session
      |> fill_in(text_field("Title"), with: "   ") # empty title
      |> fill_in(text_field("Description"), with: "Some updated neat Description")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to update ticket!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "deleting tickets" do
    setup %{session: session, project: project, user: user} do
      ticket = TestFactory.insert(:ticket,
        project: project,
        title: "Some neat ticket")

      {:ok, _} = Roles.grant_role(user, :manager, project)

      session
      |> visit(project_path(project))
      |> click(link(ticket.title))

      {:ok, ticket: ticket}
    end

    feature "users can delete existing tickets", %{session: session, project: project, ticket: ticket} do
      session
      |> click_confirmation(link("Delete"), "Are you sure?")

      session
      |> assert_has(css(".alert", ticket: "Ticket has been successfully deleted!"))
      |> refute_has(project_ticket_row(ticket.title))

      assert current_path(session) == project_path(project)
    end
  end

  ## Helpers

  def project_ticket_row(title) do
    css(".tickets-table .ticket-row a", text: title)
  end

  def latest_ticket(project) do
    project
    |> Projects.with_tickets()
    |> Map.get(:tickets)
    |> List.last()
  end
end
