defmodule TicketeeWeb.Features.TicketsAttachmentTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Roles
  alias Ticketee.Attachments

  setup %{session: session} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project, author: user)

    {:ok, _role} = Roles.grant_role(user, :editor, project)
    sign_in_with(session, user.email)

    {:ok, user: user, project: project, ticket: ticket}
  end

  describe "creating attachments" do
    setup %{session: session, project: project, ticket: ticket} do
      session
      |> visit("/")
      |> click(link(project.name))
      |> click(link(ticket.title))
      |> click(link("New Attachment"))

      :ok
    end

    feature "project editors can create attachments",
        %{session: session, project: project, ticket: ticket} do
      session
      |> attach_file(file_field("File"), path: fixture_file("speed.txt"))
      |> fill_in(text_field("Description"), with: "A nice description for the speed.txt file")
      |> click(button("Save"))


      session
      |> assert_has_current_path(project_ticket_path(project, ticket))
      |> assert_has(css(".alert", text: "Ticket attachment has been successfully created!"))
      |> assert_has(css(".ticket-attachment-container .card-title", text: "speed.txt"))
      |> assert_has(css(".ticket-attachment-container .card-title .attachment-size", text: "71 B"))
      |> assert_has(css(".ticket-attachment-container .card-text", text: "A nice description for the speed.txt file"))
    end

    feature "when providing invalid attributes returns errors", %{session: session} do
      session
      |> attach_file(file_field("File"), path: fixture_file("something.invalid"))
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create attachment!"))
      |> assert_has(css(".invalid-feedback", text: "invalid file extension"))
    end
  end

  describe "deleting attachments" do
    setup %{ticket: ticket} do
      assert {:ok, attachment} = Attachments.create_ticket_attachment(ticket, %{
        file: fixture_file_upload("speed.txt")
      })

      {:ok, attachment: attachment}
    end

    setup %{session: session, user: user, project: project, ticket: ticket} do
      {:ok, _role} = Roles.grant_role(user, :manager, project)

      session
      |> visit("/")
      |> click(link(project.name))
      |> click(link(ticket.title))

      :ok
    end

    feature "project managers can delete attachments",
        %{session: session, project: project, ticket: ticket} do
      session
      |> assert_has(css(".ticket-attachment-container .card-title", text: "speed.txt"))
      |> click_confirmation(
          css(".ticket-attachment-container .card-title a.link-delete", text: "Delete"),
          "Are you sure you want to delete this attachment?"
        )

      session
      |> assert_has_current_path(project_ticket_path(project, ticket))
      |> assert_has(css(".alert", text: "Ticket attachment has been successfully deleted!"))
      |> refute_has(css(".ticket-attachment-container .card-title", text: "speed.txt"))
    end
  end
end
