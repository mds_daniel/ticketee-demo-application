defmodule TicketeeWeb.Features.HiddenLinksTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Roles

  setup do
    project = TestFactory.insert(:project, name: "Project #{__MODULE__}")
    {:ok, project: project}
  end

  describe "anonymous users" do
    feature "get redirected when trying to access projects",
        %{session: session, project: project} do
      session
      |> visit("/projects")

      session
      |> assert_has(css(".alert", text: "You must sign in first!"))
      |> assert_has_current_path(Routes.session_path(Endpoint, :new))

      ## Show project page

      session
      |> visit(project_path(project))

      session
      |> assert_has(css(".alert", text: "You must sign in first!"))
      |> assert_has_current_path(Routes.session_path(Endpoint, :new))
    end
  end

  describe "viewers of the project" do
    setup %{session: session, project: project} do
      user = TestFactory.insert(:user)
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      sign_in_with(session, user.email)

      :ok
    end

    feature "cannot see project create, edit, or delete links",
        %{session: session, project: project} do
      session
      |> visit("/projects")

      session
      |> assert_has_current_path("/projects")
      |> refute_has(css(".link-add", text: "New Project"))

      ## Show project page

      session
      |> visit(project_path(project))

      session
      |> assert_has_current_path(project_path(project))
      |> assert_has(css(".project-name", text: "Project #{__MODULE__}"))
      |> refute_has(css(".link-delete", text: "Delete"))
      |> refute_has(css(".link-edit", text: "Edit"))
    end

    feature "cannot see ticket create, edit, delete links",
        %{session: session, project: project} do
      ticket = TestFactory.insert(:ticket, project: project, title: "Awesome ticket")

      session
      |> visit(project_path(project))

      session
      |> refute_has(css(".link-add", text: "New Ticket"))

      session
      |> click(link(ticket.title))

      session
      |> assert_has(css(".ticket-title", text: ticket.title))
      |> refute_has(css(".link-edit", text: "Edit"))
      |> refute_has(css(".link-delete", text: "Delete"))
    end
  end

  describe "admin users" do
    setup %{session: session} do
      admin = TestFactory.insert(:user, admin: true)
      sign_in_with(session, admin.email)

      :ok
    end

    feature "see project create, edit and delete links",
        %{session: session, project: project} do
      session
      |> visit("/projects")

      session
      |> assert_has_current_path("/projects")
      |> assert_has(css(".link-add", text: "New Project"))

      ## Show project page

      session
      |> visit(project_path(project))

      session
      |> assert_has_current_path(project_path(project))
      |> assert_has(css(".project-name", text: "Project #{__MODULE__}"))
      |> assert_has(css(".link-delete", text: "Delete"))
      |> assert_has(css(".link-edit", text: "Edit"))
    end

    feature "see ticket create, edit, delete links",
        %{session: session, project: project} do
      ticket = TestFactory.insert(:ticket, project: project, title: "Awesome ticket")

      session
      |> visit(project_path(project))

      session
      |> assert_has(css(".link-add", text: "New Ticket"))

      session
      |> click(link(ticket.title))

      session
      |> assert_has(css(".ticket-title", text: ticket.title))
      |> assert_has(css(".link-edit", text: "Edit"))
      |> assert_has(css(".link-delete", text: "Delete"))
    end
  end
end
