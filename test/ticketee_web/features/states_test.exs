defmodule TicketeeWeb.Features.StatesTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory

  setup %{session: session} do
    user = TestFactory.insert(:user, admin: true, email: "admin@#{__MODULE__}.com")
    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  describe "viewing states" do
    setup do
      TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true)
      TestFactory.insert(:state, name: "Awesome #{__MODULE__}")
      TestFactory.insert(:state, name: "In progress #{__MODULE__}")
      TestFactory.insert(:state, name: "Closed #{__MODULE__}")

      :ok
    end

    feature "admins can see all states", %{session: session} do
      session
      |> click(link("Admin Area"))
      |> click(admin_link("States"))

      session
      |> assert_has(state_row("Open #{__MODULE__}"))
      |> assert_has(state_row("Awesome #{__MODULE__}"))
      |> assert_has(state_row("In progress #{__MODULE__}"))
      |> assert_has(state_row("Closed #{__MODULE__}"))
    end
  end

  describe "creating states" do
    setup %{session: session} do
      session
      |> click(link("Admin Area"))
      |> click(admin_link("States"))
      |> click(link("New State"))

      :ok
    end

    feature "admins can create new states", %{session: session} do
      session
      |> fill_in(text_field("Name"), with: "Open #{__MODULE__}")
      |> fill_in(text_field("Order no"), with: 42)
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "State has been successfully created!"))
      |> assert_has(state_row("Open #{__MODULE__}"))
    end

    feature "when providing invalid attributes returns errors", %{session: session} do
      session
      |> fill_in(text_field("Name"), with: "     ") # blank name
      |> fill_in(text_field("Order no"), with: 42)
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create state!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "updating states" do
    setup do
      state = TestFactory.insert(:state, name: "Awesome #{__MODULE__}")
      {:ok, state: state}
    end

    setup %{session: session} do
      session
      |> click(link("Admin Area"))
      |> click(admin_link("States"))
      |> click(css(".state-row .row-actions a.link-edit", text: "Edit"))

      :ok
    end

    feature "admins can update states", %{session: session} do
      session
      |> fill_in(text_field("Name"), with: "Updated Awesome #{__MODULE__}")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "State has been successfully updated!"))
      |> assert_has(state_row("Updated Awesome #{__MODULE__}"))
    end

    feature "when providing invalid attributes returns errors", %{session: session} do
      session
      |> fill_in(text_field("Name"), with: "    ") # blank name
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to update state!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "deleting states" do
    setup do
      state = TestFactory.insert(:state, name: "Awesome #{__MODULE__}")
      {:ok, state: state}
    end

    setup %{session: session} do
      session
      |> click(link("Admin Area"))
      |> click(admin_link("States"))

      :ok
    end

    feature "admins can delete states", %{session: session} do
      # Assert precondition
      session
      |> assert_has(state_row("Awesome #{__MODULE__}"))

      session
      |> click_confirmation(
          css(".state-row .row-actions a.link-delete", text: "Delete"),
          "Are you sure you want to delete this state?"
        )

      session
      |> assert_has(css(".alert", text: "State has been successfully deleted!"))
      |> refute_has(state_row("Awesome #{__MODULE__}"))
    end

    feature "when tickets with this state already exist returns error",
        %{session: session, state: state} do
      project = TestFactory.insert(:project)
      TestFactory.insert(:ticket, project: project, state: state)

      session
      |> click_confirmation(
          css(".state-row .row-actions a.link-delete", text: "Delete"),
          "Are you sure you want to delete this state?"
        )

      session
      |> assert_has(css(".alert", text: "Failed to delete state! Cannot delete while tickets have this state."))
      |> assert_has(state_row("Awesome #{__MODULE__}"))
    end
  end

  describe "setting default state" do
    setup do
      state_new = TestFactory.insert(:state, name: "New #{__MODULE__}", default: true)
      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}")
      {:ok, state_new: state_new, state_open: state_open}
    end

    setup %{session: session} do
      session
      |> click(link("Admin Area"))
      |> click(admin_link("States"))

      :ok
    end

    feature "admins can toggle default state",
        %{session: session, state_new: state_new, state_open: state_open} do
      session
      |> assert_has(state_row(state_new.name))
      |> assert_has(state_row(state_open.name))
      |> assert_default_state(state_new)

      session
      |> click(css("#state-row-#{state_open.id} a", text: "Set as default"))

      |> assert_has(css(".alert", text: "State has been successfully set as default!"))
      |> assert_default_state(state_open)
    end
  end

  defp admin_link(text) do
    css(".admin-nav-links a", text: text)
  end

  defp state_row(text) do
    css(".state-row .state-name", text: text)
  end

  defp assert_default_state(session, state) do
    assert_has(session, css("#state-row-#{state.id} .state-actions .state-badge", text: "Default"))
  end
end
