defmodule TicketeeWeb.Features.SessionsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Accounts

  setup do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, user: user}
  end

  describe "signing in" do
    feature "users can sign in", %{session: session, user: user} do
      session
      |> visit("/")
      |> sign_in_with(user.email, "super secret")

      session
      |> assert_has(css(".alert", text: "Signed in successfully!"))
      |> assert_has(css(".signed-in-user", text: user.email))

      assert current_path(session) == Routes.project_path(Endpoint, :index)
    end

    feature "archived users cannot sign in", %{session: session, user: user} do
      {:ok, user} = Accounts.archive_user(user)

      session
      |> visit("/")
      |> sign_in_with(user.email, "super secret")

      session
      |> assert_has(css(".alert", text: "You cannot sign in, because your account has been archived!"))
      |> refute_has(css(".signed-in-user"))
    end

    feature "when password invalid returns errors", %{session: session, user: user} do
      session
      |> visit("/")
      |> sign_in_with(user.email, "invalid password")

      session
      |> assert_has(css(".alert", text: "Invalid email/password combination!"))
    end
  end

  describe "signing out" do
    setup %{session: session, user: user} do
      session
      |> visit("/")
      |> sign_in_with(user.email, "super secret")

      :ok
    end

    feature "users can sign out", %{session: session} do
      session
      |> click(link("Sign out"))

      session
      |> refute_has(css(".signed-in-user"))

      assert current_path(session) == Routes.session_path(Endpoint, :new)
    end
  end
end
