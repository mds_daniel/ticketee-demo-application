defmodule TicketeeWeb.Features.TagsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Roles

  setup %{session: session} do
    user = TestFactory.insert(:user, email: "manager@#{__MODULE__}.com")
    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  setup do
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, project: project, ticket: ticket}
  end

  @new_tag_name text_field("new-tag-name")

  feature "managers can add tags to tickets",
      %{session: session, user: user, project: project, ticket: ticket} do
    assert {:ok, _role} = Roles.grant_role(user, :manager, project)

    session
    |> visit(project_ticket_path(project, ticket))
    |> click(css("a.link-tags", text: "Tags"))

    session
    |> fill_in(@new_tag_name, with: "Adventure")
    |> click(button("Add"))
    |> fill_in(@new_tag_name, with: "Mystery")
    |> click(button("Add"))
    |> click(button("Save"))

    session
    |> assert_has(css(".alert", text: "Tags have been successfully added!"))
    |> assert_has(css(".tag-badge", text: "Adventure"))
    |> assert_has(css(".tag-badge", text: "Mystery"))

    # Assert tags also appear on ticket show page
    session
    |> visit(project_ticket_path(project, ticket))
    |> assert_has(css(".tag-badge", text: "Adventure"))
    |> assert_has(css(".tag-badge", text: "Mystery"))
  end
end
