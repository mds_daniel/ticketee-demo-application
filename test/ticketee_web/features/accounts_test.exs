defmodule TicketeeWeb.Features.AccountsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory

  describe "signing up" do
    setup %{session: session} do
      session
      |> visit("/")
      |> click(link("Sign up"))

      :ok
    end

    # There should be 2 "Password" fields, choose the first
    @password_field text_field("Password", count: 2, at: 0)
    @password_confirmation text_field("Password confirmation")

    feature "users can sign up for an account", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "test_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "super secret")
      |> fill_in(@password_confirmation, with: "super secret")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "You have signed up successfully!"))
      |> assert_has(css(".signed-in-user", text: "test_user@#{__MODULE__}.com"))

      assert current_path(session) == Routes.project_path(Endpoint, :index)
    end

    feature "with invalid account params returns errors", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "test_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "    ") # blank password
      |> fill_in(@password_confirmation, with: "super secret")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create new account!"))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "updating accounts" do
    setup %{session: session} do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")

      session
      |> sign_in_with(user.email)
      |> click(css("a.signed-in-user", text: user.email))
      |> click(link("Edit"))

      {:ok, user: user}
    end

    feature "users can update their account", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "updated_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "updated password")
      |> fill_in(@password_confirmation, with: "updated password")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Account has been successfully updated!"))
      |> assert_has(css(".user-email", text: "updated_user@#{__MODULE__}.com"))

      # Check that we can now sign in with the updated password
      session
      |> click(link("Sign out"))
      |> sign_in_with("updated_user@#{__MODULE__}.com", "updated password")

      session
      |> assert_has(css(".alert", text: "Signed in successfully!"))
    end

    feature "ignores password when it is blank and updates account", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "updated_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "    ") # blank password
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Account has been successfully updated!"))
      |> assert_has(css(".user-email", text: "updated_user@#{__MODULE__}.com"))
    end

    feature "with invalid account params returns errors", %{session: session} do
      session
      |> fill_in(text_field("Email"), with: "updated_user@#{__MODULE__}.com")
      |> fill_in(@password_field, with: "updated password")
      |> fill_in(@password_confirmation, with: "invalid") # *invalid* confirmation
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to update account!"))
      |> assert_has(css(".invalid-feedback", text: "does not match confirmation"))
    end
  end
end
