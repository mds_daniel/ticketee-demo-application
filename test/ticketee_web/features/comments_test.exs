defmodule TicketeeWeb.Features.CommentsTest do
  use TicketeeWeb.FeatureCase, async: false

  alias Ticketee.TestFactory
  alias Ticketee.Roles

  setup do
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, project: project, ticket: ticket}
  end

  setup %{session: session, project: project} do
    user = TestFactory.insert(:user, email: "ticketee@#{__MODULE__}.com")
    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    sign_in_with(session, user.email)

    {:ok, user: user}
  end

  @new_comment_field text_field("comment[description]")

  describe "creating comments" do
    setup %{session: session, project: project, ticket: ticket} do
      session
      |> visit("/")
      |> click(link(project.name))
      |> click(link(ticket.title))
      |> click(css("a.link-comments", text: "Comments"))

      :ok
    end

    feature "users can create new comments", %{session: session, ticket: ticket, user: user} do
      session
      |> fill_in(@new_comment_field, with: "Exciting comment with much description!")
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Comment has been successfully created!"))
      |> assert_has(css(".ticket-title", text: ticket.title))
      |> assert_has(css(".comment-description", text: "Exciting comment with much description!"))
      |> assert_has(css(".comment-user", text: user.email))
    end

    feature "when providing invalid attributes returns errors", %{session: session, ticket: ticket} do
      session
      |> fill_in(@new_comment_field, with: "        ") # blank description
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Failed to create comment!"))
      |> assert_has(css(".ticket-title", text: ticket.title))
      |> assert_has(css(".invalid-feedback", text: "can't be blank"))
    end
  end

  describe "changing ticket state through comments" do
    setup %{session: session, user: user, project: project, ticket: ticket} do
      {:ok, _role} = Roles.grant_role(user, :manager, project)
      TestFactory.insert(:state, name: "Open")
      TestFactory.insert(:state, name: "In Progress")

      session
      |> visit(project_ticket_path(project, ticket))
      |> click(css("a.link-comments", text: "Comments"))

      :ok
    end

    feature "managers can change a ticket's state through comments", %{session: session} do
      session
      |> fill_in(@new_comment_field, with: "The only constant is change")
      |> click(state_select_option("Open"))
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Comment has been successfully created!"))
      |> assert_has(css(".comment-description", text: "The only constant is change"))
      |> assert_has(css(".comment-state-transition", text: "state changed to Open"))
      |> assert_has(css(".ticket-title .state-badge", text: "Open"))

      session
      |> fill_in(@new_comment_field, with: "Once again, we find ourselves changed")
      |> click(state_select_option("In Progress"))
      |> click(button("Save"))

      session
      |> assert_has(css(".alert", text: "Comment has been successfully created!"))
      |> assert_has(css(".comment-description", text: "Once again, we find ourselves changed"))
      |> assert_has(css(".comment-state-transition", text: "state changed from Open to In Progress"))
      |> assert_has(css(".ticket-title .state-badge", text: "In Progress"))
    end
  end

  def state_select_option(label) do
    css("select#comment_state_id option", text: label)
  end
end
