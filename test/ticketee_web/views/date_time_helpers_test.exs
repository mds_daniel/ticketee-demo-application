defmodule TicketeeWeb.DateTimeHelpersTest do
  use ExUnit.Case, async: true
  alias TicketeeWeb.DateTimeHelpers

  @one_minute 60 # seconds
  @one_hour 60 * @one_minute
  @one_day 24 * @one_hour

  describe "seconds_ago_in_words/1" do
    test "returns days ago correctly" do
      assert DateTimeHelpers.seconds_ago_in_words(5 * @one_day) == "5 days ago"
      assert DateTimeHelpers.seconds_ago_in_words(2 * @one_day) == "2 days ago"
      assert DateTimeHelpers.seconds_ago_in_words(@one_day) == "one day ago"
    end

    test "returns hours ago correctly" do
      assert DateTimeHelpers.seconds_ago_in_words(5 * @one_hour) == "5 hours ago"
      assert DateTimeHelpers.seconds_ago_in_words(2 * @one_hour) == "2 hours ago"
      assert DateTimeHelpers.seconds_ago_in_words(@one_hour) == "one hour ago"
    end

    test "returns minutes ago correctly" do
      assert DateTimeHelpers.seconds_ago_in_words(5 * @one_minute) == "5 minutes ago"
      assert DateTimeHelpers.seconds_ago_in_words(2 * @one_minute) == "2 minutes ago"
      assert DateTimeHelpers.seconds_ago_in_words(@one_minute) == "one minute ago"
    end

    test "returns seconds ago correctly" do
      assert DateTimeHelpers.seconds_ago_in_words(5) == "5 seconds ago"
      assert DateTimeHelpers.seconds_ago_in_words(2) == "2 seconds ago"
      assert DateTimeHelpers.seconds_ago_in_words(1) == "one second ago"

      # returns seconds ago for negative value
      assert DateTimeHelpers.seconds_ago_in_words(-12) == "-12 seconds ago"
      assert DateTimeHelpers.seconds_ago_in_words(-4000) == "-4000 seconds ago"
    end
  end
end
