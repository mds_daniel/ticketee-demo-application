defmodule TicketeeWeb.TicketWatcherControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Roles
  alias Ticketee.Notifications.Watchers

  setup %{conn: conn} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)

    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    {:ok, conn: conn, user: user, ticket: ticket, project: project}
  end

  describe "POST /tickets/:ticket_id/watcher" do
    test "adds watcher to ticket for current user",
        %{conn: conn, ticket: ticket, user: user} do
      conn = post(conn, Routes.ticket_watcher_path(conn, :create, ticket))

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, ticket.project_id, ticket)
      assert get_flash(conn, :info) == "You are now watching this ticket!"

      assert [watcher] = Watchers.list_ticket_watchers(ticket)
      assert watcher.id == user.id
    end

    test "when already watching ticket returns error",
        %{conn: conn, ticket: ticket, user: user} do
      assert {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user)

      conn = post(conn, Routes.ticket_watcher_path(conn, :create, ticket))

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, ticket.project_id, ticket)
      assert get_flash(conn, :error) == "Failed to watch ticket! ticket_id - is already beeing watched"

      # Assert watcher not removed
      assert [watcher] = Watchers.list_ticket_watchers(ticket)
      assert watcher.id == user.id
    end

    test "when ticket now found returns error", %{conn: conn} do
      invalid_ticket_id = 0

      conn = post(conn, Routes.ticket_watcher_path(conn, :create, invalid_ticket_id))

      assert conn.status == 404
      assert conn.resp_body == "Ticket not found!"
    end

    test "without role on project returns error", %{conn: conn, ticket: ticket,
                                                        user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = post(conn, Routes.ticket_watcher_path(conn, :create, ticket))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  describe "DELETE /tickets/:ticket_id/watcher" do
    setup %{user: user, ticket: ticket} do
      {:ok, _watcher} = Watchers.create_ticket_watcher(ticket, user)
      :ok
    end

    test "removes watcher for current user", %{conn: conn, ticket: ticket} do
      conn = delete(conn, Routes.ticket_watcher_path(conn, :delete, ticket))

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, ticket.project_id, ticket)
      assert get_flash(conn, :info) == "You are no longer watching this ticket!"

      assert [] = Watchers.list_ticket_watchers(ticket)
    end

    test "when watcher already removed returns error", %{conn: conn, ticket: ticket, user: user} do
      assert {:ok, _watcher} = Watchers.delete_ticket_watcher(ticket, user)

      conn = delete(conn, Routes.ticket_watcher_path(conn, :delete, ticket))

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, ticket.project_id, ticket)
      assert get_flash(conn, :error) == "Failed to unwatch ticket! Ticket was not being watched."

      assert [] = Watchers.list_ticket_watchers(ticket)
    end

    test "when ticket now found returns error", %{conn: conn} do
      invalid_ticket_id = 0

      conn = delete(conn, Routes.ticket_watcher_path(conn, :delete, invalid_ticket_id))

      assert conn.status == 404
      assert conn.resp_body == "Ticket not found!"
    end

    test "without role on project returns error", %{conn: conn, ticket: ticket,
                                                        user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = delete(conn, Routes.ticket_watcher_path(conn, :delete, ticket))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end
end
