defmodule TicketeeWeb.StateControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Projects.State

  setup %{conn: conn} do
    user = TestFactory.insert(:user, admin: true, email: "admin@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)

    {:ok, conn: conn, user: user}
  end

  describe "GET /states" do
    setup do
      states = [
        TestFactory.insert(:state, name: "Open #{__MODULE__}"),
        TestFactory.insert(:state, name: "Awesome #{__MODULE__}"),
        TestFactory.insert(:state, name: "In Progress #{__MODULE__}"),
        TestFactory.insert(:state, name: "Closed #{__MODULE__}"),
      ]

      {:ok, states: states}
    end

    test "returns all states", %{conn: conn} do
      conn = get(conn, Routes.state_path(conn, :index))

      response = html_response(conn, 200)
      assert String.contains?(response, "Open #{__MODULE__}")
      assert String.contains?(response, "Awesome #{__MODULE__}")
      assert String.contains?(response, "In Progress #{__MODULE__}")
      assert String.contains?(response, "Closed #{__MODULE__}")
    end

    test "when logged in as regular user redirects", %{conn: conn} do
      regular_user = TestFactory.insert(:user)
      conn = sign_in_user(conn, regular_user)

      conn = get(conn, Routes.state_path(conn, :index))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end

  @valid_state_params %{
    "name" => "In Progress StateControllerTest",
    "color" => "orange",
    "order_no" => 42,
  }

  describe "POST /states" do
    test "with valid params creates new state and redirects", %{conn: conn} do
      conn = post(conn, Routes.state_path(conn, :create, state: @valid_state_params))

      assert redirected_to(conn) == Routes.state_path(conn, :index)
      assert get_flash(conn, :info) == "State has been successfully created!"

      assert [
          %State{name: "In Progress StateControllerTest",
                 color: "orange",
                 order_no: 42}
        ] = Projects.list_states()
    end

    test "with invalid params returns errors", %{conn: conn} do
      invalid_params = Map.put(@valid_state_params, "name", "    ") # blank name

      conn = post(conn, Routes.state_path(conn, :create, state: invalid_params))

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to create state!"
    end

    test "when logged in as regular user redirects", %{conn: conn} do
      regular_user = TestFactory.insert(:user)
      conn = sign_in_user(conn, regular_user)

      conn = post(conn, Routes.state_path(conn, :create, state: @valid_state_params))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end

  describe "PATCH /state/:id" do
    setup do
      state = TestFactory.insert(:state)
      {:ok, state: state}
    end

    @update_state_params %{
      "name" => "Updated Awesome #{__MODULE__}",
      "color" => "updated turquoise",
      "order_no" => 17,
    }

    test "with valid params update state and redirects", %{conn: conn, state: state} do
      conn = patch(conn, Routes.state_path(conn, :update, state), state: @update_state_params)

      assert redirected_to(conn) == Routes.state_path(conn, :index)
      assert get_flash(conn, :info) == "State has been successfully updated!"

      state = Projects.get_state(state.id)
      assert state.name == "Updated Awesome #{__MODULE__}"
      assert state.color == "updated turquoise"
      assert state.order_no == 17
    end

    test "with invalid params returns errors", %{conn: conn, state: state} do
      invalid_params = Map.put(@update_state_params, "name", "    ") # blank name

      conn = patch(conn, Routes.state_path(conn, :update, state), state: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to update state!"
    end

    test "when logged in as regular user redirects", %{conn: conn, state: state} do
      regular_user = TestFactory.insert(:user)
      conn = sign_in_user(conn, regular_user)

      conn = patch(conn, Routes.state_path(conn, :update, state), state: @update_state_params)

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end

  describe "DELETE /states/:id" do
    setup do
      state = TestFactory.insert(:state)
      {:ok, state: state}
    end

    test "deletes state and redirects", %{conn: conn, state: state} do
      conn = delete(conn, Routes.state_path(conn, :delete, state))

      assert redirected_to(conn) == Routes.state_path(conn, :index)
      assert get_flash(conn, :info) == "State has been successfully deleted!"

      refute Projects.get_state(state.id)
    end

    test "when state is attached to a comment", %{conn: conn, state: state} do
      project = TestFactory.insert(:project)
      ticket = TestFactory.insert(:ticket, project: project)
      TestFactory.insert(:comment, state: state, ticket: ticket)

      conn = delete(conn, Routes.state_path(conn, :delete, state))

      assert redirected_to(conn) == Routes.state_path(conn, :index)
      assert get_flash(conn, :error) == "Failed to delete state! Cannot delete while comments have this state."

      assert Projects.get_state(state.id)
    end

    test "when logged in as regular user redirects", %{conn: conn, state: state} do
      regular_user = TestFactory.insert(:user)
      conn = sign_in_user(conn, regular_user)

      conn = delete(conn, Routes.state_path(conn, :delete, state))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end

  describe "PATCH /states/:id/default" do
    test "sets desired state as default", %{conn: conn} do
      state = TestFactory.insert(:state)
      current_default = TestFactory.insert(:state, default: true)

      conn = patch(conn, Routes.state_path(conn, :make_default, state))

      assert redirected_to(conn) == Routes.state_path(conn, :index)
      assert get_flash(conn, :info) == "State has been successfully set as default!"

      assert Projects.get_state(state.id).default
      refute Projects.get_state(current_default.id).default
    end

    test "when logged in as regular user redirects", %{conn: conn} do
      state = TestFactory.insert(:state)
      regular_user = TestFactory.insert(:user)
      conn = sign_in_user(conn, regular_user)

      conn = patch(conn, Routes.state_path(conn, :make_default, state))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end
end
