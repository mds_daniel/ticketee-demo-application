defmodule TicketeeWeb.TicketSearchControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles

  setup do
    project = TestFactory.insert(:project)
    {:ok, project: project}
  end

  setup %{conn: conn, project: project} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    conn = sign_in_user(conn, user)

    {:ok, conn: conn, user: user}
  end

  describe "GET /tickets/search" do
    setup %{project: project, user: user} do
      other_project = TestFactory.insert(:project)
      {:ok, _role} = Roles.grant_role(user, :viewer, other_project)

      state_opened = TestFactory.insert(:state, name: "Opened #{__MODULE__}")
      state_closed = TestFactory.insert(:state, name: "Closed #{__MODULE__}")

      ticket_a = TestFactory.insert(:ticket,
        project: project,
        state: state_opened,
        title: "Ticket A #{__MODULE__}")

      ticket_b = TestFactory.insert(:ticket,
        state: state_opened,
        project: other_project,
        title: "Ticket B #{__MODULE__}")

      ticket_c = TestFactory.insert(:ticket,
        project: project,
        state: state_closed,
        title: "Ticket C #{__MODULE__}")

      ticket_d = TestFactory.insert(:ticket,
        project: other_project,
        state: state_closed,
        title: "Ticket D #{__MODULE__}")

      {:ok, other_project: other_project,
            ticket_a: ticket_a,
            ticket_b: ticket_b,
            ticket_c: ticket_c,
            ticket_d: ticket_d,
            state_opened: state_opened,
            state_closed: state_closed}
    end

    test "with no search params shows all tickets", %{conn: conn} do
      conn = get(conn, Routes.ticket_search_path(conn, :index))

      response = html_response(conn, 200)
      assert String.contains?(response, "Ticket A #{__MODULE__}")
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      assert String.contains?(response, "Ticket C #{__MODULE__}")
      assert String.contains?(response, "Ticket D #{__MODULE__}")
    end

    test "only shows tickets for projects on which roles are assigned",
        %{conn: conn, user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = get(conn, Routes.ticket_search_path(conn, :index))

      response = html_response(conn, 200)
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")

      # Belong to `other_project` for which `viewer` role still exists
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      assert String.contains?(response, "Ticket D #{__MODULE__}")
    end

    test "allows searching by ticket title", %{conn: conn} do
      # Search for `Ticket B`
      conn_b = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"ticket_title" => "Ticket B"})

      response = html_response(conn_b, 200)
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")
      refute String.contains?(response, "Ticket D #{__MODULE__}")

      # Search for `Ticket D`
      conn_d = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"ticket_title" => "Ticket D"})

      response = html_response(conn_d, 200)
      assert String.contains?(response, "Ticket D #{__MODULE__}")
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      refute String.contains?(response, "Ticket B #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")
    end

    test "allows searching by project id",
        %{conn: conn, project: project, other_project: other_project} do
      # Search for `project`
      conn_a = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"project_id" => project.id})

      response = html_response(conn_a, 200)
      assert String.contains?(response, "Ticket A #{__MODULE__}")
      assert String.contains?(response, "Ticket C #{__MODULE__}")
      refute String.contains?(response, "Ticket B #{__MODULE__}")
      refute String.contains?(response, "Ticket D #{__MODULE__}")

      # Search for `other_project`
      conn_b = get(conn, Routes.ticket_search_path(conn, :index),
       search: %{"project_id" => other_project.id})

      response = html_response(conn_b, 200)
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      assert String.contains?(response, "Ticket D #{__MODULE__}")
    end

    test "allows searching by state name", %{conn: conn} do
      # Search for `state_opened`
      conn_a = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"state_name" => "Opened #{__MODULE__}"})

      response = html_response(conn_a, 200)
      assert String.contains?(response, "Ticket A #{__MODULE__}")
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")
      refute String.contains?(response, "Ticket D #{__MODULE__}")

      # Search for `state_closed`
      conn_b = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"state_name" => "Closed #{__MODULE__}"})

      response = html_response(conn_b, 200)
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      refute String.contains?(response, "Ticket B #{__MODULE__}")
      assert String.contains?(response, "Ticket C #{__MODULE__}")
      assert String.contains?(response, "Ticket D #{__MODULE__}")
    end

    test "allows searching by tag_name",
        %{conn: conn, ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c} do
      {:ok, _tags} = Projects.create_ticket_tags(ticket_a, ["Neat #{__MODULE__}"])
      {:ok, _tags} = Projects.create_ticket_tags(ticket_b, ["Neat #{__MODULE__}",
                                                            "Awesome #{__MODULE__}"])
      {:ok, _tags} = Projects.create_ticket_tags(ticket_c, ["Awesome #{__MODULE__}"])

      # Search for `Neat`
      conn_a = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"tag_name" => "Neat #{__MODULE__}"})

      response = html_response(conn_a, 200)
      assert String.contains?(response, "Ticket A #{__MODULE__}")
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      refute String.contains?(response, "Ticket C #{__MODULE__}")
      refute String.contains?(response, "Ticket D #{__MODULE__}")


      # Search for `Awesome`
      conn_b = get(conn, Routes.ticket_search_path(conn, :index),
        search: %{"tag_name" => "Awesome #{__MODULE__}"})

      response = html_response(conn_b, 200)
      refute String.contains?(response, "Ticket A #{__MODULE__}")
      assert String.contains?(response, "Ticket B #{__MODULE__}")
      assert String.contains?(response, "Ticket C #{__MODULE__}")
      refute String.contains?(response, "Ticket D #{__MODULE__}")
    end
  end
end
