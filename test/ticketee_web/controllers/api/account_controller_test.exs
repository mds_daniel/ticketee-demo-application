defmodule TicketeeWeb.API.AccountControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory

  setup %{conn: conn} do
    {:ok, conn: set_jsonapi_content_type(conn)}
  end

  describe "GET /api/account" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "with valid token returns user details", %{conn: conn, user: user} do
      user_id = user.id
      user_email = user.email
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_account_path(conn, :show))

      assert %{
          "data" => %{
            "id" => ^user_id,
            "type" => "users",
            "attributes" => %{
              "email" => ^user_email,
            }
          }
        } = json_response(conn, 200)
    end

    test "without valid token returns unauthorized", %{conn: conn} do
      conn = get(conn, Routes.api_account_path(conn, :show))

      assert json_response(conn, 401) == %{
          "errors" => [
             %{"title" => "Unauthorized",
                "detail" => "Unauthorized",
                "status" => "401"}
            ]
        }
    end
  end
end
