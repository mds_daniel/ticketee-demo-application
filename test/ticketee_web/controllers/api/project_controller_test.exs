defmodule TicketeeWeb.API.ProjectControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles

  setup %{conn: conn} do
    {:ok, conn: set_jsonapi_content_type(conn)}
  end

  describe "GET /api/projects" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")

      project_a = TestFactory.insert(:project, name: "Project A #{__MODULE__}")
      project_b = TestFactory.insert(:project, name: "Project B #{__MODULE__}")
      project_c = TestFactory.insert(:project, name: "Project C #{__MODULE__}")

      {:ok, _role} = Roles.grant_role(user, :viewer, project_a)
      {:ok, _role} = Roles.grant_role(user, :viewer, project_c)

      {:ok, user: user,
            project_a: project_a,
            project_b: project_b,
            project_c: project_c}
    end

    test "returns projects viewable by user",
        %{conn: conn, user: user, project_a: project_a, project_c: project_c} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_project_path(conn, :index))

      assert json_response(conn, 200) == %{
          "data" => [
            %{
              "id" => to_string(project_a.id),
              "type" => "projects",
              "attributes" => %{
                "name" => project_a.name,
                "description" => project_a.description,
                "slug" => project_a.slug,
                "inserted_at" => DateTime.to_iso8601(project_a.inserted_at),
              },
              "relationships" => %{},
            },
            %{
              "id" => to_string(project_c.id),
              "type" => "projects",
              "attributes" => %{
                "name" => project_c.name,
                "description" => project_c.description,
                "slug" => project_c.slug,
                "inserted_at" => DateTime.to_iso8601(project_c.inserted_at),
              },
              "relationships" => %{},
            }
          ],
          "included" => [],
        }
    end
  end

  describe "GET /api/projects/:id" do
    setup %{conn: conn} do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      project = TestFactory.insert(:project, name: "Project A #{__MODULE__}")

      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      {:ok, conn: conn, user: user, project: project}
    end

    test "returns project details", %{conn: conn, project: project} do
      conn = get(conn, Routes.api_project_path(conn, :show, project))

      assert json_response(conn, 200) == %{
          "data" => %{
            "id" => to_string(project.id),
            "type" => "projects",
            "attributes" => %{
              "name" => project.name,
              "description" => project.description,
              "slug" => project.slug,
              "inserted_at" => DateTime.to_iso8601(project.inserted_at),
            },
            "relationships" => %{},
          },
          "included" => [],
        }
    end

    test "without view permission returns forbidden",
        %{conn: conn, user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = get(conn, Routes.api_project_path(conn, :show, project))

      assert json_response(conn, 403) == %{
          "errors" => [
            %{"title" => "Forbidden",
              "detail" => "You are not allowed to perform that action!",
              "status" => "403"}
            ]
          }
    end

    test "when project is not found", %{conn: conn, project: project} do
      {:ok, deleted_project} = Projects.delete_project(project)

      conn = get(conn, Routes.api_project_path(conn, :show, deleted_project))

      assert json_response(conn, 404) == %{
          "errors" => [
            %{"title" => "Not Found",
              "detail" => "Project not found",
              "status" => "404"}
            ]
          }
    end
  end
end
