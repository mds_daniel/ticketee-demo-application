defmodule TicketeeWeb.API.CommentControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Roles

  setup %{conn: conn} do
    {:ok, conn: set_jsonapi_content_type(conn)}
  end

  setup do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    project = TestFactory.insert(:project)

    {:ok, user: user, project: project}
  end

  describe "GET /api/tickets/:ticket_id/comments" do
    setup %{project: project, user: user} do
      ticket = TestFactory.insert(:ticket, project: project)
      other_ticket = TestFactory.insert(:ticket, project: project)
      other_user = TestFactory.insert(:user, email: "other@#{__MODULE__}.com")

      comment_a = TestFactory.insert(:comment,
        user: user,
        ticket: ticket,
        description: "Comment A #{__MODULE__}")

      comment_b = TestFactory.insert(:comment,
        user: user,
        ticket: other_ticket,
        description: "Comment B #{__MODULE__}")

      comment_c = TestFactory.insert(:comment,
        user: other_user,
        ticket: ticket,
        description: "Comment C #{__MODULE__}")

      {:ok, ticket: ticket, other_ticket: other_ticket, other_user: other_user,
            comment_a: comment_a, comment_b: comment_b, comment_c: comment_c}
    end

    test "returns comments for ticket", %{conn: conn, user: user, project: project,
                                          ticket: ticket, other_user: other_user,
                                          comment_a: comment_a, comment_c: comment_c} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_comment_path(conn, :index, ticket))

      assert json_response(conn, 200) == %{
        "data" => [
          %{"id" => to_string(comment_a.id),
            "type" => "comments",
            "attributes" => %{
              "description" => comment_a.description,
              "inserted_at" => DateTime.to_iso8601(comment_a.inserted_at),
            },
            "relationships" => %{
              "ticket" => %{"data" => %{"type" => "tickets", "id" => to_string(ticket.id)}},
              "user" => %{"data" => %{"type" => "users", "id" => to_string(user.id)}},
            },
          },
          %{"id" => to_string(comment_c.id),
            "type" => "comments",
            "attributes" => %{
              "description" => comment_c.description,
              "inserted_at" => DateTime.to_iso8601(comment_c.inserted_at),
            },
            "relationships" => %{
              "ticket" => %{"data" => %{"type" => "tickets", "id" => to_string(ticket.id)}},
              "user" => %{"data" => %{"type" => "users", "id" => to_string(other_user.id)}},
            },
          }
        ],
        "included" => [
          %{"id" => to_string(user.id),
            "type" => "users",
            "attributes" => %{
              "email" => user.email,
            },
            "relationships" => %{}
          },
          %{"id" => to_string(ticket.id),
            "type" => "tickets",
            "attributes" => %{
              "title" => ticket.title,
              "description" => ticket.description,
              "comment_count" => 0,
              "inserted_at" => DateTime.to_iso8601(ticket.inserted_at),
            },
            "relationships" => %{}
          },
          %{"id" => to_string(other_user.id),
            "type" => "users",
            "attributes" => %{
              "email" => other_user.email,
            },
            "relationships" => %{}
          },
        ],
      }
    end

    test "when no roles assigned returns forbidden", %{conn: conn,
                                                       user: user,
                                                       ticket: ticket} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_comment_path(conn, :index, ticket))

      assert json_response(conn, 403) == %{
          "errors" => [
            %{"title" => "Forbidden",
              "detail" => "You are not allowed to perform that action!",
              "status" => "403"}
            ]
          }
    end

    test "when not logged in returns unauthorized", %{conn: conn,
                                                      ticket: ticket} do
      conn = get(conn, Routes.api_ticket_comment_path(conn, :index, ticket))

      assert json_response(conn, 401) == %{
          "errors" => [
            %{"title" => "Unauthorized",
              "detail" => "Unauthorized",
              "status" => "401"}
            ]
          }
    end
  end

  describe "GET /api/comments/:id" do
    setup %{project: project, user: user} do
      ticket = TestFactory.insert(:ticket, project: project)

      comment = TestFactory.insert(:comment,
        user: user,
        ticket: ticket,
        description: "Comment A #{__MODULE__}")

      {:ok, ticket: ticket, comment: comment}
    end

    test "returns details for comment", %{conn: conn, user: user, project: project,
                                          ticket: ticket, comment: comment} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_comment_path(conn, :show, comment))

      assert json_response(conn, 200) == %{
          "data" => %{
            "id" => to_string(comment.id),
            "type" => "comments",
            "attributes" => %{
              "description" => comment.description,
              "inserted_at" => DateTime.to_iso8601(comment.inserted_at),
            },
            "relationships" => %{
              "ticket" => %{"data" => %{"type" => "tickets", "id" => to_string(ticket.id)}},
              "user" => %{"data" => %{"type" => "users", "id" => to_string(user.id)}},
            },
          },
          "included" => [
            %{"id" => to_string(user.id),
              "type" => "users",
              "attributes" => %{
                "email" => user.email,
              },
              "relationships" => %{}
            },
            %{"id" => to_string(ticket.id),
              "type" => "tickets",
              "attributes" => %{
                "title" => ticket.title,
                "description" => ticket.description,
                "comment_count" => 0,
                "inserted_at" => DateTime.to_iso8601(ticket.inserted_at),
              },
              "relationships" => %{}
            },
          ]
        }
    end

    test "when no roles assigned returns forbidden",
        %{conn: conn, user: user, comment: comment} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_comment_path(conn, :show, comment))

      assert json_response(conn, 403) == %{
          "errors" => [
            %{"title" => "Forbidden",
              "detail" => "You are not allowed to perform that action!",
              "status" => "403"}
            ]
          }
    end

    test "when not logged in returns unauthorized", %{conn: conn,
                                                      comment: comment} do
      conn = get(conn, Routes.api_comment_path(conn, :show, comment))

      assert json_response(conn, 401) == %{
          "errors" => [
            %{"title" => "Unauthorized",
              "detail" => "Unauthorized",
              "status" => "401"}
            ]
          }
    end
  end
end
