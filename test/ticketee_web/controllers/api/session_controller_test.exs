defmodule TicketeeWeb.API.SessionControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias TicketeeWeb.APITokens

  setup %{conn: conn} do
    conn = put_req_header(conn, "content-type", JSONAPI.mime_type())
    {:ok, conn: conn}
  end

  describe "POST /api/sessions" do
    setup do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      {:ok, user: user}
    end

    test "with valid credentials returns API token", %{conn: conn, user: user} do
      user_id = user.id
      user_email = "test_user@#{__MODULE__}.com"
      credentials = %{
        type: "sessions",
        attributes: %{
          email: user_email,
          password: "super secret",
        }
      }

      conn = post(conn, Routes.api_session_path(conn, :create), %{data: credentials})

      assert response_content_type_header(conn) == jsonapi_mime_type()
      assert %{
          "data" => %{
            "type" => "users",
            "id" => ^user_id,
            "attributes" => %{"email" => ^user_email},
          },
          "meta" => %{"access_token" => access_token }
        } = json_response(conn, 201)

      assert {:ok, user_id} == APITokens.verify(access_token)
    end

    test "with invalid credentials returns unauthorized", %{conn: conn} do
      user_email = "test_user@#{__MODULE__}.com"
      invalid_credentials = %{
        type: "sessions",
        attributes: %{
          email: user_email,
          password: "invalid password",
        }
      }


      conn = post(conn, Routes.api_session_path(conn, :create), %{data: invalid_credentials})

      assert json_response(conn, 401) == %{
          "errors" => [
            %{"title" => "Unauthorized",
              "detail" => "Unauthorized",
              "status" => "401"}
          ]
        }
    end

    test "without credentials returns missing data error", %{conn: conn} do
      conn = post(conn, Routes.api_session_path(conn, :create))

      assert %{
          "errors" => [
            %{"title" => "Missing data parameter",
              "status" => "400"}
          ]
        } = json_response(conn, 400)
    end
  end
end
