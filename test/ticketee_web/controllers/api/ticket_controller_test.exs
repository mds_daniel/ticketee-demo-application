defmodule TicketeeWeb.API.TicketControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Roles
  alias Ticketee.Projects

  setup %{conn: conn} do
    {:ok, conn: set_jsonapi_content_type(conn)}
  end

  setup do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    project = TestFactory.insert(:project)

    {:ok, user: user, project: project}
  end

  describe "GET /api/tickets" do
    setup %{project: project} do
      other_project = TestFactory.insert(:project)

      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true, order_no: 1)
      state_in_progress = TestFactory.insert(:state, name: "In Progress #{__MODULE__}", order_no: 2)

      tag_awesome = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      tag_neat = TestFactory.insert(:tag, name: "Neat #{__MODULE__}")

      ticket_a = TestFactory.insert(:ticket,
        project: project,
        title: "Ticket A #{__MODULE__}",
        state: state_open,
        tags: [tag_awesome, tag_neat])

      ticket_b = TestFactory.insert(:ticket,
        project: other_project,
        title: "Ticket B #{__MODULE__}",
        state: state_open,
        tags: [tag_awesome, tag_neat])

      ticket_c = TestFactory.insert(:ticket,
        project: project,
        title: "Ticket C #{__MODULE__}",
        state: state_in_progress,
        tags: [tag_neat])

      {:ok, other_project: other_project,
            state_open: state_open, state_in_progress: state_in_progress,
            tag_awesome: tag_awesome, tag_neat: tag_neat,
            ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c}
    end

    test "returns tickets for projects with roles", %{conn: conn, user: user, project: project,
                                                      ticket_a: ticket_a, ticket_c: ticket_c} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_path(conn, :index))

      ticket_a_id = to_string(ticket_a.id)
      ticket_c_id = to_string(ticket_c.id)

      assert %{
          "data" => [
            %{"id" => ^ticket_a_id, "type" => "tickets"},
            %{"id" => ^ticket_c_id, "type" => "tickets"},
          ]
        } = json_response(conn, 200)
    end

    test "when no project roles are assigned returns no tickets", %{conn: conn, user: user} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_path(conn, :index))

      assert json_response(conn, 200) == %{
        "data" => [],
        "included" => [],
      }
    end

    test "when search filters are used returns matching tickets",
        %{conn: conn, user: user, project: project, other_project: other_project,
          ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      {:ok, _role} = Roles.grant_role(user, :viewer, other_project)
      conn = sign_in_api_user(conn, user)

      ticket_a_id = to_string(ticket_a.id)
      ticket_b_id = to_string(ticket_b.id)
      ticket_c_id = to_string(ticket_c.id)

      # Search by project_id `other_project`
      result = get(conn, Routes.api_ticket_path(conn, :index), filter: %{project_id: other_project.id})

      assert %{
          "data" => [
            %{"id" => ^ticket_b_id, "type" => "tickets"},
          ]
        } = json_response(result, 200)

      # Search by state `Open`
      result = get(conn, Routes.api_ticket_path(conn, :index), filter: %{state_name: "Open #{__MODULE__}"})

      assert %{
          "data" => [
            %{"id" => ^ticket_a_id, "type" => "tickets"},
            %{"id" => ^ticket_b_id, "type" => "tickets"},
          ]
        } = json_response(result, 200)

      # Search by state `In progress`
      result = get(conn, Routes.api_ticket_path(conn, :index), filter: %{state_name: "In Progress #{__MODULE__}"})

      assert %{
          "data" => [
            %{"id" => ^ticket_c_id, "type" => "tickets"},
          ]
        } = json_response(result, 200)

      # Search by project_id `project` and tag_name `Awesome`
      result = get(conn, Routes.api_ticket_path(conn, :index), filter: %{project_id: project.id, tag_name: "Awesome #{__MODULE__}"})

      assert %{
          "data" => [
            %{"id" => ^ticket_a_id, "type" => "tickets"},
          ]
        } = json_response(result, 200)
    end
  end

  describe "GET /api/tickets/:id" do
    setup %{project: project} do
      author = TestFactory.insert(:user)
      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true, order_no: 1)
      tag_awesome = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")

      ticket = TestFactory.insert(:ticket,
        author: author,
        project: project,
        title: "Ticket A #{__MODULE__}",
        state: state_open,
        tags: [tag_awesome])

      {:ok, author: author, ticket: ticket,
            state_open: state_open, tag_awesome: tag_awesome}
    end

    test "returns ticket details", %{conn: conn, user: user,
                                     project: project, author: author, ticket: ticket,
                                     state_open: state_open, tag_awesome: tag_awesome} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_path(conn, :show, ticket))

      assert json_response(conn, 200) == %{
        "data" => %{
          "id" => to_string(ticket.id),
          "type" => "tickets",
          "attributes" => %{
            "title" => ticket.title,
            "description" => ticket.description,
            "comment_count" => 0,
            "inserted_at" => DateTime.to_iso8601(ticket.inserted_at),
          },
          "relationships" => %{
            "project" => %{
              "data" => %{"type" => "projects", "id" => to_string(project.id)},
            },
            "author" => %{
              "data" => %{"type" => "users", "id" => to_string(author.id)},
            },
            "state" => %{
              "data" => %{"type" => "states", "id" => to_string(state_open.id)},
            },
            "tags" => %{
              "data" => [
                %{"type" => "tags", "id" => to_string(tag_awesome.id)},
              ]
            },
          },
        },
        "included" => [
          %{"id" => to_string(project.id),
            "type" => "projects",
            "attributes" => %{
              "name" => project.name,
              "description" => project.description,
              "slug" => project.slug,
              "inserted_at" => DateTime.to_iso8601(project.inserted_at),
            },
            "relationships" => %{},
          },
          %{"id" => to_string(author.id),
            "type" => "users",
            "attributes" => %{
              "email" => author.email,
            },
            "relationships" => %{},
          },
          %{"id" => to_string(state_open.id),
            "type" => "states",
            "attributes" => %{
              "name" => "Open #{__MODULE__}",
              "color" => state_open.color,
              "order_no" => 1,
              "default" => true,
            },
            "relationships" => %{},
          },
          %{"id" => to_string(tag_awesome.id),
            "type" => "tags",
            "attributes" => %{
              "name" => "Awesome #{__MODULE__}",
            },
            "relationships" => %{},
          },
        ]
      }
    end

    test "when no roles assigned for project returns forbidden",
        %{conn: conn, user: user, ticket: ticket} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_path(conn, :show, ticket))

      assert json_response(conn, 403) == %{
          "errors" => [
            %{"title" => "Forbidden",
              "detail" => "You are not allowed to perform that action!",
              "status" => "403"}
            ]
          }
    end

    test "when ticket missing returns not found",
        %{conn: conn, user: user, ticket: ticket} do
      {:ok, _} = Projects.delete_ticket(ticket)

      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_ticket_path(conn, :show, ticket))

      assert json_response(conn, 404) == %{
          "errors" => [
            %{"title" => "Not Found",
              "detail" => "Ticket not found",
              "status" => "404"}
            ]
          }
    end
  end

  describe "GET /api/projects/:id/tickets" do
    setup %{project: project, user: user} do
      other_project = TestFactory.insert(:project)

      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true, order_no: 1)
      state_in_progress = TestFactory.insert(:state, name: "In Progress #{__MODULE__}", order_no: 2)

      tag_awesome = TestFactory.insert(:tag, name: "Awesome #{__MODULE__}")
      tag_neat = TestFactory.insert(:tag, name: "Neat #{__MODULE__}")

      ticket_a = TestFactory.insert(:ticket,
        author: user,
        project: project,
        title: "Ticket A #{__MODULE__}",
        state: state_open,
        tags: [tag_awesome, tag_neat])

      ticket_b = TestFactory.insert(:ticket,
        project: other_project,
        title: "Ticket B #{__MODULE__}",
        state: state_open,
        tags: [tag_awesome, tag_neat])

      ticket_c = TestFactory.insert(:ticket,
        project: project,
        title: "Ticket C #{__MODULE__}",
        state: state_in_progress,
        tags: [tag_neat])

      {:ok, state_open: state_open, state_in_progress: state_in_progress,
            tag_awesome: tag_awesome, tag_neat: tag_neat,
            ticket_a: ticket_a, ticket_b: ticket_b, ticket_c: ticket_c}
    end

    test "returns tickets for project", %{conn: conn, user: user, project: project,
                                          state_open: state_open, state_in_progress: state_in_progress,
                                          tag_awesome: tag_awesome, tag_neat: tag_neat,
                                          ticket_a: ticket_a, ticket_c: ticket_c} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_project_ticket_path(conn, :index_for_project, project))

      assert json_response(conn, 200) == %{
          "data" => [
            %{"id" => to_string(ticket_a.id),
              "type" => "tickets",
              "attributes" => %{
                "title" => ticket_a.title,
                "description" => ticket_a.description,
                "comment_count" => 0,
                "inserted_at" => DateTime.to_iso8601(ticket_a.inserted_at),
              },
              "relationships" => %{
                "author" => %{
                  "data" => %{
                    "id" => to_string(user.id),
                    "type" => "users"
                  },
                },
                "project" => %{
                  "data" => %{
                    "id" => to_string(project.id),
                    "type" => "projects"
                  },
                },
                "state" => %{
                  "data" => %{
                    "id" => to_string(state_open.id),
                    "type" => "states"
                  }
                },
                "tags" => %{
                  "data" => [
                    %{"id" => to_string(tag_awesome.id), "type" => "tags"},
                    %{"id" => to_string(tag_neat.id), "type" => "tags"},
                  ]
                }
              }
            },
            %{"id" => to_string(ticket_c.id),
              "type" => "tickets",
              "attributes" => %{
                "title" => ticket_c.title,
                "description" => ticket_c.description,
                "comment_count" => 0,
                "inserted_at" => DateTime.to_iso8601(ticket_c.inserted_at),
              },
              "relationships" => %{
                "project" => %{
                  "data" => %{
                    "id" => to_string(project.id),
                    "type" => "projects"
                  },
                },
                "state" => %{
                  "data" => %{
                    "id" => to_string(state_in_progress.id),
                    "type" => "states"
                  }
                },
                "tags" => %{
                  "data" => [
                    %{"id" => to_string(tag_neat.id), "type" => "tags"},
                  ]
                }
              }
            },
          ],
          "included" => [
            %{"id" => to_string(project.id),
              "type" => "projects",
              "attributes" => %{
                "name" => project.name,
                "description" => project.description,
                "slug" => project.slug,
                "inserted_at" => DateTime.to_iso8601(project.inserted_at),
              },
              "relationships" => %{},
            },
            %{"id" => to_string(user.id),
              "type" => "users",
              "attributes" => %{
                "email" => user.email,
              },
              "relationships" => %{},
            },
            %{"id" => to_string(state_open.id),
              "type" => "states",
              "relationships" => %{},
              "attributes" => %{
                "name" => "Open #{__MODULE__}",
                "color" => state_open.color,
                "order_no" => 1,
                "default" => true,
              }
            },
            %{"id" => to_string(tag_awesome.id),
              "type" => "tags",
              "relationships" => %{},
              "attributes" => %{
                "name" => "Awesome #{__MODULE__}"
              },
            },
            %{"id" => to_string(tag_neat.id),
              "type" => "tags",
              "relationships" => %{},
              "attributes" => %{
                "name" => "Neat #{__MODULE__}"
              },
            },
            %{"id" => to_string(state_in_progress.id),
              "type" => "states",
              "relationships" => %{},
              "attributes" => %{
                "name" => "In Progress #{__MODULE__}",
                "color" => state_in_progress.color,
                "order_no" => 2,
                "default" => false,
              }
            },
          ]
        }
    end

    test "when no roles assigned for project returns forbidden",
        %{conn: conn, user: user, project: project} do
      conn = sign_in_api_user(conn, user)

      conn = get(conn, Routes.api_project_ticket_path(conn, :index_for_project, project))

      assert json_response(conn, 403) == %{
          "errors" => [
            %{"title" => "Forbidden",
              "detail" => "You are not allowed to perform that action!",
              "status" => "403"}
            ]
          }
    end
  end
end
