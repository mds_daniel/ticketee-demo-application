defmodule TicketeeWeb.TicketControllerTest do
  use TicketeeWeb.ConnCase, async: true
  use Bamboo.Test

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles
  alias Ticketee.Notifications.Watchers

  setup do
    project = TestFactory.insert(:project)
    {:ok, project: project}
  end

  setup %{conn: conn, project: project} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, _role} = Roles.grant_role(user, :viewer, project)

    conn = sign_in_user(conn, user)

    {:ok, conn: conn, user: user}
  end

  describe "GET /projects/:project_id/tickets/:id" do
    test "shows ticket details", %{conn: conn, project: project} do
      ticket = TestFactory.insert(:ticket, project_id: project.id)

      conn = get(conn, Routes.project_ticket_path(conn, :show, project, ticket))
      response = html_response(conn, 200)

      assert String.contains?(response, project.name)
      assert String.contains?(response, ticket.title)
      assert String.contains?(response, ticket.description)
    end

    test "handles missing tickets by redirecting", %{conn: conn, project: project} do
      non_existing_ticket_id = 0

      conn = get(conn, Routes.project_ticket_path(conn, :show, project, non_existing_ticket_id))

      assert redirected_to(conn) == Routes.project_path(conn, :show, project)
      assert get_flash(conn, :error) == "The ticket you were looking for could not be found!"
    end

    test "handles missing projects by redirecting", %{conn: conn, project: project} do
      ticket = TestFactory.insert(:ticket, project_id: project.id)
      non_existing_project_id = 0

      conn = get(conn, Routes.project_ticket_path(conn, :show, non_existing_project_id, ticket))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "The project you were looking for could not be found!"
    end
  end

  @valid_ticket_params %{
    "title" => "Mow lawn #{__MODULE__}",
    "description" => "Clean yard and mow lawn",
  }

  describe "POST /projects/:project_id/tickets" do
    setup %{user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :editor, project)
      TestFactory.insert(:state, name: "Open #{__MODULE__}", default: true)

      :ok
    end

    test "creates new ticket and redirects", %{conn: conn, project: project, user: user} do
      conn = post(conn, Routes.project_ticket_path(conn, :create, project), ticket: @valid_ticket_params)

      assert %{id: ticket_id} = redirected_params(conn)
      ticket = Projects.get_ticket_for(project, ticket_id)

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, project, ticket)
      assert get_flash(conn, :info) == "Ticket has been successfully created!"

      ## Assert ticket was saved with correct attributes
      assert ticket.title == "Mow lawn #{__MODULE__}"
      assert ticket.description == "Clean yard and mow lawn"

      ticket = Projects.with_ticket_author(ticket)
      assert ticket.author == user

      ticket = Projects.with_state(ticket)
      assert ticket.state.name == "Open #{__MODULE__}"

    end

    test "when create ticket params are invalid returns errors", %{conn: conn, project: project} do
      invalid_params = Map.put(@valid_ticket_params, "title", "   ") # blank title

      conn = post(conn, Routes.project_ticket_path(conn, :create, project), ticket: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to create ticket!"
    end

    test "ticket author is registered as watcher", %{conn: conn, project: project, user: user} do
      conn = post(conn, Routes.project_ticket_path(conn, :create, project), ticket: @valid_ticket_params)

      assert %{id: ticket_id} = redirected_params(conn)
      ticket = Projects.get_ticket_for(project, ticket_id)

      assert Watchers.list_ticket_watcher_addresses(ticket) == [{user.id, user.email}]
    end
  end

  describe "PATCH /projects/:project_id/tickets/:id" do
    setup %{project: project, user: user} do
      ticket = TestFactory.insert(:ticket, project: project, author: user)
      {:ok, _role} = Roles.grant_role(user, :editor, project)

      {:ok, ticket: ticket}
    end

    @update_ticket_params %{
      "title" => "Updated ticket title #{__MODULE__}",
      "description" => "Updated ticket description #{__MODULE__}",
    }

    test "updates ticket and redirects", %{conn: conn, project: project, ticket: ticket} do
      conn = patch(conn, Routes.project_ticket_path(conn, :update, project, ticket),
        ticket: @update_ticket_params)

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, project, ticket)
      assert get_flash(conn, :info) == "Ticket has been successfully updated!"

      ## Assert ticket was updated with correct attributes
      ticket = Projects.get_ticket_for(project, ticket.id)

      assert ticket.title == "Updated ticket title #{__MODULE__}"
      assert ticket.description == "Updated ticket description #{__MODULE__}"
    end

    test "when update ticket params are invalid returns errors",
        %{conn: conn, project: project, ticket: ticket} do
      invalid_params = Map.put(@update_ticket_params, "title", "   ") # blank title

      conn = patch(conn, Routes.project_ticket_path(conn, :update, project, ticket),
        ticket: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to update ticket!"
    end
  end

  describe "DELETE /projects/:project_id/tickets/:id" do
    setup %{project: project, user: user} do
      ticket = TestFactory.insert(:ticket, project: project)
      {:ok, _user} = Roles.grant_role(user, :manager, project)

      {:ok, ticket: ticket}
    end

    test "deletes ticket and redirects", %{conn: conn, project: project, ticket: ticket} do
      conn = delete(conn, Routes.project_ticket_path(conn, :delete, project, ticket))

      assert redirected_to(conn) == Routes.project_path(conn, :show, project)
      assert get_flash(conn, :info) == "Ticket has been successfully deleted!"

      ## Assert ticket has been deleted from db
      assert Projects.get_ticket(ticket.id) == nil
    end
  end
end
