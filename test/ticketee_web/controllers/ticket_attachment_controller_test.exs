defmodule TicketeeWeb.TicketAttachmentControllerTest do
  use TicketeeWeb.ConnCase, async: true
  import Ticketee.TestFixtureFiles, only: [fixture_file_upload: 1]

  alias Ticketee.TestFactory
  alias Ticketee.Attachments
  alias Ticketee.Roles

  @attachment_filename "speed.txt"
  @attachment_test_file fixture_file_upload(@attachment_filename)

  setup %{conn: conn} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)

    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project_id: project.id, author_id: user.id)

    {:ok, conn: conn, user: user, ticket: ticket, project: project}
  end

  describe "GET /tickets/:ticket_id/attachments/:filename" do
    test "returns file attachment", %{conn: conn, ticket: ticket,
                                      user: user, project: project} do
      # Preconditions
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      {:ok, _attachment} = add_ticket_attachment(ticket, @attachment_test_file)

      conn = get(conn, Routes.ticket_attachment_path(conn, :show, ticket, @attachment_filename))

      assert conn.status == 200
      assert conn.resp_body == "The blink tag can blink faster if you use the speed=\"hyper\" attribute.\n"
      assert get_resp_header(conn, "content-type") == ["text/plain"]
      assert get_resp_header(conn, "content-disposition") == ["attachment; filename=\"speed.txt\""]
    end

    test "when user does not have project role", %{conn: conn, ticket: ticket} do
      # Preconditions
      {:ok, _attachment} = add_ticket_attachment(ticket, @attachment_test_file)

      conn = get(conn, Routes.ticket_attachment_path(conn, :show, ticket, @attachment_filename))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end

    test "when attachment does not exist", %{conn: conn, ticket: ticket,
                                             user: user, project: project} do
      # Preconditions
      {:ok, _role} = Roles.grant_role(user, :viewer, project)

      conn = get(conn, Routes.ticket_attachment_path(conn, :show, ticket, @attachment_filename))

      assert conn.status == 404
      assert conn.resp_body == "Attachment not found!"
    end

    test "when attachment filename is invalid", %{conn: conn, ticket: ticket,
                                                  user: user, project: project} do
      # Preconditions
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      {:ok, _attachment} = add_ticket_attachment(ticket, @attachment_test_file)

      conn = get(conn, Routes.ticket_attachment_path(conn, :show, ticket, "invalid.txt"))

      assert conn.status == 404
      assert conn.resp_body == "Attachment not found!"
    end

    test "when invalid ticket_id is requested", %{conn: conn} do
      invalid_ticket_id = 0

      conn = get(conn, Routes.ticket_attachment_path(conn, :show, invalid_ticket_id, @attachment_filename))

      assert conn.status == 404
      assert conn.resp_body == "Ticket not found!"
    end
  end

  describe "POST /tickets/:ticket_id/attachments" do
    @valid_attachment_params %{
      "description" => "Some nice attachment description",
      "file" => @attachment_test_file,
    }

    setup %{user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :editor, project)
      :ok
    end

    test "with valid params saves new attachment and redirects",
        %{conn: conn, ticket: ticket, project: project} do
      conn = post(conn, Routes.ticket_attachment_path(conn, :create, ticket),
          ticket_attachment: @valid_attachment_params)

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, project, ticket)
      assert get_flash(conn, :info) == "Ticket attachment has been successfully created!"
    end

    test "with invalid params returns errors", %{conn: conn, ticket: ticket} do
      invalid_file = fixture_file_upload("something.invalid")
      invalid_params = Map.put(@valid_attachment_params, "file", invalid_file)

      conn = post(conn, Routes.ticket_attachment_path(conn, :create, ticket),
        ticket_attachment: invalid_params)

      assert html_response(conn, 200) =~ html_escape("invalid file extension")
      assert get_flash(conn, :error) == "Failed to create attachment!"
    end

    test "without project role returns forbidden", %{conn: conn, user: user,
                                                     project: project, ticket: ticket} do
      {:ok, _} = Roles.revoke_role(user, project)

      conn = post(conn, Routes.ticket_attachment_path(conn, :create, ticket),
          ticket_attachment: @valid_attachment_params)

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  describe "DELETE /tickets/:ticket_id/attachments/:filename" do
    setup %{user: user, project: project, ticket: ticket} do
      assert {:ok, _role} = Roles.grant_role(user, :manager, project)
      assert {:ok, attachment} = add_ticket_attachment(ticket, @attachment_test_file)

      {:ok, attachment: attachment}
    end

    test "removes attachment and redirects", %{conn: conn, ticket: ticket, project: project, attachment: attachment} do
      conn = delete(conn, Routes.ticket_attachment_path(conn, :delete, ticket, attachment.file))

      assert redirected_to(conn) == Routes.project_ticket_path(conn, :show, project, ticket)
      assert get_flash(conn, :info) == "Ticket attachment has been successfully deleted!"
    end

    test "when attachment is missing", %{conn: conn, ticket: ticket} do
      conn = delete(conn, Routes.ticket_attachment_path(conn, :delete, ticket, "missing.txt"))

      assert conn.status == 404
      assert conn.resp_body == "Attachment not found!"
    end

    test "without project role returns forbidden",
        %{conn: conn, user: user, project: project, ticket: ticket, attachment: attachment} do
      {:ok, _} = Roles.revoke_role(user, project)

      conn = delete(conn, Routes.ticket_attachment_path(conn, :delete, ticket, attachment.file))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  ## Helpers

  def add_ticket_attachment(ticket, attachment) do
    Attachments.create_ticket_attachment(ticket, %{file: attachment})
  end
end
