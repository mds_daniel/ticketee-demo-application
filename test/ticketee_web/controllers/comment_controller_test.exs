defmodule TicketeeWeb.CommentControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Roles
  alias Ticketee.Projects
  alias Ticketee.Notifications.Watchers

  setup %{conn: conn} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)

    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    {:ok, conn: conn, user: user, ticket: ticket, project: project}
  end

  describe "GET /tickets/:ticket_id/comments" do
    setup %{user: user, ticket: ticket} do
      other = TestFactory.insert(:user, email: "other@#{__MODULE__}.com")

      TestFactory.insert(:comment, ticket: ticket, user: user, description: "Neat comment C description")
      TestFactory.insert(:comment, ticket: ticket, user: user, description: "Neat comment A description")
      TestFactory.insert(:comment, ticket: ticket, user: other, description: "Neat comment B description")

      :ok
    end

    test "returns list of comments with usernames", %{conn: conn, user: user,
                                                      project: project, ticket: ticket} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)

      conn = get(conn, Routes.ticket_comment_path(conn, :index, ticket))

      response = html_response(conn, 200)
      assert String.contains?(response, "Neat comment A description")
      assert String.contains?(response, "Neat comment B description")
      assert String.contains?(response, "Neat comment C description")
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket} do
      conn = get(conn, Routes.ticket_comment_path(conn, :index, ticket))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  describe "POST /tickets/:ticket_id/comments" do
    @valid_comment_params %{
      "description" => "A nice comment description about #{__MODULE__}",
    }

    setup %{user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      :ok
    end

    test "with valid params saves new comment and redirects", %{conn: conn, ticket: ticket} do
      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket), comment: @valid_comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) =~ "Comment has been successfully created!"

      assert [comment] = Projects.list_ticket_comments(ticket)
      assert comment.description == "A nice comment description about #{__MODULE__}"
    end

    test "with invalid parrams returns errors", %{conn: conn, ticket: ticket} do
      invalid_params = Map.put(@valid_comment_params, "description", "   ")

      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket), comment: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to create comment!"
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket,
                                                     user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket), comment: @valid_comment_params)

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end

    test "managers can set comment and ticket state",
        %{conn: conn, ticket: ticket, user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :manager, project)
      state = TestFactory.insert(:state, name: "Working on it")
      comment_params = Map.put(@valid_comment_params, "state_id", state.id)

      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket),
        comment: comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Comment has been successfully created!"

      assert [comment] = Projects.list_ticket_comments(ticket)
      assert comment.description == "A nice comment description about #{__MODULE__}"
      assert comment.state_id == state.id

      ticket = Projects.get_ticket(ticket.id)
      assert ticket.state_id == state.id
    end

    test "viewers cannot set comment and ticket state",
        %{conn: conn, ticket: ticket, user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      state = TestFactory.insert(:state, name: "Working on it")
      comment_params = Map.put(@valid_comment_params, "state_id", state.id)

      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket),
        comment: comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Comment has been successfully created!"

      assert [comment] = Projects.list_ticket_comments(ticket)
      assert comment.description == "A nice comment description about #{__MODULE__}"
      assert comment.state_id == nil

      ticket = Projects.get_ticket(ticket.id)
      assert ticket.state_id == nil
    end

    test "comment author is registered as watcher", %{conn: conn, user: user, ticket: ticket} do
      conn = post(conn, Routes.ticket_comment_path(conn, :create, ticket), comment: @valid_comment_params)

      assert get_flash(conn, :info) =~ "Comment has been successfully created!"
      assert Watchers.list_ticket_watcher_addresses(ticket) == [{user.id, user.email}]
    end
  end

  describe "PATCH /tickets/:ticket_id/comments/:id" do
    @update_comment_params %{
      "description" => "An updated comment description about #{__MODULE__}",
    }

    setup %{user: user, project: project, ticket: ticket} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      comment = TestFactory.insert(:comment, ticket: ticket, user: user)

      {:ok, comment: comment}
    end

    test "with valid params updates comment and redirects",
        %{conn: conn, ticket: ticket, comment: comment} do
      conn = patch(conn, Routes.ticket_comment_path(conn, :update, ticket, comment),
        comment: @update_comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) =~ "Comment has been successfully updated!"

      assert [comment] = Projects.list_ticket_comments(ticket)
      assert comment.description == "An updated comment description about #{__MODULE__}"
    end

    test "with invalid parrams returns errors",
        %{conn: conn, ticket: ticket, comment: comment} do
      invalid_params = Map.put(@update_comment_params, "description", "   ")

      conn = patch(conn, Routes.ticket_comment_path(conn, :update, ticket, comment),
        comment: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to update comment!"
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket, comment: comment,
                                                     user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = patch(conn, Routes.ticket_comment_path(conn, :update, ticket, comment),
        comment: @update_comment_params)

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end

    test "managers can update comment state",
        %{conn: conn, ticket: ticket, comment: comment, user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :manager, project)
      state = TestFactory.insert(:state, name: "Working on it")
      comment_params = Map.put(@update_comment_params, "state_id", state.id)

      conn = patch(conn, Routes.ticket_comment_path(conn, :update, ticket, comment),
        comment: comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Comment has been successfully updated!"

      assert comment = Projects.get_comment_for(ticket, comment.id)
      assert comment.description == "An updated comment description about #{__MODULE__}"
      assert comment.state_id == state.id
    end

    test "viewers cannot update comment state",
        %{conn: conn, ticket: ticket, comment: comment, user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      state = TestFactory.insert(:state, name: "Working on it")
      comment_params = Map.put(@update_comment_params, "state_id", state.id)

      conn = patch(conn, Routes.ticket_comment_path(conn, :update, ticket, comment),
        comment: comment_params)

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Comment has been successfully updated!"

      assert comment = Projects.get_comment_for(ticket, comment.id)
      assert comment.description == "An updated comment description about #{__MODULE__}"
      assert comment.state_id == nil
    end
  end

  describe "DELETE /tickets/:ticket_id/comments/:id" do
    setup :create_comment

    setup %{user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)
      :ok
    end

    test "deletes comment and redirects", %{conn: conn, ticket: ticket, comment: comment} do
      conn = delete(conn, Routes.ticket_comment_path(conn, :delete, ticket, comment))

      assert redirected_to(conn) == Routes.ticket_comment_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Comment has been successfully deleted!"
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket, comment: comment,
                                                     user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = delete(conn, Routes.ticket_comment_path(conn, :delete, ticket, comment))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end

    test "when comment not found returns error", %{conn: conn, ticket: ticket} do
      invalid_comment_id = 0

      conn = delete(conn, Routes.ticket_comment_path(conn, :delete, ticket, invalid_comment_id))

      assert conn.status == 404
      assert conn.resp_body == "Comment not found!"
    end
  end

  ## Helpers

  defp create_comment(%{user: user, ticket: ticket}) do
    comment = TestFactory.insert(:comment, ticket: ticket, user: user)
    {:ok, comment: comment}
  end
end
