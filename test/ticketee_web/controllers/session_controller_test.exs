defmodule TicketeeWeb.SessionControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Accounts

  setup do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, user: user}
  end

  describe "POST /sessions" do
    test "with valid credentials signs in user", %{conn: conn, user: user} do
      credentials = %{email: user.email, password: "super secret"}

      conn = post(conn, Routes.session_path(conn, :create), session: credentials)

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :info) == "Signed in successfully!"
      assert get_session(conn, :user_id) == user.id
    end

    test "when password is invalid returns error", %{conn: conn, user: user} do
      invalid_credentials = %{email: user.email, password: "wrong password"}

      conn = post(conn, Routes.session_path(conn, :create), session: invalid_credentials)

      assert html_response(conn, 200) =~ "Invalid email/password combination!"
      assert get_session(conn, :user_id) == nil
    end

    test "when email is invalid returns error", %{conn: conn} do
      invalid_credentials = %{email: "invalid@#{__MODULE__}.com", password: "super secret"}

      conn = post(conn, Routes.session_path(conn, :create), session: invalid_credentials)

      assert html_response(conn, 200) =~ "Invalid email/password combination!"
      assert get_session(conn, :user_id) == nil
    end

    test "when user account is archived returns error", %{conn: conn, user: user} do
      assert {:ok, user} = Accounts.archive_user(user)
      credentials = %{email: user.email, password: "super secret"}

      conn = post(conn, Routes.session_path(conn, :create), session: credentials)

      assert html_response(conn, 200) =~ "You cannot sign in, because your account has been archived!"
      assert get_session(conn, :user_id) == nil
    end
  end

  describe "DELETE /sessions" do
    setup %{conn: conn, user: user} do
      conn = sign_in_user(conn, user)

      {:ok, conn: conn}
    end

    test "drops user session", %{conn: conn} do
      # Precondition
      assert conn.assigns[:current_user]

      conn = delete(conn, Routes.session_path(conn, :delete))

      assert conn.assigns[:current_user] == nil
      assert get_flash(conn, :info) == "Signed out succesfully!"
      assert session_dropped?(conn)
    end
  end
end
