defmodule TicketeeWeb.ProjectControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Accounts
  alias Ticketee.Roles

  setup %{conn: conn} do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)

    {:ok, conn: conn, user: user}
  end

  describe "GET /projects" do
    setup do
      project_a = TestFactory.insert(:project, name: "Manhattan #{__MODULE__}")
      project_b = TestFactory.insert(:project, name: "Orion #{__MODULE__}")

      {:ok, projects: [project_a, project_b]}
    end

    test "when admin lists all projects on index", %{conn: conn, user: user} do
      assert {:ok, _admin} = Accounts.promote_to_admin(user)

      conn = get(conn, Routes.project_path(conn, :index))
      response = html_response(conn, 200)

      assert response =~ ~r/Projects/
      assert String.contains?(response, "Manhattan #{__MODULE__}")
      assert String.contains?(response, "Orion #{__MODULE__}")
    end

    test "when no roles assigned sees no projects", %{conn: conn} do
      conn = get(conn, Routes.project_path(conn, :index))
      response = html_response(conn, 200)

      assert response =~ ~r/Projects/
      refute String.contains?(response, "Manhattan #{__MODULE__}")
      refute String.contains?(response, "Orion #{__MODULE__}")
    end

    test "lists all projects for which roles are assigned",
        %{conn: conn, user: user, projects: [project_a | _]} do
      assert {:ok, _role} = Roles.grant_role(user, :viewer, project_a)

      conn = get(conn, Routes.project_path(conn, :index))
      response = html_response(conn, 200)

      assert response =~ ~r/Projects/
      assert String.contains?(response, "Manhattan #{__MODULE__}")
      refute String.contains?(response, "Orion #{__MODULE__}")
    end
  end

  describe "GET /projects/:id" do
    setup do
      project = TestFactory.insert(:project, name: "Manhattan #{__MODULE__}")
      {:ok, project: project}
    end

    test "when viewer shows project details", %{conn: conn, user: user, project: project} do
      {:ok, _role} = Roles.grant_role(user, :viewer, project)

      conn = get(conn, Routes.project_path(conn, :show, project))
      response = html_response(conn, 200)

      assert String.contains?(response, project.name)
      assert String.contains?(response, project.description)
    end

    test "when user not assigned role on project redirects", %{conn: conn, project: project} do
      conn = get(conn, Routes.project_path(conn, :show, project))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You are not allowed to perform that action!"
    end

    test "handles missing projects by redirecting", %{conn: conn} do
      conn = get(conn, Routes.project_path(conn, :show, "not-here"))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "The project you were looking for could not be found!"
    end
  end

  @valid_project_params %{
    "name" => "Test Project #{__MODULE__}",
    "description" => "Some useful description",
  }

  describe "POST /projects (when regular user)" do
    test "returns error and redirects", %{conn: conn} do
      conn = post(conn, Routes.project_path(conn, :create), project: @valid_project_params)

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"
    end
  end

  describe "POST /projects (when admin)" do
    setup :sign_in_with_admin

    test "creates new project and redirects", %{conn: conn} do
      conn = post(conn, Routes.project_path(conn, :create), project: @valid_project_params)

      assert %{id: project_id} = redirected_params(conn)
      project = Projects.get_project(project_id)

      assert redirected_to(conn) == Routes.project_path(conn, :show, project)
      assert get_flash(conn, :info) == "Project has been successfully created!"

      ## Assert project was saved successfully
      assert project.name == "Test Project #{__MODULE__}"
      assert project.description == "Some useful description"
    end

    test "when create project params are invalid renders errors", %{conn: conn} do
      invalid_params = Map.put(@valid_project_params, "name", "   ")

      conn = post(conn, Routes.project_path(conn, :create), project: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to create project!"
    end
  end


  describe "PATCH /projects/:id" do
    @valid_update_params %{
      "name" => "Updated name #{__MODULE__}",
      "description" => "Updated description #{__MODULE__}",
    }

    setup %{user: user} do
      project = TestFactory.insert(:project, name: "Manhattan #{__MODULE__}")
      {:ok, _role} = Roles.grant_role(user, :manager, project)

      {:ok, project: project}
    end

    test "updates existing project and redirects", %{conn: conn, project: project} do
      conn = patch(conn, Routes.project_path(conn, :update, project), project: @valid_update_params)

      updated_project = Projects.get_project(project.id)
      assert redirected_to(conn) == Routes.project_path(conn, :show, updated_project)
      assert get_flash(conn, :info) == "Project has been successfully updated!"

      # Assert project updated correctly
      assert updated_project.name == "Updated name #{__MODULE__}"
      assert updated_project.description == "Updated description #{__MODULE__}"
    end

    test "when update project params are invalid renders errors", %{conn: conn, project: project} do
      invalid_params = Map.put(@valid_update_params, "name", "   ")

      conn = patch(conn, Routes.project_path(conn, :update, project), project: invalid_params)

      assert html_response(conn, 200) =~ html_escape("can't be blank")
      assert get_flash(conn, :error) == "Failed to update project!"
    end
  end

  describe "DELETE /projects/:id (when regular user)" do
    setup :create_project

    test "returns error and redirects", %{conn: conn, project: project} do
      conn = delete(conn, Routes.project_path(conn, :delete, project))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"

      ## Assert project has *NOT* been deleted from db
      assert Projects.get_project(project.id)
    end
  end

  describe "DELETE /projects/:id (when admin)" do
    setup [:sign_in_with_admin, :create_project]

    test "deletes project and redirects", %{conn: conn, project: project} do
      conn = delete(conn, Routes.project_path(conn, :delete, project))

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :info) == "Project has been successfully deleted!"

      ## Assert project has been deleted from db
      assert Projects.get_project(project.id) == nil
    end
  end

  ## Helpers

  defp create_project(_context) do
    project = TestFactory.insert(:project)
    {:ok, project: project}
  end

  defp sign_in_with_admin(%{conn: conn}) do
    admin = TestFactory.insert(:user, admin: true)
    conn = sign_in_user(conn, admin)

    {:ok, conn: conn, admin: admin}
  end
end
