defmodule TicketeeWeb.AccountControllerTest do
  use TicketeeWeb.ConnCase, async: true
  use Bamboo.Test

  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Passwords
  alias TicketeeWeb.Notifications.Email

  @valid_account_params %{
    "email" => "test_user@#{__MODULE__}.com",
    "password" => "super secret",
    "password_confirmation" => "super secret",
  }

  describe "GET /account (when not logged in)" do
    test "returns error and redirects", %{conn: conn} do
      conn = get(conn, Routes.account_path(conn, :show))

      assert redirected_to(conn) == Routes.session_path(conn, :new)
      assert get_flash(conn, :error) == "You must sign in first!"
    end
  end

  describe "GET /account" do
    setup %{conn: conn} do
      user = TestFactory.insert(:user)
      conn = sign_in_user(conn, user)

      {:ok, conn: conn, user: user}
    end

    test "returns information about the current user", %{conn: conn, user: user} do
      conn = get(conn, Routes.account_path(conn, :show))

      response = html_response(conn, 200)
      assert String.contains?(response, "My Account")
      assert String.contains?(response, user.email)
      assert String.contains?(response, "Regular User")
    end
  end

  describe "POST /accounts" do
    test "with valid params registers new user", %{conn: conn} do
      conn = post(conn, Routes.account_path(conn, :create), user: @valid_account_params)

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :info) == "You have signed up successfully!"

      user = Accounts.get_user_by(email: "test_user@#{__MODULE__}.com")
      assert_delivered_email Email.welcome_email(user)
    end

    test "when params are invalid returns errors", %{conn: conn} do
      invalid_params = Map.put(@valid_account_params, "password_confirmation", "invalid")

      conn = post(conn, Routes.account_path(conn, :create), user: invalid_params)

      assert get_flash(conn, :error) == "Failed to create new account!"
      assert html_response(conn, 200) =~ html_escape("does not match confirmation")
    end
  end

  @update_account_params %{
    "email" => "updated_user_email@#{__MODULE__}.com",
    "password" => "updated new password",
    "password_confirmation" => "updated new password",
  }

  describe "PATCH /account (when not logged in)" do
    test "returns error and redirects", %{conn: conn} do
      conn = patch(conn, Routes.account_path(conn, :update), user: @update_account_params)

      assert redirected_to(conn) == Routes.session_path(conn, :new)
      assert get_flash(conn, :error) == "You must sign in first!"
    end
  end

  describe "PATCH /account" do
    setup %{conn: conn} do
      user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
      conn = sign_in_user(conn, user)

      {:ok, conn: conn, user: user}
    end

    test "with valid params updates user", %{conn: conn, user: user} do
      conn = patch(conn, Routes.account_path(conn, :update), user: @update_account_params)

      assert redirected_to(conn) == Routes.account_path(conn, :show)
      assert get_flash(conn, :info) == "Account has been successfully updated!"

      # Assert user information has been updated
      updated_user = Accounts.get_user(user.id)
      assert updated_user.email == "updated_user_email@#{__MODULE__}.com"
      assert Passwords.verify("updated new password", updated_user.password_hash)
    end

    test "when params are invalid returns errors", %{conn: conn} do
      invalid_params = Map.put(@update_account_params, "password_confirmation", "invalid")

      conn = patch(conn, Routes.account_path(conn, :update), user: invalid_params)

      assert get_flash(conn, :error) == "Failed to update account!"
      assert html_response(conn, 200) =~ html_escape("does not match confirmation")
    end
  end
end
