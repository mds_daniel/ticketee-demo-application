defmodule TicketeeWeb.UserControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Accounts

  @valid_account_params %{
    "email" => "test_user@#{__MODULE__}.com",
    "password" => "super secret",
    "password_confirmation" => "super secret",
  }

  describe "POST /users (when regular user)" do
    setup %{conn: conn} do
      user = TestFactory.insert(:user, email: "regular_user@#{__MODULE__}.com")
      conn = sign_in_user(conn, user)

      {:ok, conn: conn, user: user}
    end

    test "redirects without creating user", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @valid_account_params)

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"

      # Assert user *NOT* persisted in db
      refute Accounts.get_user_by(email: @valid_account_params["email"])
    end
  end

  describe "POST /users (when admin)" do
    setup %{conn: conn} do
      admin = TestFactory.insert(:user, email: "admin@#{__MODULE__}.com", admin: true)
      conn = sign_in_user(conn, admin)

      {:ok, conn: conn, user: admin}
    end

    test "with valid params creates new user", %{conn: conn} do
      conn = post(conn, Routes.user_path(conn, :create), user: @valid_account_params)

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :info) == "User has been successfully created!"

      # Assert user persisted in db
      assert %Accounts.User{} = Accounts.get_user_by(email: @valid_account_params["email"])
    end

    test "allows the creation of new admin users", %{conn: conn} do
      admin_params = Map.put(@valid_account_params, "admin", "true")

      conn = post(conn, Routes.user_path(conn, :create), user: admin_params)

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :info) == "User has been successfully created!"

      # Assert user persisted in db
      assert %Accounts.User{admin: true} = Accounts.get_user_by(email: @valid_account_params["email"])
    end

    test "when params are invalid returns errors", %{conn: conn} do
      invalid_params = Map.put(@valid_account_params, "password_confirmation", "invalid")

      conn = post(conn, Routes.user_path(conn, :create), user: invalid_params)

      assert get_flash(conn, :error) == "Failed to create user!"
      assert html_response(conn, 200) =~ html_escape("does not match confirmation")
    end
  end

  describe "PATCH /users/:id/promote" do
    setup do
      admin = TestFactory.insert(:user, email: "admin@#{__MODULE__}.com", admin: true)
      user = TestFactory.insert(:user, email: "regular_user@#{__MODULE__}.com")

      {:ok, user: user, admin: admin}
    end

    test "when admin promotes user to admin", %{conn: conn, user: user, admin: admin} do
      conn = sign_in_user(conn, admin)

      conn = patch(conn, Routes.user_path(conn, :promote, user), user: %{promote: "admin"})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :info) == "User has been successfully promoted to admin!"

      # Assert user promoted to admin
      assert %Accounts.User{admin: true} = Accounts.get_user(user.id)
    end

    test "when regular user redirects without promoting user", %{conn: conn, user: user} do
      conn = sign_in_user(conn, user)

      conn = patch(conn, Routes.user_path(conn, :promote, user), user: %{promote: "admin"})

      assert redirected_to(conn) == Routes.project_path(conn, :index)
      assert get_flash(conn, :error) == "You must be signed in as an admin!"

      # Assert user *NOT* promoted to admin
      assert %Accounts.User{admin: false} = Accounts.get_user(user.id)
    end

    test "when admin and user not found redirects and returns error", %{conn: conn, admin: admin} do
      conn = sign_in_user(conn, admin)
      invalid_user_id = TestFactory.user_uuid()

      conn = patch(conn, Routes.user_path(conn, :promote, invalid_user_id), user: %{promote: "admin"})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :error) == "The user you were looking for could not be found!"
    end

    test "when admin and promote user params invalid", %{conn: conn, user: user, admin: admin} do
      conn = sign_in_user(conn, admin)

      conn = patch(conn, Routes.user_path(conn, :promote, user), user: %{invalid: true})

      assert conn.status == 422
      assert conn.resp_body == "Invalid promote user params!"
    end
  end

  describe "PATCH /users/:id/archive" do
    setup do
      admin = TestFactory.insert(:user, email: "admin@#{__MODULE__}.com", admin: true)
      user = TestFactory.insert(:user, email: "regular_user@#{__MODULE__}.com")

      {:ok, user: user, admin: admin}
    end

    test "when admin archives user", %{conn: conn, user: user, admin: admin} do
      conn = sign_in_user(conn, admin)

      conn = patch(conn, Routes.user_path(conn, :archive, user), user: %{archive: true})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :info) == "User has been successfully archived!"

      # Assert user archived
      assert %Accounts.User{archived_at: %DateTime{}} = Accounts.get_user(user.id)
    end

    test "when admin restores archived user", %{conn: conn, user: user, admin: admin} do
      conn = sign_in_user(conn, admin)
      assert {:ok, user} = Accounts.archive_user(user)

      conn = patch(conn, Routes.user_path(conn, :archive, user), user: %{archive: false})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :info) == "User has been successfully restored!"

      # Assert user restored
      assert %Accounts.User{archived_at: nil} = Accounts.get_user(user.id)
    end

    test "when archive user params invalid", %{conn: conn, user: user, admin: admin} do
      conn = sign_in_user(conn, admin)

      conn = patch(conn, Routes.user_path(conn, :archive, user), user: %{archive: "invalid"})

      assert conn.status == 422
      assert conn.resp_body == "Invalid archive user params!"
    end

    test "when admin tries to archive self returns errors", %{conn: conn, admin: admin} do
      conn = sign_in_user(conn, admin)

      conn = patch(conn, Routes.user_path(conn, :archive, admin), user: %{archive: true})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :error) == "Action not permitted on own account!"

      # Assert user not archived
      assert %Accounts.User{archived_at: nil} = Accounts.get_user(admin.id)
    end

    test "when admin tries to archive another admin returns errors", %{conn: conn, admin: admin} do
      conn = sign_in_user(conn, admin)
      other_admin = TestFactory.insert(:user, email: "other_admin@#{__MODULE__}.com", admin: true)

      conn = patch(conn, Routes.user_path(conn, :archive, other_admin), user: %{archive: true})

      assert redirected_to(conn) == Routes.user_path(conn, :index)
      assert get_flash(conn, :error) == "Action not permitted on an admin account!"

      # Assert user not archived
      assert %Accounts.User{archived_at: nil} = Accounts.get_user(other_admin.id)
    end
  end
end
