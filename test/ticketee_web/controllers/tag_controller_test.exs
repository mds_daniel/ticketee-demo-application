defmodule TicketeeWeb.TagControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Projects
  alias Ticketee.Roles

  setup %{conn: conn} do
    project = TestFactory.insert(:project)
    ticket = TestFactory.insert(:ticket, project: project)

    user = TestFactory.insert(:user, email: "ticketee@#{__MODULE__}.com")
    conn = sign_in_user(conn, user)
    assert {:ok, _role} = Roles.grant_role(user, :manager, project)

    {:ok, conn: conn, user: user, project: project, ticket: ticket}
  end

  describe "GET /tickets/:ticket_id/tags" do
    setup %{ticket: ticket} do
      tag1 = TestFactory.insert(:tag, name: "Adventure #{__MODULE__}")
      tag2 = TestFactory.insert(:tag, name: "Mystery #{__MODULE__}")
      assert {:ok, _} = Projects.create_ticket_tags(ticket, [tag1.name, tag2.name])
      {:ok, tags: [tag1, tag2]}
    end

    test "returns ticket tags", %{conn: conn, ticket: ticket} do
      conn = get(conn, Routes.ticket_tag_path(conn, :index, ticket))

      response = html_response(conn, 200)
      assert String.contains?(response, "Adventure #{__MODULE__}")
      assert String.contains?(response, "Mystery #{__MODULE__}")
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket,
                                                     project: project, user: user} do
      assert {:ok, _role} = Roles.revoke_role(user, project)

      conn = get(conn, Routes.ticket_tag_path(conn, :index, ticket))

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  describe "POST /tickets/:ticket_id/tags" do
    setup do
      tag1 = TestFactory.insert(:tag, name: "Adventure #{__MODULE__}")
      tag2 = TestFactory.insert(:tag, name: "Mystery #{__MODULE__}")

      {:ok, tags: [tag1, tag2]}
    end

    test "persists tags", %{conn: conn, ticket: ticket, tags: tags} do
      tag_names = Enum.map(tags, &(&1.name))
      conn = post(conn, Routes.ticket_tag_path(conn, :create, ticket), tags: tag_names)

      assert redirected_to(conn) == Routes.ticket_tag_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Tags have been successfully added!"

      ticket = Projects.with_ticket_tags(ticket)
      assert ticket.tags == tags
    end

    test "without tags returns error", %{conn: conn, ticket: ticket} do
      conn = post(conn, Routes.ticket_tag_path(conn, :create, ticket))

      assert redirected_to(conn) == Routes.ticket_tag_path(conn, :index, ticket)
      assert get_flash(conn, :error) == "Must select tags to be added!"
    end

    test "without project role returns forbidden", %{conn: conn, ticket: ticket, tags: tags,
                                                     project: project, user: user} do
      assert {:ok, _role} = Roles.revoke_role(user, project)

      tag_names = Enum.map(tags, &(&1.name))
      conn = post(conn, Routes.ticket_tag_path(conn, :create, ticket), tags: tag_names)

      assert conn.status == 403
      assert conn.resp_body == "You are not allowed to perform that action!"
    end
  end

  describe "DELETE /tickets/:ticket_id/tags/:id" do
    setup %{ticket: ticket} do
      tag = TestFactory.insert(:tag, name: "Adventure #{__MODULE__}")
      assert {:ok, _} = Projects.create_ticket_tags(ticket, [tag.name])

      {:ok, tag: tag}
    end

    test "removes tag from ticket", %{conn: conn, ticket: ticket, tag: tag} do
      conn = delete(conn, Routes.ticket_tag_path(conn, :delete, ticket, tag))

      assert redirected_to(conn) == Routes.ticket_tag_path(conn, :index, ticket)
      assert get_flash(conn, :info) == "Tag has been successfully removed!"
      assert Projects.with_ticket_tags(ticket).tags == []
    end

    test "when tag not associated with ticket returns error", %{conn: conn, ticket: ticket} do
      other_tag = TestFactory.insert(:tag)
      conn = delete(conn, Routes.ticket_tag_path(conn, :delete, ticket, other_tag))

      assert redirected_to(conn) == Routes.ticket_tag_path(conn, :index, ticket)
      assert get_flash(conn, :error) == "Failed to remove tag!"
      assert Projects.with_ticket_tags(ticket).tags |> Enum.count() == 1
    end
  end
end
