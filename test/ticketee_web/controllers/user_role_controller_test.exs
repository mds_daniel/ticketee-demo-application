defmodule TicketeeWeb.UserRoleControllerTest do
  use TicketeeWeb.ConnCase, async: true

  alias Ticketee.TestFactory
  alias Ticketee.Accounts
  alias Ticketee.Projects
  alias Ticketee.Roles
  alias Ticketee.Roles.Role

  setup %{conn: conn} do
    current_user = TestFactory.insert(:user, email: "current_user@#{__MODULE__}.com")
    conn = sign_in_user(conn, current_user)

    user = TestFactory.insert(:user, email: "simple_user@#{__MODULE__}.com")
    project = TestFactory.insert(:project, name: "Project #{__MODULE__}")

    {:ok, conn: conn,
          current_user: current_user,
          user: user,
          project: project}
  end

  describe "POST /users/:user_id/roles (when non-admin)" do
    test "redirects and returns error", %{conn: conn, user: user, project: project} do
      role_params = %{role: %{project_id: project.id, role: "manager"}}
      conn = post(conn, Routes.user_role_path(conn, :create, user, role_params))

      assert json_response(conn, 403) == %{"errors" => %{"user" => ["must be signed in as an admin"]}}
    end
  end

  describe "POST /users/:user_id/roles (when admin)" do
    setup %{current_user: current_user} do
      {:ok, admin} = Accounts.promote_to_admin(current_user)
      {:ok, current_user: admin}
    end

    test "with valid params grants role to user", %{conn: conn, user: user, project: project} do
      role_params = %{role: %{project_id: project.id, role: "manager"}}
      conn = post(conn, Routes.user_role_path(conn, :create, user, role_params))

      assert json_response(conn, 201) == %{
        "project_id" => project.id,
        "role" => "manager",
        "user_id" => user.id
      }

      assert %Role{role: :manager} = Roles.role_on_project(user, project)
    end

    test "when user is not found returns error", %{conn: conn, project: project} do
      role_params = %{role: %{project_id: project.id, role: "manager"}}
      invalid_user_id = TestFactory.user_uuid()

      conn = post(conn, Routes.user_role_path(conn, :create, invalid_user_id, role_params))

      assert json_response(conn, 404) == %{"errors" => %{"user_id" => ["not found"]}}
    end

    test "when project is not found returns error", %{conn: conn, user: user, project: project} do
      {:ok, _} = Projects.delete_project(project)
      invalid_params = %{role: %{project_id: project.id, role: "manager"}}

      conn = post(conn, Routes.user_role_path(conn, :create, user, invalid_params))

      assert json_response(conn, 404) == %{"errors" => %{"project_id" => ["not found"]}}
    end

    test "when role is invalid returns error", %{conn: conn, user: user, project: project} do
      invalid_params = %{role: %{project_id: project.id, role: "dragon_tamer"}}
      conn = post(conn, Routes.user_role_path(conn, :create, user, invalid_params))

      assert json_response(conn, 422) == %{"errors" => %{"role" => ["is invalid"]}}
    end

    test "when project id is invalid returns error", %{conn: conn, user: user} do
      invalid_params = %{role: %{project_id: 0, role: "manager"}}

      conn = post(conn, Routes.user_role_path(conn, :create, user, invalid_params))

      assert json_response(conn, 422) == %{"errors" => %{"project_id" => ["is invalid"]}}
    end
  end

  describe "DELETE /users/:user_id/roles/:project_id (when non-admin)" do
    setup %{user: user, project: project} do
      {:ok, _} = Roles.grant_role(user, :manager, project)
      :ok
    end

    test "redirects and returns error", %{conn: conn, user: user, project: project} do
      conn = delete(conn, Routes.user_role_path(conn, :delete, user, project))

      assert json_response(conn, 403) == %{"errors" => %{"user" => ["must be signed in as an admin"]}}
    end
  end

  describe "DELETE /users/:user_id/roles/:project_id (when admin)" do
    setup %{current_user: current_user, user: user, project: project} do
      {:ok, _} = Roles.grant_role(user, :manager, project)
      {:ok, admin} = Accounts.promote_to_admin(current_user)
      {:ok, current_user: admin}
    end

    test "with valid params revokes role from user", %{conn: conn, user: user, project: project} do
      conn = delete(conn, Routes.user_role_path(conn, :delete, user, project))

      assert json_response(conn, 200) == %{
        "project_id" => project.id,
        "role" => "manager",
        "user_id" => user.id
      }

      ## Assert role on project is removed
      refute Roles.role_on_project(user, project)
    end

    test "when user is not found returns error", %{conn: conn, project: project} do
      invalid_user_id = TestFactory.user_uuid()

      conn = delete(conn, Routes.user_role_path(conn, :delete, invalid_user_id, project))

      assert json_response(conn, 404) == %{"errors" => %{"user_id" => ["not found"]}}
    end

    test "when project is not found returns error", %{conn: conn, user: user, project: project} do
      {:ok, _} = Projects.delete_project(project)

      conn = delete(conn, Routes.user_role_path(conn, :delete, user, project))

      assert json_response(conn, 404) == %{"errors" => %{"project_id" => ["not found"]}}
    end

    test "when role is not found returns error", %{conn: conn, user: user, project: project} do
      {:ok, _role} = Roles.revoke_role(user, project)

      conn = delete(conn, Routes.user_role_path(conn, :delete, user, project))

      assert json_response(conn, 404) == %{"errors" => %{"role" => ["not found"]}}
    end

    test "when project id is invalid returns error", %{conn: conn, user: user} do
      invalid_project_id = 0

      conn = delete(conn, Routes.user_role_path(conn, :delete, user, invalid_project_id))

      assert json_response(conn, 422) == %{"errors" => %{"project_id" => ["is invalid"]}}
    end
  end
end
