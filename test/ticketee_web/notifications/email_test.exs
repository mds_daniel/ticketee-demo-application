defmodule TicketeeWeb.Notifications.EmailTest do
  use Ticketee.DataCase, async: true

  alias TicketeeWeb.Notifications.Email
  alias TicketeeWeb.Endpoint
  alias TicketeeWeb.Router.Helpers, as: Routes
  alias Ticketee.TestFactory
  alias Ticketee.Projects

  setup do
    user = TestFactory.insert(:user, email: "test_user@#{__MODULE__}.com")
    {:ok, user: user}
  end

  describe "welcome_email/1" do
    test "welcomes user to app", %{user: user} do
      email = Email.welcome_email(user)

      assert email.to == user.email
      assert email.subject == "[ticketee] Welcome to Ticketee"
      assert email.html_body =~ Routes.project_path(Endpoint, :index)
      assert email.text_body =~ Routes.project_path(Endpoint, :index)
    end
  end

  describe "comment_created_email/3" do
    setup %{user: user} do
      state_open = TestFactory.insert(:state, name: "Open #{__MODULE__}")
      state_in_progress = TestFactory.insert(:state, name: "In Progress #{__MODULE__}")

      project = TestFactory.insert(:project)
      ticket = TestFactory.insert(:ticket, project: project, state: state_open)

      {:ok, comment} = Projects.create_comment_for(user, ticket, %{
          state_id: state_in_progress.id,
          description: "Some useful comment description"
        })

      {:ok, ticket: ticket, comment: comment,
            state_open: state_open, state_in_progress: state_in_progress}
    end

    test "sends notification about created comment",
        %{user: user, ticket: ticket, comment: comment,
          state_open: state_open, state_in_progress: state_in_progress} do
      indexed_states = [state_open, state_in_progress]
        |> Enum.into(%{}, fn state -> {state.id, state} end)

      email = Email.comment_created_email("alice@#{__MODULE__}.com", ticket, comment, indexed_states)

      assert email.to == "alice@#{__MODULE__}.com"
      assert email.subject == "[ticketee] New Comment for Ticket: #{ticket.title}"

      assert email.text_body =~ comment.description
      assert email.text_body =~ "#{user.email} has posted a new comment."
      assert email.text_body =~ "State changed from 'Open #{__MODULE__}' to 'In Progress #{__MODULE__}'"

      assert email.html_body =~ comment.description
      assert email.html_body =~ "#{user.email} has posted a new comment."
      assert email.html_body =~ "Open #{__MODULE__}"
      assert email.html_body =~ "In Progress #{__MODULE__}"
    end
  end
end
