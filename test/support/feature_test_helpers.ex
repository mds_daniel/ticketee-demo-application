defmodule TicketeeWeb.FeatureTestHelpers do
  import ExUnit.Assertions, only: [assert: 1]
  use Wallaby.DSL

  import Wallaby.Query, only: [button: 1, text_field: 1, link: 1]

  def click_confirmation(session, element, confirmation_message) do
    message =
      accept_confirm(session, fn inner_session ->
        click(inner_session, element)
      end)

    assert message == confirmation_message

    session
  end

  def assert_has_current_path(session, expected_path) do
    assert current_path(session) == expected_path

    session
  end

  @test_password "super secret"

  def sign_in_with(session, email, password \\ @test_password) do
    session
    |> visit("/")
    |> click(link("Sign in"))

    session
    |> fill_in(text_field("Email"), with: email)
    |> fill_in(text_field("Password"), with: password)
    |> click(button("Sign in"))
  end
end
