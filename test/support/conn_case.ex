defmodule TicketeeWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common data structures and query the data layer.

  Finally, if the test case interacts with the database,
  we enable the SQL sandbox, so changes done to the database
  are reverted at the end of every test. If you are using
  PostgreSQL, you can even run database tests asynchronously
  by setting `use TicketeeWeb.ConnCase, async: true`, although
  this option is not recommended for other databases.
  """

  use ExUnit.CaseTemplate

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import TicketeeWeb.ConnCase

      alias TicketeeWeb.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint TicketeeWeb.Endpoint
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Ticketee.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Ticketee.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  alias TicketeeWeb.Plugs.Auth
  alias TicketeeWeb.APITokens

  def html_escape(string) do
    string
    |> Phoenix.HTML.html_escape()
    |> Phoenix.HTML.safe_to_string()
  end

  def sign_in_user(conn, user) do
    conn
    |> Plug.Test.init_test_session(%{})
    |> Auth.login(user)
  end

  def session_dropped?(%Plug.Conn{private: %{plug_session_info: :drop}}) do
    true
  end

  def session_dropped?(%Plug.Conn{} = _other) do
    false
  end

  def response_content_type_header(conn) do
    conn
    |> Plug.Conn.get_resp_header("content-type")
    |> List.first()
  end

  @jsonapi_mime_type "#{JSONAPI.mime_type()}; charset=utf-8"

  def jsonapi_mime_type do
    @jsonapi_mime_type
  end

  def sign_in_api_user(conn, user) do
    token = APITokens.sign(user.id)
    Plug.Conn.put_req_header(conn, "authorization", "Token token=#{token}")
  end

  def set_jsonapi_content_type(conn) do
    Plug.Conn.put_req_header(conn, "content-type", JSONAPI.mime_type())
  end
end
