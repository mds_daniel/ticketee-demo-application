defmodule Ticketee.TestFixtureFiles do
  @fixture_file_folder "test/fixtures/files"

  def fixture_file(filename) do
    Path.join(@fixture_file_folder, filename)
  end

  def fixture_file_upload(filename) do
    %Plug.Upload{
      content_type: content_type_from_ext(filename),
      filename: filename,
      path: fixture_file(filename),
    }
  end

  def content_type_from_ext(filename) do
    filename
    |> Path.extname()
    |> String.trim_leading(".")
    |> MIME.type()
  end
end
