defmodule Ticketee.TestFactory do
  use ExMachina.Ecto, repo: Ticketee.Repo

  alias Ticketee.Projects.{Ticket, Project, Comment, State}
  alias Ticketee.Tags.Tag
  alias Ticketee.Accounts.User
  alias Ticketee.Attachments.{TicketAttachment, Upload}
  alias Ticketee.Passwords

  def project_factory do
    project_name = sequence(:project_name, &"#{Faker.Team.name()} #{&1}")

    %Project{
      name: project_name,
      description: Faker.Lorem.sentence(),
      slug: Ticketee.Permalink.slugify(project_name),
    }
  end

  def ticket_factory do
    %Ticket{
      title: Faker.Food.dish(),
      description: Faker.Food.description(),
    }
  end

  def ticket_attachment_factory do
    %TicketAttachment{
      file: attachment_file_name(),
      description: Faker.Lorem.sentence(),
    }
  end

  def attachment_file_name do
    filename = Faker.File.file_name()
    basename = Path.basename(filename, Path.extname(filename))
    basename <> Enum.random(Upload.allowed_image_extensions())
  end

  def user_factory do
    %User{
      email: sequence(:user_email, &"test_#{&1}_#{Faker.Internet.safe_email()}"),
      password_hash: Passwords.hash("super secret"),
    }
  end

  def comment_factory do
    %Comment{
      description: Faker.Lorem.sentence(),
    }
  end

  def state_factory do
    %State{
      name: sequence(:state_name, &"#{Faker.Beer.name()} #{&1}"),
      color: "##{Faker.Color.rgb_hex()}",
      order_no: 0,
      default: false,
    }
  end

  def tag_factory do
    %Tag{
      name: sequence(:tag_name, &"#{Faker.Beer.name()} #{&1}"),
    }
  end

  def user_uuid do
    Ecto.UUID.generate()
  end
end
