defmodule TicketeeWeb.FeatureCase do
  @moduledoc """
  This module defines the test case to by used by feature tests.
  Wallaby is used for browser testing.
  """
  use ExUnit.CaseTemplate

  using do
    quote do
      use Wallaby.Feature

      import Wallaby.Query,
        only: [css: 1, css: 2,
               checkbox: 1,
               link: 1, button: 1,
               file_field: 1,
               text_field: 1, text_field: 2]

      import TicketeeWeb.FeatureTestHelpers,
        only: [click_confirmation: 3,
               assert_has_current_path: 2,
               sign_in_with: 2,
               sign_in_with: 3]

      import TicketeeWeb.FeatureCase,
        only: [project_path: 1,
               project_ticket_path: 2]

      import Ticketee.TestFixtureFiles,
        only: [fixture_file: 1, fixture_file_upload: 1]

      alias TicketeeWeb.Endpoint
      alias TicketeeWeb.Router.Helpers, as: Routes
    end
  end

  ## Routing convenience helpers

  alias TicketeeWeb.Endpoint
  alias TicketeeWeb.Router.Helpers, as: Routes

  def project_path(project) do
    Routes.project_path(Endpoint, :show, project)
  end

  def project_ticket_path(project, ticket) do
    Routes.project_ticket_path(Endpoint, :show, project, ticket)
  end
end
