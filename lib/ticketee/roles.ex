defmodule Ticketee.Roles do
  @moduledoc """
  Project roles context module.
  """
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Repo
  alias Ticketee.Roles.Role
  alias Ticketee.Projects
  alias Ticketee.Projects.Project
  alias Ticketee.Accounts.User

  defdelegate allowed?(role, action, resource), to: Ticketee.Roles.Authorize

  def list_roles do
    Repo.all(Role)
  end

  def with_user(role_or_roles) do
    Repo.preload(role_or_roles, :user)
  end

  def with_project(role_or_roles) do
    Repo.preload(role_or_roles, :project)
  end

  def roles_for_project(%Project{id: project_id}) do
    from(Role, where: [project_id: ^project_id])
    |> Repo.all()
  end

  def roles_for_user(%User{id: user_id}) do
    from(Role, where: [user_id: ^user_id])
    |> Repo.all()
  end

  def project_role_summary_for(%User{id: user_id}) do
    from(p in Projects.list_projects_query(),
      join: r in Role,
      on: r.project_id == p.id and
          r.user_id == ^user_id,
      select: %{project: p.name,
                project_id: p.id,
                role: r.role})
    |> Repo.all()
  end

  def role_on_project(%User{id: user_id}, %Project{id: project_id}) do
    Repo.get_by(Role, project_id: project_id, user_id: user_id)
  end

  def grant_role(%User{id: user_id}, role, %Project{id: project_id}) do
    %Role{user_id: user_id, project_id: project_id}
    |> Role.changeset(%{role: role})
    |> Repo.insert(
        on_conflict: [set: [role: role]],
        returning: true,
        conflict_target: [:user_id, :project_id]
    )
  end

  def revoke_role(%User{id: user_id}, %Project{id: project_id}) do
    from(r in Role, where: [user_id: ^user_id, project_id: ^project_id], select: r)
    |> Repo.delete_all()
    |> case do
        {1, [role]} ->
          {:ok, role}

        {0, []} ->
          {:error, :not_found}
       end
  end
end
