defmodule Ticketee.Passwords do
  def hash(password) do
    Argon2.hash_pwd_salt(password)
  end

  def verify(password, digest) when is_binary(digest) do
    Argon2.verify_pass(password, digest)
  end

  def verify(_password, nil) do
    Argon2.no_user_verify()
  end
end
