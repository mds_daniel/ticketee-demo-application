defmodule Ticketee.Authorize do
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Repo
  alias Ticketee.Roles
  alias Ticketee.Roles.Role
  alias Ticketee.Accounts.User
  alias Ticketee.Projects
  alias Ticketee.Projects.{Project, Ticket}

  @not_authorized_message "You are not allowed to perform that action!"

  def not_authorized_message do
    @not_authorized_message
  end

  def allowed?(user, action, resource)

  # Anonymous users are not allowed to perform any actions
  def allowed?(nil, _any_action, _any_resource) do
    false
  end

  # Archived users are not allowed to perform any actions
  def allowed?(%User{archived_at: %DateTime{}}, _any_action, _any_resource) do
    false
  end

  # Admins are allowed to perform all actions
  def allowed?(%User{admin: true}, _any_action, _any_resource) do
    true
  end

  def allowed?(%User{} = user, action, %Project{} = project) do
    user
    |> get_role_on_project(project)
    |> Roles.allowed?(action, project)
  end

  # Query project_resource permission on project role
  def allowed?(%User{} = user, action, {%Project{} = project, project_resource}) do
    user
    |> get_role_on_project(project)
    |> Roles.allowed?(action, project_resource)
  end

  # Query ticket permission on project role
  def allowed?(%User{} = user, action, %Ticket{project_id: project_id} = ticket) when not is_nil(project_id) do
    user
    |> get_role_on_project(%Project{id: project_id})
    |> Roles.allowed?(action, ticket)
  end

  # Query ticket resource permission on project role
  def allowed?(%User{} = user, action, {%Ticket{project_id: project_id}, _ticket_resource} = resource)
      when not is_nil(project_id) do
    user
    |> get_role_on_project(%Project{id: project_id})
    |> Roles.allowed?(action, resource)
  end

  # Catch-all deny
  def allowed?(_other_user, _other_action, _other_resource) do
    false
  end


  ## Roles caching
  # Prevent multiple `roles` queries when rendering views by caching

  def cache_project_role(%User{cached_roles: cached_roles} = user, %Project{id: project_id} = project) do
    role = Roles.role_on_project(user, project)
    updated_roles = Map.put(cached_roles, project_id, role)

    %User{user | cached_roles: updated_roles}
  end

  def cache_project_role(%User{} = user, %Ticket{project_id: project_id}) when not is_nil(project_id) do
    cache_project_role(user, %Project{id: project_id})
  end

  def get_role_on_project(%User{cached_roles: cached_roles} = user, %Project{id: project_id} = project) do
    case Map.fetch(cached_roles, project_id) do
      {:ok, role} ->
        role

      :error ->
        Roles.role_on_project(user, project)
    end
  end

  ## Scopes

  def list_projects_for(user) do
    Projects.list_projects_query()
    |> scope_projects(user)
    |> Repo.all()
  end

  def scope_projects(query, user)

  def scope_projects(query, %User{admin: true}) do
    query
  end

  def scope_projects(query, %User{id: user_id}) do
    from(p in query,
      join: r in Role,
      on: r.project_id == p.id and r.user_id == ^user_id)
  end

  def search_tickets_for(user, search_params \\ %{}) do
    Projects.search_tickets_query(search_params)
    |> scope_tickets(user)
    |> Repo.all()
  end

  def scope_tickets(query, user)

  def scope_tickets(query, %User{admin: true}) do
    query
  end

  def scope_tickets(query, %User{id: user_id}) do
    from(t in query,
      join: r in Role,
      on: r.project_id == t.project_id and r.user_id == ^user_id)
  end
end
