defmodule Ticketee.Notifications.TicketWatcher do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias Ticketee.Projects.Ticket
  alias Ticketee.Accounts.User

  schema "ticket_watchers" do
    belongs_to :ticket, Ticket
    belongs_to :user, User, type: :binary_id
  end

  def changeset_for(%Ticket{id: ticket_id}, %User{id: user_id}) do
    %TicketWatcher{}
    |> change(ticket_id: ticket_id, user_id: user_id)
    |> validate_required([:ticket_id, :user_id])
    |> assoc_constraint(:ticket)
    |> assoc_constraint(:user)
    |> unique_constraint([:ticket_id, :user_id], message: "is already beeing watched")
  end
end
