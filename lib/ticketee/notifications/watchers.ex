defmodule Ticketee.Notifications.Watchers do
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Repo
  alias Ticketee.Notifications.TicketWatcher
  alias Ticketee.Projects.Ticket
  alias Ticketee.Accounts.User

  def list_ticket_watchers(ticket) do
    list_ticket_watchers_query(ticket)
    |> Repo.all()
  end

  defp list_ticket_watchers_query(%Ticket{id: ticket_id}) do
    from(u in User,
      join: w in TicketWatcher,
      on: w.user_id == u.id and w.ticket_id == ^ticket_id)
  end

  def list_ticket_watcher_addresses(ticket, opts \\ [])

  def list_ticket_watcher_addresses(ticket, []) do
    list_ticket_watchers_query(ticket)
    |> select_id_and_email()
    |> Repo.all()
  end

  def list_ticket_watcher_addresses(ticket, [exclude_ids: excluded_user_ids]) do
    from(u in list_ticket_watchers_query(ticket), where: u.id not in ^excluded_user_ids)
    |> select_id_and_email()
    |> Repo.all()
  end

  defp select_id_and_email(query) do
    from(u in query, select: {u.id, u.email})
  end

  def get_ticket_watcher(%Ticket{id: ticket_id}, %User{id: user_id}) do
    from(TicketWatcher, where: [ticket_id: ^ticket_id, user_id: ^user_id])
    |> Repo.one()
  end

  def create_ticket_watcher(ticket, user, on_conflict \\ :raise) do
    TicketWatcher.changeset_for(ticket, user)
    |> Repo.insert(on_conflict: on_conflict)
  end

  def delete_ticket_watcher(%Ticket{id: ticket_id}, %User{id: user_id}) do
    from(TicketWatcher, where: [ticket_id: ^ticket_id, user_id: ^user_id])
    |> Repo.delete_all()
    |> case do
        {1, nil} ->
          {:ok, %TicketWatcher{ticket_id: ticket_id, user_id: user_id}}

        {0, nil} ->
          {:error, :not_found}
      end
  end
end
