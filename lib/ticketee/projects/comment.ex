defmodule Ticketee.Projects.Comment do
  use Ecto.Schema
  import Ecto.Changeset

  alias Ticketee.Projects.{Ticket, State}
  alias Ticketee.Accounts.User

  schema "comments" do
    field :description, :string

    belongs_to :ticket, Ticket
    belongs_to :user, User, type: :binary_id
    belongs_to :state, State
    belongs_to :previous_state, State

    timestamps(type: :utc_datetime)
  end

  def changeset(comment, attrs \\ %{}) do
    comment
    |> cast(attrs, [:description, :state_id])
    |> validate_required([:description])
    |> assoc_constraint(:previous_state)
    |> assoc_constraint(:state)
  end
end
