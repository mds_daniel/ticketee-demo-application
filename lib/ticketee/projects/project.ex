defmodule Ticketee.Projects.Project do
  use Ecto.Schema
  import Ecto.Changeset

  alias Ticketee.Permalink
  alias Ticketee.Projects.Ticket

  @primary_key {:id, Permalink, autogenerate: true}
  schema "projects" do
    field :name, :string
    field :description, :string
    field :slug, :string

    has_many :tickets, Ticket

    timestamps(type: :utc_datetime)
  end

  def changeset(project, attrs) do
    project
    |> cast(attrs, [:name, :description])
    |> slugify_name()
    |> validate_required([:name, :description])
    |> validate_length(:name, max: 255)
    |> unique_constraint(:name)
  end

  defp slugify_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} ->
        put_change(changeset, :slug, Permalink.slugify(new_name))

      :error ->
        changeset
    end
  end
end
