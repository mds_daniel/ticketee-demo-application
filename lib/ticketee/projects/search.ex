defmodule Ticketee.Projects.Search do
  use Ecto.Schema
  import Ecto.Changeset, only: [cast: 3, apply_changes: 1]
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Permalink

  embedded_schema do
    field :project_id, Permalink
    field :state_id, :id
    field :state_name, :string
    field :tag_name, :string
    field :ticket_title, :string
  end

  def changeset(attrs) do
    %__MODULE__{}
    |> cast(attrs, [:project_id, :state_id, :state_name, :tag_name, :ticket_title])
  end

  def search_tickets(queryable, opts) do
    query = from(queryable, order_by: :id)

    changeset(opts)
    |> apply_changes()
    |> Map.from_struct()
    |> Enum.reduce(query, fn {key, value}, query ->
      search_by(query, key, value)
    end)
  end

  def search_by(query, key, value)

  def search_by(query, _any_key, nil) do
    query
  end

  def search_by(query, :project_id, project_id) do
    from(query, where: [project_id: type(^project_id, Permalink)])
  end

  def search_by(query, :state_id, state_id) do
    from(query, where: [state_id: ^state_id])
  end

  def search_by(query, :state_name, state_name) do
    from(t in query,
      join: s in assoc(t, :state),
      where: s.name == ^state_name)
  end

  def search_by(query, :tag_name, tag_name) do
    from(t in query,
      join: tag in assoc(t, :tags),
      on: tag.name == ^tag_name)
  end

  def search_by(query, :ticket_title, ticket_title) do
    ts_query = to_tsquery(ticket_title)
    from(t in query, where: fragment("? @@ to_tsquery('pg_catalog.english', ?)", t.tsv, ^ts_query))
  end

  def search_by(query, _key, _value) do
    query
  end

  @word_regex ~r{\W}u

  def to_tsquery(text) do
    text
    |> String.split(@word_regex, trim: true)
    |> Enum.map(fn word -> word <> ":*" end)
    |> Enum.join(" & ")
  end
end
