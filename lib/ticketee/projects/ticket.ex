defmodule Ticketee.Projects.Ticket do
  use Ecto.Schema
  import Ecto.Changeset

  alias Ticketee.Projects.{Project, State}
  alias Ticketee.Accounts.User
  alias Ticketee.Tags.Tag

  schema "tickets" do
    field :title, :string
    field :description, :string
    field :comment_count, :integer, default: 0

    belongs_to :project, Project
    belongs_to :author, User, type: :binary_id
    belongs_to :state, State
    many_to_many :tags, Tag, join_through: "ticket_tags"

    timestamps(type: :utc_datetime)
  end

  def changeset(ticket, attrs) do
    ticket
    |> cast(attrs, [:title, :description])
    |> validate_required([:title, :description])
    |> validate_length(:title, max: 255)
    |> assoc_constraint(:project)
  end

  def state_changeset(ticket, attrs) do
    ticket
    |> cast(attrs, [:state_id])
    |> assoc_constraint(:state)
  end
end
