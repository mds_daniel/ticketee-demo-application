defmodule Ticketee.Projects.CommentCounters do
  import Ecto.Query, only: [from: 2]

  alias Ecto.Multi
  alias Ticketee.Repo
  alias Ticketee.Projects.Ticket

  def update_ticket_comment_counters(query \\ Ticket) do
    from(t in query, update: [set: [
          comment_count: fragment("(
            SELECT count(comments.id)
            FROM comments
            WHERE comments.ticket_id = ?
          )", t.id)
    ]])
    |> Repo.update_all([])
  end

  def increment_comment_count_step(multi, ticket_id, amount \\ 1) do
    Multi.update_all(
      multi,
      :ticket_comment_count,
      from(Ticket, where: [id: ^ticket_id]),
      inc: [comment_count: amount]
    )
  end

  def decrement_comment_count_step(multi, ticket_id, amount \\ 1) do
    increment_comment_count_step(multi, ticket_id, -amount)
  end
end
