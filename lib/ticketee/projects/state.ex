defmodule Ticketee.Projects.State do
  use Ecto.Schema
  import Ecto.Changeset

  schema "states" do
    field :name, :string
    field :color, :string
    field :order_no, :integer, default: 0
    field :default, :boolean, default: false
  end

  def changeset(state, attrs) do
    state
    |> cast(attrs, [:name, :color, :order_no])
    |> validate_required([:name, :color])
    |> unique_constraint(:name)
  end

  def set_default_changeset(state, attrs) do
    state
    |> cast(attrs, [:default])
    |> validate_required([:default])
  end

  def delete_changeset(state) do
    state
    |> change()
    |> foreign_key_constraint(:name,
          name: :tickets_state_id_fkey,
          message: "cannot delete while tickets have this state")
    |> foreign_key_constraint(:name,
          name: :comments_state_id_fkey,
          message: "cannot delete while comments have this state")
    |> foreign_key_constraint(:name,
          name: :comments_previous_state_id_fkey,
          message: "cannot delete while comments have this previous state")
  end
end
