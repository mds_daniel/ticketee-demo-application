defmodule Ticketee.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias Ticketee.Passwords

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "users" do
    field :email, :string
    field :admin, :boolean, default: false
    field :password, :string, virtual: true
    field :password_hash, :string
    field :archived_at, :utc_datetime

    # Prevent multiple `roles` queries when rendering views by caching
    field :cached_roles, :map, default: %{}, virtual: true

    timestamps(type: :utc_datetime)
  end

  ## Helpers

  def admin?(%User{admin: true}), do: true

  def admin?(_other), do: false

  def archived?(%User{archived_at: %DateTime{}}), do: true

  def archived?(%User{archived_at: nil}), do: false

  ## Changesets

  def changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:email])
    |> validate_required([:email])
    |> email_validations()
  end

  defp email_validations(changeset) do
    changeset
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:email)
  end

  def registration_changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:email, :password])
    |> validate_required([:email, :password])
    |> email_validations()
    |> password_validations()
  end

  defp password_validations(changeset) do
    changeset
    |> validate_length(:password, min: 6, max: 255)
    |> validate_confirmation(:password)
    |> put_pass_hash()
    |> put_change(:password, nil)
  end

  def admin_changeset(user, attrs \\ %{}) do
    user
    |> registration_changeset(attrs)
    |> cast(attrs, [:admin])
  end

  def update_account_changeset(user, attrs \\ %{}) do
    attrs = ignore_password_when_blank(attrs)

    user
    |> cast(attrs, [:email, :password])
    |> validate_required([:email]) # password is *NOT* required when updating account
    |> email_validations()
    |> password_validations()
  end

  defp put_pass_hash(%Ecto.Changeset{
      valid?: true,
      changes: %{password: password}
    } = changeset) do
    put_change(changeset, :password_hash, Passwords.hash(password))
  end

  defp put_pass_hash(other) do
    other
  end

  defp ignore_password_when_blank(%{"password" => password} = attrs) do
    case String.trim(password) do
      "" ->
        attrs
        |> Map.delete("password")
        |> Map.delete("password_confirmation")

      _other ->
        attrs
    end
  end

  defp ignore_password_when_blank(%{password: password} = attrs) do
    case String.trim(password) do
      "" ->
        attrs
        |> Map.delete(:password)
        |> Map.delete(:password_confirmation)

      _other ->
        attrs
    end

  end

  defp ignore_password_when_blank(other) do
    other
  end
end
