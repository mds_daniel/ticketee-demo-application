defmodule Ticketee.Accounts do
  @moduledoc """
  `Accounts` context module for managing users.
  """
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Repo
  alias Ticketee.Passwords
  alias Ticketee.Accounts.User

  def list_users do
    Repo.all(from(User, order_by: :email))
  end

  def get_user(user_id) do
    Repo.get(User, user_id)
  end

  def get_user_by(attrs) do
    Repo.get_by(User, attrs)
  end

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def register_user(attrs \\ %{}) do
    %User{}
    |> User.registration_changeset(attrs)
    |> Repo.insert()
  end

  def admin_register_user(attrs \\ %{}) do
    %User{}
    |> User.admin_changeset(attrs)
    |> Repo.insert()
  end

  def update_user_account(user = %User{}, attrs) do
    user
    |> User.update_account_changeset(attrs)
    |> Repo.update()
  end

  def promote_to_admin(user = %User{}) do
    user
    |> Ecto.Changeset.change(admin: true)
    |> Repo.update()
  end

  def change_user(user \\ %User{}, attrs) do
    User.changeset(user, attrs)
  end

  def authenticate(email, password) do
    with %User{} = user <- get_user_by(email: email),
         true <- Passwords.verify(password, user.password_hash) do
      {:ok, user}
    else
      false ->
        {:error, :unauthorized}

      nil ->
        Passwords.verify(password, nil)
        {:error, :not_found}
    end
  end

  def archive_user(%User{archived_at: %DateTime{}} = user) do
    # user already archived
    {:ok, user}
  end

  def archive_user(%User{id: user_id, archived_at: nil} = user) do
    archived_now = DateTime.utc_now()

    from(u in User,
      select: {u.id, u.archived_at},
      where: [id: ^user_id],
      update: [
        set: [archived_at: fragment("coalesce(?, ?)", u.archived_at, ^archived_now)
      ]])
    |> Repo.update_all([])
    |> case do
      {1, [{^user_id, archived_at}]} ->
        {:ok, %User{user | archived_at: archived_at}}

      {0, []} ->
        {:error, :not_found}
    end
  end

  def restore_archived_user(user) do
    user
    |> Ecto.Changeset.change(archived_at: nil)
    |> Repo.update()
  end
end
