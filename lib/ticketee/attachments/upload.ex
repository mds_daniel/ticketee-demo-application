defmodule Ticketee.Attachments.Upload do
  use Arc.Definition
  alias Ticketee.Attachments.TicketAttachment

  @storage_dir "priv/uploads"
  @image_types ~w(.jpg .jpeg .gif .png)
  @extension_whitelist (@image_types ++ ~w(.txt .xlsx))

  @allowed_mime_types (
    @extension_whitelist
    |> Enum.map(fn extname ->
        extname
        |> String.trim_leading(".")
        |> MIME.type()
      end)
  )

  def allowed_mime_types do
    @allowed_mime_types
  end

  def allowed_image_extensions do
    @image_types
  end

  def image_type?(filename) when is_binary(filename) do
    Path.extname(filename) in @image_types
  end

  def validate({file, _}) do
    validate_filename(file.file_name)
  end

  def validate_filename(filename) do
    file_extension = filename |> Path.extname() |> String.downcase()

    file_extension in @extension_whitelist
  end

  def __storage do
    Arc.Storage.Local
  end

  def storage_dir(_version, {_file, %{__struct__: TicketAttachment, ticket_id: ticket_id}}) do
    ticket_attachment_dir(ticket_id)
  end

  ## Path helpers

  defp ticket_attachment_dir(ticket_id) do
    "#{@storage_dir}/tickets/#{ticket_id}"
  end

  def ticket_attachment_path(ticket_id, filename)
      when is_integer(ticket_id) and is_binary(filename) do
    Path.join([
      Application.app_dir(:ticketee),
      ticket_attachment_dir(ticket_id),
      filename
    ])
  end

  def get_attachment_size(path) when is_binary(path) do
    File.stat!(path).size
  end
end
