defmodule Ticketee.Attachments.Helpers do
  @moduledoc """
  Helpers for attachment files, such as file size descriptions.
  """
  @one_kilobyte 1024
  @one_megabyte @one_kilobyte * @one_kilobyte
  @one_gigabyte @one_kilobyte * @one_megabyte

  def file_size_description(size)

  def file_size_description(size) when size >= @one_gigabyte do
    file_size_quantify(size / @one_gigabyte, "GB")
  end

  def file_size_description(size) when size >= @one_megabyte do
    file_size_quantify(size / @one_megabyte, "MB")
  end

  def file_size_description(size) when size >= @one_kilobyte do
    file_size_quantify(size / @one_kilobyte, "KB")
  end

  def file_size_description(size) when is_integer(size) do
    "#{size} B"
  end

  defp file_size_quantify(size, quantifier) do
    "#{Float.round(size, 2)} #{quantifier}"
  end
end
