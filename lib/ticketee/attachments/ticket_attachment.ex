defmodule Ticketee.Attachments.TicketAttachment do
  use Ecto.Schema
  import Ecto.Changeset

  alias __MODULE__
  alias Ticketee.Attachments.{Upload, FilenameType}
  alias Ticketee.Projects.Ticket

  schema "ticket_attachments" do
    field :file, FilenameType
    field :size, :integer, default: 0
    field :description, :string

    belongs_to :ticket, Ticket

    timestamps(type: :utc_datetime)
  end

  ## Changesets

  def changeset(attachment, attrs \\ %{}) do
    attachment
    |> cast(attrs, [:file, :description])
    |> validate_required([:file])
    |> validate_file_extension(:file)
    |> put_file_size(attrs["file"] || attrs[:file])
    |> check_constraint(:size, name: :size_must_be_non_negative, message: "must be non-negative")
    |> unique_constraint(:file,
          name: :ticket_attachments_ticket_id_file_index,
          message: "filename must be unique for each one of this ticket's attachments")
  end

  defp validate_file_extension(changeset, field) do
    validate_change(changeset, field, fn ^field, filename ->
      if Upload.validate_filename(filename) do
        []
      else
        [{:file, "invalid file extension"}]
      end
    end)
  end

  defp put_file_size(changeset, upload)

  defp put_file_size(changeset = %Ecto.Changeset{valid?: true}, %Plug.Upload{path: path}) do
    case fetch_change(changeset, :file) do
      {:ok, _filename} ->
        put_change(changeset, :size, Upload.get_attachment_size(path))

      :error ->
        changeset
    end
  end

  defp put_file_size(other_changeset, _upload), do: other_changeset

  ## Helpers

  def attachment_path(%TicketAttachment{ticket_id: ticket_id, file: filename}) do
    Upload.ticket_attachment_path(ticket_id,  filename)
  end

  def attachment_path(_other) do
    nil
  end

  def matches_attachment?(%TicketAttachment{file: filename}, filename) do
    true
  end

  def matches_attachment?(_attachment, _other_filename) do
    false
  end
end
