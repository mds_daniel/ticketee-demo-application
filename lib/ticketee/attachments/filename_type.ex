defmodule Ticketee.Attachments.FilenameType do
  @moduledoc """
  Use own `Ecto.Type` for attachment handling.
  Version cache busting will rely on the `updated_at` field of the record instead.
  """
  use Ecto.Type

  @impl true
  def type do
    :string
  end

  @impl true
  def cast(value)

  def cast(%Plug.Upload{filename: filename}) do
    {:ok, normalize_filename(filename)}
  end

  def cast(filename) when is_binary(filename) do
    {:ok, normalize_filename(filename)}
  end

  def cast(_other) do
    :error
  end

  @impl true
  def load(value)

  def load(value) when is_binary(value) do
    {:ok, value}
  end

  def load(nil) do
    {:ok, nil}
  end

  def load(_other) do
    :error
  end

  @impl true
  def dump(value) do
    {:ok, value}
  end

  ## Helpers

  @filename_regex ~r/[^\w_-]+/u

  def normalize_filename(filename) when is_binary(filename) do
    extname = Path.extname(filename)
    basename = Path.basename(filename, extname)

    basename
    |> String.downcase()
    |> String.replace(@filename_regex, "_")
    |> Kernel.<>(String.downcase(extname))
  end
end
