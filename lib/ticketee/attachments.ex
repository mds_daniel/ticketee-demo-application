defmodule Ticketee.Attachments do
  @moduledoc """
  Context module for managing attachment file uploads.
  """
  import Ecto.Query, only: [from: 2]
  require Logger

  alias Ticketee.Repo
  alias Ticketee.Attachments.{Upload, TicketAttachment}
  alias Ticketee.Projects.Ticket

  ## Utilities

  defdelegate file_size_description(size), to: Ticketee.Attachments.Helpers

  ## Ticket Attachments

  def attachments_for(%Ticket{id: ticket_id}) do
    from(TicketAttachment, where: [ticket_id: ^ticket_id])
    |> Repo.all()
  end

  def get_ticket_attachment(%Ticket{id: ticket_id}, filename) when is_binary(filename) do
    Repo.get_by(TicketAttachment, ticket_id: ticket_id, file: filename)
  end

  def change_ticket_attachment_for(%Ticket{id: ticket_id}, attrs) do
    %TicketAttachment{ticket_id: ticket_id}
    |> TicketAttachment.changeset(attrs)
  end

  def create_ticket_attachment(%Ticket{id: ticket_id}, attrs) do
    %TicketAttachment{ticket_id: ticket_id}
    |> TicketAttachment.changeset(attrs)
    |> store_ticket_attachment(attrs["file"] || attrs[:file])
    |> Repo.insert()
  end

  def delete_ticket_attachment(%TicketAttachment{file: filename} = attachment)
      when is_binary(filename) do
    with {:ok, attachment} <- Repo.delete(attachment),
         {:remove_file, :ok} <- {:remove_file, File.rm(TicketAttachment.attachment_path(attachment))} do
      {:ok, attachment}
    else
      {:remove_file, {:error, _} = err} ->
        Logger.error(attachment_file_error_message(attachment, err))
        err

      {:error, %Ecto.Changeset{} = changeset} = err ->
        Logger.error(attachment_changeset_error_message(changeset))
        err
    end
  end

  defp attachment_file_error_message(attachment, {:error, err}) do
    path = TicketAttachment.attachment_path(attachment)
    "#{__MODULE__} - delete attachment file error: '#{path}' - #{inspect(err)}"
  end

  defp attachment_changeset_error_message(%Ecto.Changeset{errors: errors}) do
    "#{__MODULE__} - delete attachment error: #{inspect(errors)}"
  end

  defp store_ticket_attachment(%Ecto.Changeset{valid?: true} = changeset, %Plug.Upload{} = upload) do
    attachment = Ecto.Changeset.apply_changes(changeset)
    upload = %Plug.Upload{upload | filename: attachment.file}

    case Upload.store({upload, attachment}) do
      {:ok, filename} ->
        Logger.info("#{__MODULE__} - Uploaded file #{filename}")
        changeset

      {:error, error} ->
        Logger.error("#{__MODULE__ } - ticket attachment upload error: #{inspect(error)}")
        Ecto.Changeset.add_error(changeset, :file, "failed to persist")
    end
  end

  defp store_ticket_attachment(changeset, _) do
    changeset
  end
end
