defmodule Ticketee.Tags.Tag do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tags" do
    field :name, :string
  end

  def changeset(tag, attrs \\ %{}) do
    tag
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> update_change(:name, &normalize_name/1)
    |> unique_constraint(:name)
  end

  def normalize_name(name) when is_binary(name) do
    String.trim(name)
  end
end
