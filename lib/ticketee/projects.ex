defmodule Ticketee.Projects do
  @moduledoc """
  Projects context module.
  """
  import Ecto.Query, only: [from: 2]

  alias Ecto.Multi

  alias Ticketee.Repo
  alias Ticketee.Projects.{Project, Ticket, Comment, State}
  alias Ticketee.Projects.{Search, CommentCounters}
  alias Ticketee.Accounts.User
  alias Ticketee.Tags
  alias Ticketee.Tags.Tag

  def get_project(id) do
    Repo.get(Project, id)
  end

  def get_project_by_name(name) do
    Repo.get_by(Project, name: name)
  end

  def list_projects do
    list_projects_query()
    |> Repo.all()
  end

  def list_projects_for_search do
    from(list_projects_query(), select: [:id, :name, :slug])
    |> Repo.all()
  end

  def list_projects_query do
    from(Project, order_by: :id)
  end

  def create_project(attrs) do
    %Project{}
    |> Project.changeset(attrs)
    |> Repo.insert()
  end

  def update_project(project = %Project{}, attrs) do
    project
    |> Project.changeset(attrs)
    |> Repo.update()
  end

  def change_project(project \\ %Project{}, attrs) do
    Project.changeset(project, attrs)
  end

  def delete_project(project = %Project{}) do
    Repo.delete(project)
  end

  def fetch_project(id) do
    case get_project(id) do
      project when not is_nil(project) ->
        {:ok, project}

      nil ->
        {:error, :not_found}
    end

  rescue
    Ecto.Query.CastError ->
      {:error, :invalid}
  end

  ## Tickets

  def list_tickets_for_index(project) do
    from(t in Ecto.assoc(project, :tickets), order_by: t.id)
    |> Repo.all()
    |> with_state()
  end

  def search_tickets_query(opts) do
    Ticket
    |> Search.search_tickets(opts)
  end

  def search_tickets(opts) do
    search_tickets_query(opts)
    |> Repo.all()
  end

  def get_ticket(ticket_id) do
    Repo.get(Ticket, ticket_id)
  end

  def get_ticket_for(project \\ %Project{}, ticket_id) do
    from(Ecto.assoc(project, :tickets), where: [id: ^ticket_id])
    |> Repo.one()
  end

  def with_tickets(project_or_projects) do
    Repo.preload(project_or_projects, [:tickets])
  end

  def with_ticket_author(ticket_or_tickets) do
    Repo.preload(ticket_or_tickets, [:author])
  end

  def with_ticket_project(ticket_or_tickets) do
    Repo.preload(ticket_or_tickets, [:project])
  end

  def create_ticket(project = %Project{}, attrs) do
    %Ticket{}
    |> Ticket.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:project, project)
    |> Repo.insert()
  end

  def create_ticket_for(user = %User{}, project = %Project{}, attrs) do
    create_ticket_changeset_for(user, project, attrs)
    |> Repo.insert()
  end

  defp create_ticket_changeset_for(user = %User{}, project = %Project{}, attrs) do
    %Ticket{}
    |> Ticket.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:project, project)
    |> Ecto.Changeset.put_assoc(:author, user)
  end

  def create_ticket_with_default_state_for(user = %User{}, project = %Project{}, attrs) do
    create_ticket_changeset_for(user, project, attrs)
    |> add_ticket_default_state()
    |> Repo.insert()
  end

  defp add_ticket_default_state(changeset) do
    case get_default_state() do
      %State{id: state_id} ->
        Ticket.state_changeset(changeset, %{state_id: state_id})

      nil ->
        changeset
    end
  end

  def update_ticket(ticket = %Ticket{}, attrs) do
    ticket
    |> Ticket.changeset(attrs)
    |> Repo.update()
  end

  def change_ticket(ticket \\ %Ticket{}, attrs) do
    Ticket.changeset(ticket, attrs)
  end

  def change_ticket_for(project = %Project{}, attrs) do
    Ecto.build_assoc(project, :tickets)
    |> Ticket.changeset(attrs)
  end

  def delete_ticket(ticket = %Ticket{}) do
    Repo.delete(ticket)
  end

  ## Ticket Tags

  def with_ticket_tags(ticket_or_tickets) do
    Repo.preload(ticket_or_tickets, [tags: Tags.list_tags_query()])
  end

  def create_ticket_tags(%Ticket{id: ticket_id}, names) when is_list(names) do
    Multi.new()
    |> Multi.run(:tags, fn _repo, _changes -> {:ok, Tags.create_tags(names)} end)
    |> Multi.run(:ticket_tags, Tags.create_ticket_tag_entries(ticket_id))
    |> Repo.transaction()
    |> case do
        {:ok, %{tags: tags}} ->
          {:ok, tags}

        {:error, step, error, _changes} ->
          {:error, {:failed, step, error, names}}
      end
  end

  def delete_ticket_tag(%Ticket{id: ticket_id}, %Tag{id: tag_id}) do
    Tags.delete_ticket_tag_entry(ticket_id, tag_id)
  end

  ## Comments

  def with_user(record_or_records) do
    Repo.preload(record_or_records, [:user])
  end

  def with_previous_state(record_or_records) do
    Repo.preload(record_or_records, [:previous_state])
  end

  def with_ticket(record_or_records) do
    Repo.preload(record_or_records, [:ticket])
  end

  def get_comment(comment_id) do
    Repo.get(Comment, comment_id)
  end

  def get_comment_for(%Ticket{id: ticket_id}, comment_id) do
    Repo.get_by(Comment, id: comment_id, ticket_id: ticket_id)
  end

  def change_comment_for(%Ticket{id: ticket_id, state_id: state_id}, attrs) do
    change_comment(%Comment{ticket_id: ticket_id, state_id: state_id}, attrs)
  end

  def change_comment(comment = %Comment{}, attrs) do
    comment
    |> Comment.changeset(attrs)
  end

  def list_ticket_comments(ticket) do
    Repo.all(ticket_comments_query(ticket))
  end

  defp ticket_comments_query(%Ticket{id: ticket_id}) do
    from(Comment, where: [ticket_id: ^ticket_id], order_by: [:inserted_at, :id])
  end

  def list_ticket_comments_for_index(ticket) do
    from(c in ticket_comments_query(ticket),
      left_join: u in assoc(c, :user),
      select: [:id, :description, :ticket_id, :user_id, :state_id, :previous_state_id, :inserted_at,
              user: [:id, :email]],
      preload: [user: u])
    |> Repo.all()
  end

  def create_comment_for(user, ticket, attrs) do
    Multi.new()
    |> Multi.insert(:comment, create_comment_changeset_for(user, ticket, attrs))
    |> Multi.update(:ticket, Ticket.state_changeset(ticket, attrs))
    |> CommentCounters.increment_comment_count_step(ticket.id)
    |> Repo.transaction()
    |> case do
        {:ok, %{comment: comment}} ->
          {:ok, comment}

        {:error, :comment, changeset, _changes} ->
          {:error, changeset}
       end
  end

  defp create_comment_changeset_for(user = %User{}, ticket = %Ticket{state_id: state_id}, attrs) do
    %Comment{previous_state_id: state_id, state_id: state_id}
    |> Comment.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:ticket, ticket)
    |> Ecto.Changeset.put_assoc(:user, user)
  end

  def update_comment(comment = %Comment{}, attrs) do
    comment
    |> Comment.changeset(attrs)
    |> Repo.update()
  end

  def delete_comment(comment = %Comment{ticket_id: ticket_id}) do
    Multi.new()
    |> Multi.delete(:comment, comment)
    |> CommentCounters.decrement_comment_count_step(ticket_id)
    |> Repo.transaction()
    |> case do
        {:ok, %{comment: comment}} ->
          {:ok, comment}

        {:error, :comment, changeset, _changes} ->
          {:error, changeset}
       end
  end

  def update_ticket_comment_counters(query \\ Ticket) do
    CommentCounters.update_ticket_comment_counters(query)
  end

  ## States

  def with_state(record_or_records) do
    Repo.preload(record_or_records, [:state])
  end

  def list_states do
    Repo.all(from(State, order_by: :order_no))
  end

  def get_state(state_id) do
    Repo.get(State, state_id)
  end

  def get_state_by(attrs) do
    Repo.get_by(State, attrs)
  end

  def create_state(attrs) do
    %State{}
    |> State.changeset(attrs)
    |> Repo.insert()
  end

  def update_state(state, attrs) do
    state
    |> State.changeset(attrs)
    |> Repo.update()
  end

  def change_state(state \\ %State{}, attrs) do
    State.changeset(state, attrs)
  end

  def delete_state(state = %State{}) do
    state
    |> State.delete_changeset()
    |> Repo.delete()
  end

  def get_default_state do
    get_state_by(default: true)
  end

  def set_default_state(state = %State{}) do
    Multi.new()
    |> Multi.update_all(:states, State, set: [default: false])
    |> Multi.update(:state, State.set_default_changeset(state, %{default: true}))
    |> Repo.transaction()
    |> case do
      {:ok, %{state: state}} ->
        {:ok, state}

      {:error, :state, changeset, _changes} ->
        {:error, changeset}
    end
  end
end
