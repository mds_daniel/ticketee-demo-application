defmodule Ticketee.Repo do
  use Ecto.Repo,
    otp_app: :ticketee,
    adapter: Ecto.Adapters.Postgres
end
