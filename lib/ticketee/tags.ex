defmodule Ticketee.Tags do
  @moduledoc """
  Context module for managing tags.
  """
  import Ecto.Query, only: [from: 2]

  alias Ticketee.Repo
  alias Ticketee.Tags.Tag

  def get_tag(tag_id) do
    Repo.get(Tag, tag_id)
  end

  def get_tag_by_name(name) do
    Repo.get_by(Tag, name: name)
  end

  def list_tags_query do
    from(Tag, order_by: :name)
  end

  def list_tags do
    list_tags_query()
    |> Repo.all()
  end

  def create_tag(attrs) do
    %Tag{}
    |> Tag.changeset(attrs)
    |> Repo.insert()
  end

  def update_tag(tag, attrs) do
    tag
    |> Tag.changeset(attrs)
    |> Repo.update()
  end

  def delete_tag(tag) do
    Repo.delete(tag)
  end

  ## Ticket tag upsert support

  def create_tags(names) when is_list(names) do
    names = names |> Enum.uniq() |> filter_tag_names()
    num_records = Enum.count(names)
    entries = Enum.map(names, fn name when is_binary(name) -> [{:name, name}] end)

    Repo.insert_all(Tag, entries,
      conflict_target: :name,
      # must set name so existing records are returned by `returning` clause
      on_conflict: {:replace, [:name]},
      returning: true)
    |> case do
        {^num_records, records} when is_list(records) ->
          records
       end
  end

  defp filter_tag_names(names) do
    names
    |> Enum.reduce([], fn name, acc ->
        case Tag.normalize_name(name) do
          "" -> acc
          tag_name -> [tag_name | acc]
        end
      end)
    |> Enum.reverse()
  end

  def create_ticket_tag_entries(ticket_id) do
    fn repo, %{tags: tags} ->
      entries = Enum.map(tags, fn tag -> %{tag_id: tag.id, ticket_id: ticket_id} end)

      repo.insert_all("ticket_tags", entries,
          on_conflict: :nothing,
          conflict_target: [:ticket_id, :tag_id],
          returning: [:ticket_id, :tag_id])
      |> case do
          {num_records, records} when is_integer(num_records) and is_list(records) ->
            {:ok, records}
        end
    end
  end

  def delete_ticket_tag_entry(ticket_id, tag_id) do
    from("ticket_tags", where: [ticket_id: ^ticket_id, tag_id: ^tag_id])
    |> Repo.delete_all()
    |> case do
        {1, nil} ->
          {:ok, %{tag_id: tag_id, ticket_id: ticket_id}}

        {0, _} ->
          {:error, {:not_found, %{tag_id: tag_id, ticket_id: ticket_id}}}
      end
  end
end
