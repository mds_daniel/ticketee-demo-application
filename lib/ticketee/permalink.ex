defmodule Ticketee.Permalink do
  use Ecto.Type

  @slugify_regex ~r/[^\w-]+/u

  def slugify(text) do
    text
    |> String.downcase()
    |> String.replace(@slugify_regex, "-")
  end

  ## `Ecto.Type` implementation

  @impl true
  def type do
    :id
  end

  @impl true
  def cast(value)

  def cast(binary) when is_binary(binary) do
    case Integer.parse(binary) do
      {value, _rest} when value > 0 ->
        {:ok, value}

      _ ->
        :error
    end
  end

  def cast(integer) when is_integer(integer) do
    {:ok, integer}
  end

  def cast(_other) do
    :error
  end

  @impl true
  def dump(value) when is_integer(value) do
    {:ok, value}
  end

  @impl true
  def load(value) when is_integer(value) do
    {:ok, value}
  end
end
