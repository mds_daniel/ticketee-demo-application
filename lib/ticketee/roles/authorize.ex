defmodule Ticketee.Roles.Authorize do
  @moduledoc """
  Describes permissions roles have on other resources.
  """
  alias Ticketee.Roles.Role
  alias Ticketee.Projects.{Project, Ticket, Comment}
  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.Tags.Tag
  alias Ticketee.Notifications.TicketWatcher

  # `create` is queried on the schema module atom
  @crud_actions ~w(show update delete)a

  def allowed?(role, action, resource)

  ## Alias `new` and `edit` permissions to prevent duplication

  def allowed?(role, :new, resource), do: allowed?(role, :create, resource)

  def allowed?(role, :edit, resource), do: allowed?(role, :update, resource)

  ## Viewer permissions

  def allowed?(%Role{role: :viewer}, :show, %Project{}), do: true

  def allowed?(%Role{role: :viewer}, :show, %Ticket{}), do: true

  def allowed?(%Role{role: :viewer}, :show, {%Ticket{}, %TicketAttachment{}}), do: true

  # Comment

  def allowed?(%Role{role: :viewer}, :show, {%Ticket{}, %Comment{}}), do: true

  def allowed?(%Role{role: :viewer}, :index, {%Ticket{}, Comment}), do: true

  def allowed?(%Role{role: :viewer}, :create, {%Ticket{}, Comment}), do: true

  for action <- ~w(update delete)a do
    def allowed?(%Role{role: :viewer, user_id: user_id},
                 unquote(action),
                 {%Ticket{}, %Comment{user_id: user_id}}) when not is_nil(user_id) do
      true
    end
  end

  # Ticket Watcher

  def allowed?(%Role{role: :viewer}, :create, {%Ticket{}, TicketWatcher}), do: true

  def allowed?(%Role{role: :viewer}, :delete, {%Ticket{}, TicketWatcher}), do: true


  ## Editor permissions

  def allowed?(%Role{role: :editor}, :show, %Project{}), do: true

  # Ticket

  def allowed?(%Role{role: :editor}, :show, %Ticket{}), do: true

  def allowed?(%Role{role: :editor}, :create, Ticket), do: true

  def allowed?(%Role{role: :editor, user_id: user_id},
               :update,
               %Ticket{author_id: user_id}) when not is_nil(user_id) do
    true
  end

  # TicketAttachment

  def allowed?(%Role{role: :editor}, :show, {%Ticket{}, %TicketAttachment{}}), do: true

  def allowed?(%Role{role: :editor, user_id: user_id},
               :create,
               {%Ticket{author_id: user_id}, TicketAttachment}) when not is_nil(user_id) do
    true
  end

  # Comment

  def allowed?(%Role{role: :editor}, :show, {%Ticket{}, %Comment{}}), do: true

  def allowed?(%Role{role: :editor}, :index, {%Ticket{}, Comment}), do: true

  def allowed?(%Role{role: :editor}, :create, {%Ticket{}, Comment}), do: true

  for action <- ~w(update delete)a do
    def allowed?(%Role{role: :editor, user_id: user_id},
                 unquote(action),
                 {%Ticket{}, %Comment{user_id: user_id}}) when not is_nil(user_id) do
      true
    end
  end

  # Ticket Watcher

  def allowed?(%Role{role: :editor}, :create, {%Ticket{}, TicketWatcher}), do: true

  def allowed?(%Role{role: :editor}, :delete, {%Ticket{}, TicketWatcher}), do: true

  ## Manager permissions

  def allowed?(%Role{role: :manager}, :show, %Project{}), do: true

  def allowed?(%Role{role: :manager}, :update, %Project{}), do: true

  # Ticket

  def allowed?(%Role{role: :manager}, :create, Ticket), do: true

  for action <- @crud_actions do
    def allowed?(%Role{role: :manager}, unquote(action), %Ticket{}), do: true
  end

  # TicketAttachment

  def allowed?(%Role{role: :manager}, :create, {%Ticket{}, TicketAttachment}), do: true

  for action <- @crud_actions do
    def allowed?(%Role{role: :manager}, unquote(action), {%Ticket{}, %TicketAttachment{}}), do: true
  end

  # Comment

  def allowed?(%Role{role: :manager}, :index, {%Ticket{}, Comment}), do: true

  def allowed?(%Role{role: :manager}, :create, {%Ticket{}, Comment}), do: true

  for action <- @crud_actions do
    def allowed?(%Role{role: :manager}, unquote(action), {%Ticket{}, %Comment{}}), do: true
  end

  # Tag

  def allowed?(%Role{role: :manager}, :index, {%Ticket{}, Tag}), do: true

  def allowed?(%Role{role: :manager}, :create, {%Ticket{}, Tag}), do: true

  for action <- @crud_actions do
    def allowed?(%Role{role: :manager}, unquote(action), {%Ticket{}, %Tag{}}), do: true
  end

  # Ticket Watcher

  def allowed?(%Role{role: :manager}, :create, {%Ticket{}, TicketWatcher}), do: true

  def allowed?(%Role{role: :manager}, :delete, {%Ticket{}, TicketWatcher}), do: true


  # Catch-all deny
  def allowed?(_other_role, _other_action, _other_resource) do
    false
  end
end
