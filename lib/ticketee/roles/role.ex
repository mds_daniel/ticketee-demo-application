defmodule Ticketee.Roles.Role do
  use Ecto.Schema
  import Ecto.Changeset

  alias Ticketee.Accounts.User
  alias Ticketee.Projects.Project

  @available_roles [:viewer, :editor, :manager]

  schema "roles" do
    field :role, Ecto.Enum, values: @available_roles

    belongs_to :user, User, type: :binary_id
    belongs_to :project, Project

    timestamps(type: :utc_datetime)
  end

  def changeset(role, attrs) do
    role
    |> cast(attrs, [:role])
    |> validate_inclusion(:role, @available_roles)
    |> assoc_constraint(:user)
    |> assoc_constraint(:project)
  end

  def available_roles do
    @available_roles
  end
end
