defmodule TicketeeWeb.Notifications do
  require Logger

  alias Ticketee.Notifications.Watchers
  alias Ticketee.Projects
  alias TicketeeWeb.Notifications.{Email, Mailer}

  def add_ticket_watcher(conn, ticket) do
    current_user = conn.assigns.current_user

    case Watchers.create_ticket_watcher(ticket, current_user, _on_conflict = :nothing) do
      {:ok, _} ->
        :ok

      {:error, changeset} ->
        Logger.error("#{__MODULE__}: #{inspect(changeset)}")
    end

    conn
  end

  def notify_comment_created(conn, ticket, comment) do
    current_user = conn.assigns.current_user
    watchers = Watchers.list_ticket_watcher_addresses(ticket, exclude_ids: [current_user.id])

    # Useful for listing state transitions
    indexed_states = Projects.list_states()
      |> Enum.into(%{}, fn state -> {state.id, state} end)

    for {_id, email} <- watchers do
      send_comment_created_email(email, ticket, comment, indexed_states)
    end

    conn
  end

  def send_welcome_email(conn) do
    current_user = conn.assigns.current_user

    Email.welcome_email(current_user)
    |> Mailer.deliver_later()

    conn
  end

  def send_comment_created_email(email, ticket, comment, indexed_states \\ %{}) do
    Email.comment_created_email(email, ticket, comment, indexed_states)
    |> Mailer.deliver_later()
  end
end
