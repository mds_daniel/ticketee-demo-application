defmodule TicketeeWeb.UserRoleController do
  use TicketeeWeb, :controller
  import TicketeeWeb.UserController, only: [set_user: 2]

  alias Ticketee.{Roles, Accounts, Projects}

  @user_param_key "user_id"
  plug :set_user, [param_key: @user_param_key] when action in ~w(show)a
  plug :set_user_ajax when action in ~w(create delete)a

  def show(conn, _params) do
    user = conn.assigns.user
    projects = Projects.list_projects()
    role_summary = Roles.project_role_summary_for(user)

    render(conn, "show.html", user: user, projects: projects, role_summary: role_summary)
  end

  def create(conn, %{"role" => %{"project_id" => project_id, "role" => project_role}}) do
    user = conn.assigns.user

    with {:ok, project} <- Projects.fetch_project(project_id),
         {:ok, role} <- Roles.grant_role(user, project_role, project) do
      conn
      |> put_status(:created)
      |> render("role.json", role: role)
    else
      {:error, :not_found} ->
        json_error(conn, :not_found, %{"project_id" => ["not found"]})

      {:error, :invalid} ->
        json_error(conn, :unprocessable_entity, %{"project_id" => ["is invalid"]})

      {:error,  %Ecto.Changeset{} = changeset} ->
        json_error_changeset(conn, changeset)
    end
  end

  def delete(conn, %{"project_id" => project_id}) do
    user = conn.assigns.user

    with {:ok, project} <- Projects.fetch_project(project_id),
         {:role, {:ok, role}} <- {:role, Roles.revoke_role(user, project)} do
      render(conn, "role.json", role: role)
    else
      {:error, :not_found} ->
        json_error(conn, :not_found, %{"project_id" => ["not found"]})

      {:error, :invalid} ->
        json_error(conn, :unprocessable_entity, %{"project_id" => ["is invalid"]})

      {:role, {:error, :not_found}} ->
        json_error(conn, :not_found, %{"role" => ["not found"]})

      {:role, {:error,  %Ecto.Changeset{} = changeset}} ->
        json_error_changeset(conn, changeset)
    end
  end

  defp set_user_ajax(conn, _opts) do
    user_id = conn.params[@user_param_key]
    user = user_id && Accounts.get_user(user_id)

    if user do
      assign(conn, :user, user)
    else
      user_not_found_error(conn)
    end

  rescue
    Ecto.Query.CastError ->
      user_not_found_error(conn)
  end

  defp user_not_found_error(conn) do
    conn
    |> json_error(:not_found, %{"user_id" => ["not found"]})
    |> halt()
  end
end
