defmodule TicketeeWeb.TicketAttachmentController do
  use TicketeeWeb, :controller

  alias Ticketee.Projects
  alias Ticketee.Attachments
  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.Authorize

  plug :set_ticket
  plug :set_attachment when action in ~w(show delete)a
  plug :authorize_for_ticket

  def show(conn, _params) do
    attachment = conn.assigns.attachment
    send_download(conn, {:file, TicketAttachment.attachment_path(attachment)})
  end

  def new(conn, _params) do
    ticket = conn.assigns.ticket
    changeset = Attachments.change_ticket_attachment_for(ticket, %{})

    render(conn, "new.html", ticket: ticket, changeset: changeset)
  end

  def create(conn, %{"ticket_attachment" => attachment_params}) do
    ticket = conn.assigns.ticket
    project = Projects.get_project(ticket.project_id)

    case Attachments.create_ticket_attachment(ticket, attachment_params) do
      {:ok, _attachment} ->
        conn
        |> put_flash(:info, "Ticket attachment has been successfully created!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create attachment!")
        |> render("new.html", ticket: ticket, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    %{ticket: ticket, attachment: attachment} = conn.assigns
    project = Projects.get_project(ticket.project_id)

    case Attachments.delete_ticket_attachment(attachment) do
      {:ok, _attachment} ->
        conn
        |> put_flash(:info, "Ticket attachment has been successfully deleted!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))

      {:error, _any_error} ->
        conn
        |> put_flash(:error, "Failed to delete attachment!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))
    end
  end

  defp set_ticket(conn, _opts) do
    ticket_id = conn.params["ticket_id"]
    ticket = Projects.get_ticket(ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      conn
      |> resp(:not_found, "Ticket not found!")
      |> halt()
    end
  end

  defp set_attachment(conn, _opts) do
    ticket = conn.assigns.ticket
    filename = conn.params["id"]
    attachment = Attachments.get_ticket_attachment(ticket, filename)

    if TicketAttachment.matches_attachment?(attachment, filename) do
      assign(conn, :attachment, attachment)
    else
      conn
      |> resp(:not_found, "Attachment not found!")
      |> halt()
    end
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    attachment = conn.assigns[:attachment] || TicketAttachment

    if Authorize.allowed?(current_user, action_name(conn), {ticket, attachment}) do
      conn
    else
      conn
      |> resp(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end
end
