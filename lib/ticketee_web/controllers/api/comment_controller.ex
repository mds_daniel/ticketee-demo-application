defmodule TicketeeWeb.API.CommentController do
  use TicketeeWeb, :controller
  import TicketeeWeb.ControllerHelpers, only: [add_param: 3]
  import TicketeeWeb.API.ControllerHelpers, only: [resp_with_error_status: 3]
  import TicketeeWeb.API.TicketController, only: [set_ticket: 2]

  alias Ticketee.Authorize
  alias Ticketee.Projects
  alias Ticketee.Projects.Comment

  plug :set_comment when action in ~w(show)a
  plug :set_ticket, param_key: "ticket_id"
  plug :authorize_for_ticket

  def index(conn, _params) do
    ticket = conn.assigns.ticket

    comments = Projects.list_ticket_comments_for_index(ticket)
      |> preload_comment_associations()

    render(conn, "show.json", %{data: comments})
  end

  def show(conn, _opts) do
    %{comment: comment,
      ticket: ticket} = conn.assigns

    comment = %{comment | ticket: ticket}
      |> preload_comment_associations()

    render(conn, "show.json", %{data: comment})
  end

  ## Helpers

  defp preload_comment_associations(comment_or_comments) do
    comment_or_comments
    |> Projects.with_ticket()
    |> Projects.with_user()
    |> Projects.with_state()
    |> Projects.with_previous_state()
  end

  defp set_comment(conn, _opts) do
    comment_id = conn.params["id"]
    comment = Projects.get_comment(comment_id)

    if comment do
      conn
      |> assign(:comment, comment)
      |> add_param("ticket_id", comment.ticket_id)
    else
      conn
      |> resp_with_error_status(:not_found, "Comment not found")
      |> halt()
    end
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    comment = conn.assigns[:comment] || Comment

    if Authorize.allowed?(current_user, action_name(conn), {ticket, comment}) do
      conn
    else
      conn
      |> resp_with_error_status(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end
end
