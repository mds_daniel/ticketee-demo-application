defmodule TicketeeWeb.API.ProjectController do
  use TicketeeWeb, :controller
  import TicketeeWeb.API.ControllerHelpers, only: [resp_with_error_status: 3]

  alias Ticketee.Projects
  alias Ticketee.Authorize

  plug :set_project when action in ~w(show)a
  plug :authorize_for_project when action in ~w(show)a

  def index(conn, _params) do
    current_user = conn.assigns.current_user
    projects = Authorize.list_projects_for(current_user)

    render(conn, "show.json", %{data: projects})
  end

  def show(conn, _params) do
    project = conn.assigns.project

    render(conn, "show.json", %{data: project})
  end

  ## Helpers

  def set_project(conn, opts) do
    param_key = Keyword.get(opts, :param_key, "id")
    id = conn.params[param_key]
    project = id && Projects.get_project(id)

    if project do
      assign(conn, :project, project)
    else
      resp_with_error_status(conn, :not_found, "Project not found")
    end
  end

  defp authorize_for_project(conn, _opts) do
    %{current_user: current_user,
      project: project} = conn.assigns

    if Authorize.allowed?(current_user, :show, project) do
      conn
    else
      resp_with_error_status(conn, :forbidden, Authorize.not_authorized_message())
    end
  end
end
