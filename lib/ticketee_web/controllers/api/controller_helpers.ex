defmodule TicketeeWeb.API.ControllerHelpers do
  alias JSONAPI.ErrorView

  def resp_with_error_status(conn, status, detail \\ nil) do
    ErrorView.send_error(conn, status, build_error_from_status(status, detail))
  end

  defp build_error_from_status(status, detail) do
    code = Plug.Conn.Status.code(status)
    reason = Plug.Conn.Status.reason_phrase(code)

    ErrorView.build_error(reason, code, detail || reason)
    |> ErrorView.serialize_error()
  end
end
