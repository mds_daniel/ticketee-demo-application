defmodule TicketeeWeb.API.AccountController do
  use TicketeeWeb, :controller

  alias TicketeeWeb.API.UserView

  def show(conn, _params) do
    current_user = conn.assigns.current_user

    conn
    |> put_view(UserView)
    |> render("show.json", %{data: current_user})
  end
end
