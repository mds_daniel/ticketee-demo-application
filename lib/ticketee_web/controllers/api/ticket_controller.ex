defmodule TicketeeWeb.API.TicketController do
  use TicketeeWeb, :controller
  import TicketeeWeb.API.ControllerHelpers, only: [resp_with_error_status: 3]
  import TicketeeWeb.API.ProjectController, only: [set_project: 2]

  alias Ticketee.Authorize
  alias Ticketee.Projects

  plug :set_project, [param_key: "project_id"] when action in ~w(index_for_project)a
  plug :authorize_for_project when action in ~w(index_for_project)a

  plug :set_ticket when action in ~w(show)a
  plug :authorize_for_ticket when action in ~w(show)a

  def index(conn, params) do
    current_user = conn.assigns.current_user
    search_params = Map.get(params, "filter", %{})

    tickets = Authorize.search_tickets_for(current_user, search_params)
      |> preload_ticket_associations()

    render(conn, "show.json", %{data: tickets})
  end

  def index_for_project(conn, _params) do
    project = conn.assigns.project
    tickets = Projects.list_tickets_for_index(project)
      |> preload_ticket_associations()

    render(conn, "show.json", %{data: tickets})
  end

  def show(conn, _params) do
    ticket = conn.assigns.ticket
      |> preload_ticket_associations()

    render(conn, "show.json", %{data: ticket})
  end

  ## Helpers

  defp preload_ticket_associations(ticket_or_tickets) do
    ticket_or_tickets
    |> Projects.with_ticket_project()
    |> Projects.with_ticket_author()
    |> Projects.with_state()
    |> Projects.with_ticket_tags()
  end

  def set_ticket(conn, opts) do
    param_key = Keyword.get(opts, :param_key, "id")
    ticket_id = conn.params[param_key]
    ticket = ticket_id && Projects.get_ticket(ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      conn
      |> resp_with_error_status(:not_found, "Ticket not found")
      |> halt()
    end
  end

  defp authorize_for_project(conn, _opts) do
    %{current_user: current_user,
      project: project} = conn.assigns

    if Authorize.allowed?(current_user, :show, project) do
      conn
    else
      conn
      |> resp_with_error_status(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    if Authorize.allowed?(current_user, :show, ticket) do
      conn
    else
      conn
      |> resp_with_error_status(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end

  end
end
