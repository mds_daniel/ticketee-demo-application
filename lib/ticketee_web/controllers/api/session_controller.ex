defmodule TicketeeWeb.API.SessionController do
  use TicketeeWeb, :controller
  import TicketeeWeb.API.ControllerHelpers, only: [resp_with_error_status: 2]

  alias Ticketee.Accounts
  alias Ticketee.Accounts.User
  alias TicketeeWeb.API.UserView
  alias TicketeeWeb.APITokens

  def create(conn, %{"type" => "sessions",
                     "email" => email,
                     "password" => password}) do
    with {:ok, user} <- Accounts.authenticate(email, password),
         {:archived, false} <- {:archived, User.archived?(user)} do
      conn
      |> put_view(UserView)
      |> put_status(:created)
      |> render("show.json", %{
          data: user,
          meta: %{access_token: APITokens.sign(user.id)}
        })
    else
      _ ->
        resp_with_error_status(conn, :unauthorized)
    end
  end

  def create(conn, _other_params) do
    resp_with_error_status(conn, :unauthorized)
  end
end
