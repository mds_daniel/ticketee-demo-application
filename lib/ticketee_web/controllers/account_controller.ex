defmodule TicketeeWeb.AccountController do
  use TicketeeWeb, :controller

  alias Ticketee.Accounts
  alias Ticketee.Roles
  alias TicketeeWeb.Plugs.Auth
  alias TicketeeWeb.Notifications

  def show(conn, _params) do
    user = conn.assigns.current_user
    role_summary = Roles.project_role_summary_for(user)

    render(conn, "show.html", user: user, role_summary: role_summary)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->
        conn
        |> Auth.login(user)
        |> Notifications.send_welcome_email()
        |> put_flash(:info, "You have signed up successfully!")
        |> redirect(to: Routes.project_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create new account!")
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, _params) do
    user = conn.assigns.current_user
    changeset = Accounts.change_user(user, %{})

    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"user" => user_params}) do
    user = conn.assigns.current_user

    case Accounts.update_user_account(user, user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "Account has been successfully updated!")
        |> redirect(to: Routes.account_path(conn, :show))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to update account!")
        |> render("edit.html", changeset: changeset)
    end
  end
end
