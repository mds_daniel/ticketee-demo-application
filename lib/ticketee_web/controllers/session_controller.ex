defmodule TicketeeWeb.SessionController do
  use TicketeeWeb, :controller

  alias Ticketee.Accounts
  alias Ticketee.Accounts.User
  alias TicketeeWeb.Plugs.Auth

  @invalid_sign_in_message "Invalid email/password combination!"
  @account_archived_message "You cannot sign in, because your account has been archived!"

  def new(conn, _params) do
    render(conn, "new.html")
  end

  def create(conn, %{"session" => %{"email" => email, "password" => password}}) do
    with {:ok, user} <- Accounts.authenticate(email, password),
         {:archived, false} <- {:archived, User.archived?(user)} do
      conn
      |> Auth.login(user)
      |> put_flash(:info, "Signed in successfully!")
      |> redirect(to: Routes.project_path(conn, :index))
    else
      {:error, _any} ->
        conn
        |> put_flash(:error, @invalid_sign_in_message)
        |> render("new.html")

      {:archived, true} ->
        conn
        |> put_flash(:error, @account_archived_message)
        |> render("new.html")
    end
  end

  def create(conn, _other_params) do
    conn
    |> put_flash(:error, @invalid_sign_in_message)
    |> render("new.html")
  end

  def delete(conn, _params) do
    conn
    |> Auth.logout()
    |> put_flash(:info, "Signed out succesfully!")
    |> redirect(to: Routes.session_path(conn, :new))
  end
end
