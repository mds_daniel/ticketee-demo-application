defmodule TicketeeWeb.TicketWatcherController do
  use TicketeeWeb, :controller
  import TicketeeWeb.ControllerHelpers, only: [build_changeset_errors_message: 1]

  alias Ticketee.Projects
  alias Ticketee.Authorize
  alias Ticketee.Notifications.{Watchers, TicketWatcher}

  plug :set_ticket
  plug :authorize_for_ticket

  def create(conn, _) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    case Watchers.create_ticket_watcher(ticket, current_user) do
      {:ok, _watcher} ->
        conn
        |> put_flash(:info, "You are now watching this ticket!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, ticket.project_id, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to watch ticket! #{build_changeset_errors_message(changeset)}")
        |> redirect(to: Routes.project_ticket_path(conn, :show, ticket.project_id, ticket))
    end
  end

  def delete(conn, _) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    case Watchers.delete_ticket_watcher(ticket, current_user) do
      {:ok, _watcher} ->
        conn
        |> put_flash(:info, "You are no longer watching this ticket!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, ticket.project_id, ticket))

      {:error, :not_found} ->
        conn
        |> put_flash(:error, "Failed to unwatch ticket! Ticket was not being watched.")
        |> redirect(to: Routes.project_ticket_path(conn, :show, ticket.project_id, ticket))
    end
  end

  defp set_ticket(conn, _opts) do
    ticket_id = conn.params["ticket_id"]
    ticket = Projects.get_ticket(ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      conn
      |> resp(:not_found, "Ticket not found!")
      |> halt()
    end
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    if Authorize.allowed?(current_user, action_name(conn), {ticket, TicketWatcher}) do
      conn
    else
      conn
      |> resp(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end
end
