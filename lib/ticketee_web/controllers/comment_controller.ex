defmodule TicketeeWeb.CommentController do
  use TicketeeWeb, :controller

  alias Ticketee.Authorize
  alias Ticketee.Projects
  alias Ticketee.Projects.Comment
  alias TicketeeWeb.Notifications

  plug :set_ticket
  plug :set_comment when action in ~w(edit update delete)a
  plug :cache_project_role when action in ~w(index)a
  plug :authorize_for_ticket
  plug :set_states when action in ~w(index)a

  def index(conn, _params) do
    ticket = conn.assigns.ticket
      |> Projects.with_state()

    comments = Projects.list_ticket_comments_for_index(ticket)
    changeset = Projects.change_comment_for(ticket, %{})

    render(conn, "index.html", ticket: ticket, comments: comments, changeset: changeset)
  end

  def create(conn, %{"comment" => comment_params}) do
    current_user = conn.assigns.current_user
    ticket = conn.assigns.ticket
    comment_params = authorize_comment_params(current_user, ticket, comment_params)

    case Projects.create_comment_for(current_user, ticket, comment_params) do
      {:ok, comment} ->
        conn
        |> Notifications.add_ticket_watcher(ticket)
        |> Notifications.notify_comment_created(ticket, comment)
        |> put_flash(:info, "Comment has been successfully created!")
        |> redirect(to: Routes.ticket_comment_path(conn, :index, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create comment!")
        |> set_states()
        |> render("index.html", changeset: changeset,
                                ticket: Projects.with_state(ticket),
                                comments: Projects.list_ticket_comments_for_index(ticket))
    end
  end

  def edit(conn, _params) do
    comment = conn.assigns.comment
    changeset = Projects.change_comment(comment, %{})

    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"comment" => comment_params}) do
    %{current_user: current_user,
      ticket: ticket,
      comment: comment} = conn.assigns
    comment_params = authorize_comment_params(current_user, ticket, comment_params)

    case Projects.update_comment(comment, comment_params) do
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment has been successfully updated!")
        |> redirect(to: Routes.ticket_comment_path(conn, :index, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to update comment!")
        |> render("edit.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    %{ticket: ticket,
      comment: comment} = conn.assigns

    case Projects.delete_comment(comment) do
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment has been successfully deleted!")
        |> redirect(to: Routes.ticket_comment_path(conn, :index, ticket))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to delete comment!")
        |> redirect(to: Routes.ticket_comment_path(conn, :index, ticket))
    end
  end

  defp set_ticket(conn, _) do
    ticket_id = conn.params["ticket_id"]
    ticket = Projects.get_ticket(ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      conn
      |> resp(:not_found, "Ticket not found!")
      |> halt()
    end
  end

  defp set_comment(conn, _) do
    ticket = conn.assigns.ticket
    comment = Projects.get_comment_for(ticket, conn.params["id"])

    if comment do
      assign(conn, :comment, comment)
    else
      conn
      |> resp(:not_found, "Comment not found!")
      |> halt()
    end
  end

  defp set_states(conn, _opts \\ []) do
    states = Projects.list_states()

    conn
    |> assign(:states, states)
    |> assign(:indexed_states, Enum.into(states, %{}, &({&1.id, &1})))
  end

  defp authorize_comment_params(current_user, ticket, comment_params) do
    if Authorize.allowed?(current_user, :update, ticket) do
      comment_params
    else
      # Setting the comment `state_id` will also update the ticket's `state_id`
      comment_params
      |> Map.delete("state_id")
    end
  end

  defp authorize_for_ticket(conn, _) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    comment = conn.assigns[:comment] || Comment

    if Authorize.allowed?(current_user, action_name(conn), {ticket, comment}) do
      conn
    else
      conn
      |> resp(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end

  # Prevent multiple `roles` queries when rendering views by caching
  defp cache_project_role(conn, _opts) do
    %{current_user: current_user, ticket: ticket} = conn.assigns

    assign(conn, :current_user, Authorize.cache_project_role(current_user, ticket))
  end
end
