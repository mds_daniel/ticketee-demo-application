defmodule TicketeeWeb.TicketSearchController do
  use TicketeeWeb, :controller

  alias Ticketee.Authorize
  alias Ticketee.Projects
  alias Ticketee.Tags

  plug :set_search_options

  def index(conn, params) do
    current_user = conn.assigns.current_user
    search_params = Map.get(params, "search", %{})

    tickets = Authorize.search_tickets_for(current_user, search_params)
      |> Projects.with_ticket_author()
      |> Projects.with_ticket_tags()
      |> with_preloaded_search_associations(conn.assigns)

    render(conn, "search.html", tickets: tickets, search_params: search_params)
  end

  defp set_search_options(conn, _opts) do
    conn
    |> assign(:projects, Projects.list_projects_for_search())
    |> assign(:states, Projects.list_states())
    |> assign(:tags, Tags.list_tags())
  end

  defp with_preloaded_search_associations(tickets,
                                          %{projects: projects,
                                            states: states}) do
    indexed_projects = Enum.into(projects, %{}, &({&1.id, &1}))
    indexed_states = Enum.into(states, %{}, &({&1.id, &1}))

    Enum.map(tickets, fn ticket ->
      %{ticket | project: indexed_projects[ticket.project_id],
                 state: indexed_states[ticket.state_id]}
    end)
  end
end
