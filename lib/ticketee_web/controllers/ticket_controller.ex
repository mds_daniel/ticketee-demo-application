defmodule TicketeeWeb.TicketController do
  use TicketeeWeb, :controller
  import TicketeeWeb.ProjectController, only: [set_project: 2, cache_project_role: 2]

  alias Ticketee.Projects
  alias Ticketee.Projects.Ticket
  alias Ticketee.Attachments
  alias Ticketee.Authorize
  alias Ticketee.Notifications.Watchers
  alias TicketeeWeb.Notifications

  @ticket_not_found_message "The ticket you were looking for could not be found!"

  plug :set_project, param_key: "project_id"
  plug :set_ticket when action in ~w(show edit update delete)a
  plug :cache_project_role when action in ~w(show)a
  plug :authorize_for_ticket

  def show(conn, _params) do
    current_user = conn.assigns.current_user
    project = conn.assigns.project
    ticket = conn.assigns.ticket
      |> Projects.with_ticket_author()
      |> Projects.with_state()
      |> Projects.with_ticket_tags()

    watcher = Watchers.get_ticket_watcher(ticket, current_user)
    attachments = Attachments.attachments_for(ticket)

    conn
    |> set_title("Ticketee | Tickets | #{project.name} - #{ticket.title}")
    |> render("show.html", ticket: ticket, watcher: watcher, attachments: attachments)
  end

  def new(conn, _params) do
    project = conn.assigns.project
    changeset = Projects.change_ticket_for(project, %{})

    render(conn, "new.html", project: project, changeset: changeset)
  end

  def create(conn, %{"ticket" => ticket_params}) do
    current_user = conn.assigns.current_user
    project = conn.assigns.project

    case Projects.create_ticket_with_default_state_for(current_user, project, ticket_params) do
      {:ok, ticket} ->
        conn
        |> Notifications.add_ticket_watcher(ticket)
        |> put_flash(:info, "Ticket has been successfully created!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create ticket!")
        |> render("new.html", project: project, changeset: changeset)
    end
  end

  def edit(conn, _params) do
    ticket = conn.assigns.ticket
    changeset = Projects.change_ticket(ticket, %{})

    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"ticket" => ticket_params}) do
    %{project: project, ticket: ticket} = conn.assigns

    case Projects.update_ticket(ticket, ticket_params) do
      {:ok, ticket} ->
        conn
        |> put_flash(:info, "Ticket has been successfully updated!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to update ticket!")
        |> render("edit.html", project: project, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    %{project: project, ticket: ticket} = conn.assigns

    case Projects.delete_ticket(ticket) do
      {:ok, _ticket} ->
        conn
        |> put_flash(:info, "Ticket has been successfully deleted!")
        |> redirect(to: Routes.project_path(conn, :show, project))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to delete ticket!")
        |> redirect(to: Routes.project_ticket_path(conn, :show, project, ticket))
    end
  end

  ## Helpers

  defp set_ticket(conn, _opts) do
    ticket_id = conn.params["id"]
    project = conn.assigns.project

    ticket = ticket_id && Projects.get_ticket_for(project, ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      redirect_not_found(conn)
    end

  rescue
    Ecto.Query.CastError ->
      # When `params["id"]` cannot be cast to integer, also redirect
      redirect_not_found(conn)
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      project: project} = conn.assigns

    ticket = conn.assigns[:ticket] || Ticket
    action = action_name(conn)

    if Authorize.allowed?(current_user, action, {project, ticket}) do
      conn
    else
      conn
      |> put_flash(:error, Authorize.not_authorized_message())
      |> redirect(to: Routes.project_path(conn, :show, project))
      |> halt()
    end
  end

  defp redirect_not_found(conn, _opts \\ []) do
    project = conn.assigns.project

    conn
    |> put_flash(:error, @ticket_not_found_message)
    |> redirect(to: Routes.project_path(conn, :show, project))
    |> halt()
  end
end
