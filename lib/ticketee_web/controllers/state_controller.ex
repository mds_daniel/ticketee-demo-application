defmodule TicketeeWeb.StateController do
  use TicketeeWeb, :controller
  import TicketeeWeb.ControllerHelpers, only: [build_changeset_errors_map: 1]

  alias Ticketee.Projects

  plug :set_state when action in ~w(edit update delete make_default)a

  def index(conn, _params) do
    states = Projects.list_states()

    render(conn, "index.html", states: states)
  end

  def new(conn, _params) do
    changeset = Projects.change_state(%{})

    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"state" => state_params}) do
    case Projects.create_state(state_params) do
      {:ok, _state} ->
        conn
        |> put_flash(:info, "State has been successfully created!")
        |> redirect(to: Routes.state_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create state!")
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, _params) do
    state = conn.assigns.state
    changeset = Projects.change_state(state, %{})

    render(conn, "edit.html", state: state, changeset: changeset)
  end

  def update(conn, %{"state" => state_params}) do
    state = conn.assigns.state

    case Projects.update_state(state, state_params) do
      {:ok, _state} ->
        conn
        |> put_flash(:info, "State has been successfully updated!")
        |> redirect(to: Routes.state_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to update state!")
        |> render("edit.html", state: state, changeset: changeset)
    end
  end

  def delete(conn, _params) do
    state = conn.assigns.state

    case Projects.delete_state(state) do
      {:ok, _state} ->
        conn
        |> put_flash(:info, "State has been successfully deleted!")
        |> redirect(to: Routes.state_path(conn, :index))

      {:error, changeset} ->
        errors = build_changeset_errors_map(changeset)
        name_error = normalize_error_message(errors[:name])

        conn
        |> put_flash(:error, "Failed to delete state! #{name_error}.")
        |> redirect(to: Routes.state_path(conn, :index))
    end
  end

  def make_default(conn, _params) do
    state = conn.assigns.state

    case Projects.set_default_state(state) do
      {:ok, _state} ->
        conn
        |> put_flash(:info, "State has been successfully set as default!")
        |> redirect(to: Routes.state_path(conn, :index))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to set state as default!")
        |> redirect(to: Routes.state_path(conn, :index))
    end
  end

  defp set_state(conn, _opts) do
    state_id = conn.params["id"]
    state = Projects.get_state(state_id)

    if state do
      assign(conn, :state, state)
    else
      conn
      |> put_flash(:error, "State not found!")
      |> redirect(to: Routes.state_path(conn, :index))
      |> halt()
    end
  end

  defp normalize_error_message([message | _]) when is_binary(message) do
    String.capitalize(message)
  end

  defp normalize_error_message(nil), do: nil
end
