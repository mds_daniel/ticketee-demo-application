defmodule TicketeeWeb.ControllerHelpers do
  def set_title(conn, title) do
    Plug.Conn.assign(conn, :page_title, title)
  end

  def json_error(conn, status, error) do
    conn
    |> Plug.Conn.put_status(status)
    |> Phoenix.Controller.json(%{"errors" => error})
  end

  def json_error_changeset(conn, changeset) do
    errors = build_changeset_errors_map(changeset)
    json_error(conn, :unprocessable_entity, errors)
  end

  # A helper that transforms changeset errors into a map of messages.
  # Originally taken from `Ticketee.DataCase`
  def build_changeset_errors_map(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end

  def build_changeset_errors_message(changeset) do
    build_changeset_errors_map(changeset)
    |> Enum.map(fn {field, message} -> "#{field} - #{message}" end)
    |> Enum.join("; ")
  end

  def add_param(conn, key, value) do
    %{conn | params: Map.put(conn.params, key, value)}
  end
end
