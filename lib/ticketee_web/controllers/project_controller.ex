defmodule TicketeeWeb.ProjectController do
  use TicketeeWeb, :controller

  alias Ticketee.Projects
  alias Ticketee.Authorize

  @project_not_found_message "The project you were looking for could not be found!"

  plug :set_project when action in ~w(show edit update delete)a
  plug :cache_project_role when action in ~w(show)a
  plug :authorize_for_project when action in ~w(show edit update delete)a

  def index(conn, _params) do
    current_user = conn.assigns.current_user
    projects = Authorize.list_projects_for(current_user)

    render(conn, "index.html", projects: projects)
  end

  def show(conn, _params) do
    project = conn.assigns.project
    project = %{project | tickets: Projects.list_tickets_for_index(project)}

    conn
    |> set_title("Ticketee | Projects | #{project.name}")
    |> render("show.html", project: project)
  end

  def new(conn, _params) do
    changeset = Projects.change_project(%{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"project" => project_params}) do
    case Projects.create_project(project_params) do
      {:ok, project} ->
        conn
        |> put_flash(:info, "Project has been successfully created!")
        |> redirect(to: Routes.project_path(conn, :show, project))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create project!")
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, _params) do
    project = conn.assigns.project
    changeset = Projects.change_project(project, %{})

    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"project" => project_params}) do
    project = conn.assigns.project

    case Projects.update_project(project, project_params) do
      {:ok, project} ->
        conn
        |> put_flash(:info, "Project has been successfully updated!")
        |> redirect(to: Routes.project_path(conn, :show, project))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to update project!")
        |> render("edit.html", changeset: changeset)
    end
  end

  def delete(conn, _params) do
    project = conn.assigns.project

    case Projects.delete_project(project) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "Project has been successfully deleted!")
        |> redirect(to: Routes.project_path(conn, :index))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to delete project!")
        |> redirect(to: Routes.project_path(conn, :show, project))
    end
  end

  ## Helpers

  def set_project(conn, opts) do
    param_key = Keyword.get(opts, :param_key, "id")
    id = conn.params[param_key]
    project = id && Projects.get_project(id)

    if project do
      assign(conn, :project, project)
    else
      redirect_not_found(conn)
    end

  rescue
    Ecto.Query.CastError ->
      # When `params["id"]` cannot be cast to integer, also redirect
      redirect_not_found(conn)
  end

  defp authorize_for_project(conn, _opts) do
    %{current_user: current_user, project: project} = conn.assigns
    action = action_name(conn)

    if Authorize.allowed?(current_user, action, project) do
      conn
    else
      conn
      |> put_flash(:error, Authorize.not_authorized_message())
      |> redirect(to: Routes.project_path(conn, :index))
      |> halt()
    end
  end

  # Prevent multiple `roles` queries when rendering views by caching
  def cache_project_role(conn, _opts) do
    %{current_user: current_user, project: project} = conn.assigns

    assign(conn, :current_user, Authorize.cache_project_role(current_user, project))
  end

  defp redirect_not_found(conn, _opts \\ []) do
    conn
    |> put_flash(:error, @project_not_found_message)
    |> redirect(to: Routes.project_path(conn, :index))
    |> halt()
  end
end
