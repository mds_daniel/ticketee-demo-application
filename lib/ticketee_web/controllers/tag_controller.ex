defmodule TicketeeWeb.TagController do
  use TicketeeWeb, :controller

  alias Ticketee.Projects
  alias Ticketee.Tags
  alias Ticketee.Tags.Tag
  alias Ticketee.Authorize

  plug :set_ticket
  plug :set_tag when action in ~w(delete)a
  plug :authorize_for_ticket

  def index(conn, _params) do
    tags = Tags.list_tags()
    ticket = conn.assigns.ticket
      |> Projects.with_ticket_tags()

    render(conn, "index.html", ticket: ticket, tags: tags)
  end

  def create(conn, %{"tags" => tags}) when is_list(tags) do
    ticket = conn.assigns.ticket

    case Projects.create_ticket_tags(ticket, tags) do
      {:ok, _tags} ->
        conn
        |> put_flash(:info, "Tags have been successfully added!")
        |> redirect(to: Routes.ticket_tag_path(conn, :index, ticket))

      {:error, _} ->
        conn
        |> put_flash(:error, "Failed to assign tags!")
        |> redirect(to: Routes.ticket_tag_path(conn, :index, ticket))
    end
  end

  def create(conn, _other_params) do
    ticket = conn.assigns.ticket

    conn
    |> put_flash(:error, "Must select tags to be added!")
    |> redirect(to: Routes.ticket_tag_path(conn, :index, ticket))
  end

  def delete(conn, _params) do
    %{ticket: ticket,
      tag: tag} = conn.assigns

    case Projects.delete_ticket_tag(ticket, tag) do
      {:ok, _ticket_tag} ->
        conn
        |> put_flash(:info, "Tag has been successfully removed!")
        |> redirect(to: Routes.ticket_tag_path(conn, :index, ticket))

      {:error, _} ->
        conn
        |> put_flash(:error, "Failed to remove tag!")
        |> redirect(to: Routes.ticket_tag_path(conn, :index, ticket))
    end
  end

  defp set_ticket(conn, _opts) do
    ticket_id = conn.params["ticket_id"]
    ticket = Projects.get_ticket(ticket_id)

    if ticket do
      assign(conn, :ticket, ticket)
    else
      conn
      |> resp(:not_found, "Ticket not found!")
      |> halt()
    end
  end

  defp set_tag(conn, _opts) do
    tag_id = conn.params["id"]
    tag = Tags.get_tag(tag_id)

    if tag do
      assign(conn, :tag, tag)
    else
      conn
      |> resp(:not_found, "Tag not found!")
      |> halt()
    end
  end

  defp authorize_for_ticket(conn, _opts) do
    %{current_user: current_user,
      ticket: ticket} = conn.assigns

    tag = conn.assigns[:tag] || Tag

    if Authorize.allowed?(current_user, action_name(conn), {ticket, tag}) do
      conn
    else
      conn
      |> resp(:forbidden, Authorize.not_authorized_message())
      |> halt()
    end
  end
end
