defmodule TicketeeWeb.UserController do
  use TicketeeWeb, :controller

  alias Ticketee.Accounts
  alias Ticketee.Accounts.User

  plug :set_user when action in ~w(promote archive)a
  plug :prevent_action_on_current_user when action in ~w(promote archive)a
  plug :prevent_action_on_admin when action in ~w(archive)a

  def index(conn, _params) do
    users = Accounts.list_users()
    render(conn, "index.html", users: users)
  end

  def new(conn, _params) do
    changeset = Accounts.change_user(%{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"user" => user_params}) do
    case Accounts.admin_register_user(user_params) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User has been successfully created!")
        |> redirect(to: Routes.user_path(conn, :index))

      {:error, changeset} ->
        conn
        |> put_flash(:error, "Failed to create user!")
        |> render("new.html", changeset: changeset)
    end
  end

  def promote(conn, params)

  def promote(conn, %{"user" => %{"promote" => "admin"}}) do
    user = conn.assigns.user

    case Accounts.promote_to_admin(user) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User has been successfully promoted to admin!")
        |> redirect(to: Routes.user_path(conn, :index))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to promote user!")
        |> redirect(to: Routes.user_path(conn, :index))
    end
  end

  def promote(conn, _other_params) do
    resp(conn, :unprocessable_entity, "Invalid promote user params!")
  end

  def archive(conn, params)

  def archive(conn, %{"user" => %{"archive" => archive}}) do
    case Ecto.Type.cast(:boolean, archive) do
      {:ok, true} ->
        archive_user(conn)

      {:ok, false} ->
        restore_achived_user(conn)

      :error ->
        archive(conn, %{error: :invalid_params})
    end
  end

  def archive(conn, _other_params) do
    resp(conn, :unprocessable_entity, "Invalid archive user params!")
  end

  defp archive_user(conn) do
    user = conn.assigns.user

    case Accounts.archive_user(user) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User has been successfully archived!")
        |> redirect(to: Routes.user_path(conn, :index))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to archive user!")
        |> redirect(to: Routes.user_path(conn, :index))
    end
  end

  defp restore_achived_user(conn) do
    user = conn.assigns.user

    case Accounts.restore_archived_user(user) do
      {:ok, _user} ->
        conn
        |> put_flash(:info, "User has been successfully restored!")
        |> redirect(to: Routes.user_path(conn, :index))

      {:error, _changeset} ->
        conn
        |> put_flash(:error, "Failed to restore user!")
        |> redirect(to: Routes.user_path(conn, :index))
    end
  end

  def set_user(conn, opts) do
    param_key = Keyword.get(opts, :param_key, "id")
    user_id = conn.params[param_key]
    user = user_id && Accounts.get_user(user_id)

    if user do
      assign(conn, :user, user)
    else
      redirect_not_found(conn)
    end

  rescue
    Ecto.Query.CastError ->
      # When `params["id"]` cannot be cast to binary_id, also redirect
      redirect_not_found(conn)
  end

  defp redirect_not_found(conn) do
    conn
    |> put_flash(:error, "The user you were looking for could not be found!")
    |> redirect(to: Routes.user_path(conn, :index))
    |> halt()
  end

  defp prevent_action_on_current_user(conn, _opts) do
    %{user: user, current_user: current_user} = conn.assigns

    if user.id == current_user.id do
      conn
      |> put_flash(:error, "Action not permitted on own account!")
      |> redirect(to: Routes.user_path(conn, :index))
      |> halt()
    else
      conn
    end
  end

  defp prevent_action_on_admin(conn, _opts) do
    user = conn.assigns.user

    if User.admin?(user) do
      conn
      |> put_flash(:error, "Action not permitted on an admin account!")
      |> redirect(to: Routes.user_path(conn, :index))
      |> halt()
    else
      conn
    end
  end
end
