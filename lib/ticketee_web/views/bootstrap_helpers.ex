defmodule TicketeeWeb.BootstrapHelpers do
  @moduledoc """
  Helpers for building bootstrap forms and other elements.
  """

  use Phoenix.HTML

  def bootstrap_text_input(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    text_input(f, key, opts)
  end

  def bootstrap_textarea(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    textarea(f, key, opts)
  end

  def bootstrap_number_input(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    number_input(f, key, opts)
  end

  def bootstrap_password_input(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    password_input(f, key, opts)
  end

  def bootstrap_email_input(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    email_input(f, key, opts)
  end

  def bootstrap_color_input(f, key, opts \\ []) do
    opts = opts
      |> add_form_control_class(has_error?(f, key))

    opts = Keyword.update!(opts, :class, fn css_classes -> ["form-control-color " | css_classes] end)

    color_input(f, key, opts)
  end

  def bootstrap_assoc_select(f, key, records, opts \\ []) do
    html_opts = opts
      |> Keyword.get(:html_opts, [])
      |> add_select_class(has_error?(f, key))

    label_key = Keyword.get(opts, :label_key, :name)
    options = Enum.map(records, fn record -> {Map.get(record, label_key), record.id} end)

    select(f, key, options, html_opts)
  end

  defp add_form_control_class(opts, has_error)

  defp add_form_control_class(opts, false) do
    [{:class, "form-control"} | opts]
  end

  defp add_form_control_class(opts, true) do
    [{:class, "form-control is-invalid"} | opts]
  end

  defp add_select_class(opts, false) do
    [{:class, "form-select"} | opts]
  end

  defp add_select_class(opts, true) do
    [{:class, "form-select is-invalid"} | opts]
  end

  defp has_error?(changeset, key) do
    Keyword.has_key?(changeset.errors, key)
  end
end
