defmodule TicketeeWeb.TagView do
  use TicketeeWeb, :view
  alias Ticketee.Tags.Tag
  import TicketeeWeb.TicketSearchRouteHelpers, only: [search_tag_tickets_link: 2]

  def tag_badge(%Tag{id: tag_id, name: name}) do
    content_tag(:span, name, class: "tag-badge", data: [tag_id: tag_id])
  end

  def search_tag_badge(conn, %Tag{id: tag_id, name: name} = tag) do
    content_tag(:span, link(name, to: search_tag_tickets_link(conn, tag)),
      class: "tag-badge", data: [tag_id: tag_id])
  end
end
