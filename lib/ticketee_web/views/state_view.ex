defmodule TicketeeWeb.StateView do
  use TicketeeWeb, :view
  import TicketeeWeb.TicketSearchRouteHelpers, only: [search_state_tickets_link: 2]
  alias Ticketee.Projects.State

  def state_badge(%State{name: name, color: color}) do
    content_tag(:span, name, class: "state-badge", style: "background-color: #{color}")
  end

  def state_badge(nil), do: nil

  def search_state_badge(conn, %State{name: name, color: color} = state) do
    content_tag(:span, link(name, to: search_state_tickets_link(conn, state)),
      class: "state-badge", style: "background-color: #{color}")
  end

  def search_state_badge(_conn, nil), do: nil
end
