defmodule TicketeeWeb.UserRoleView do
  use TicketeeWeb, :view

  alias Ticketee.Roles.Role

  # Role labels must stay ordered in `available_roles` order
  @role_labels (
    Role.available_roles()
    |> Enum.map(fn role ->
        {role, String.capitalize(to_string(role))}
       end)
  )

  @role_labels_map Map.new(@role_labels)

  def role_labels do
    @role_labels
  end

  def role_option(selected, selected, label) do
    content_tag(:option, label, value: selected, selected: "selected")
  end

  def role_option(_selected, role, label) do
    content_tag(:option, label, value: role)
  end

  def project_role_badge(role) do
    label = Map.fetch!(@role_labels_map, role)
    content_tag(:span, label, class: "badge bg-#{role}")
  end

  def render("role.json", %{role: %Role{user_id: user_id,
                                        project_id: project_id,
                                        role: role}}) do
    %{user_id: user_id,
      project_id: project_id,
      role: role}
  end
end
