defmodule TicketeeWeb.CommentView do
  use TicketeeWeb, :view
  import TicketeeWeb.StateView, only: [state_badge: 1]

  alias Ticketee.Accounts.User
  alias Ticketee.Projects.Comment

  def comment_user(%User{email: email}) do
    email
  end

  def comment_user(%Ecto.Association.NotLoaded{}) do
    "Not Loaded"
  end

  def comment_user(nil) do
    "Anonymous"
  end

  def comment_state_transition(%Comment{previous_state_id: previous_state_id,
                                        state_id: state_id},
                               indexed_states) do
    if previous_state_id != state_id do
      content_tag(:span, class: "comment-state-transition") do
        ["state changed"]
        |> add_comment_state_badge("from", indexed_states[previous_state_id])
        |> add_comment_state_badge("to", indexed_states[state_id])
      end
    end
  end

  def comment_state_transition_text(%Comment{previous_state_id: previous_state_id,
                                             state_id: state_id},
                                    indexed_states) do
    if previous_state_id != state_id do
      ["State changed"]
      |> add_comment_state_text("from", indexed_states[previous_state_id])
      |> add_comment_state_text("to", indexed_states[state_id])
      |> Enum.reverse()
      |> Enum.join(" ")
    end
  end

  def add_comment_state_badge(content, label, state) when not is_nil(state) do
    content ++ [" ", label, " ", state_badge(state)]
  end

  def add_comment_state_badge(content, _label, nil), do: content

  defp add_comment_state_text(content, label, state) when not is_nil(state) do
    ["'" <> state.name <> "'", label | content]
  end

  defp add_comment_state_text(content, _label, nil), do: content
end
