defmodule TicketeeWeb.EmailView do
  use TicketeeWeb, :view
  import TicketeeWeb.CommentView, only: [
    comment_user: 1,
    comment_state_transition: 2,
    comment_state_transition_text: 2,
  ]

  @endpoint TicketeeWeb.Endpoint

  def projects_index_url do
    Routes.project_url(@endpoint, :index)
  end

  def ticket_comments_url(ticket) do
    Routes.ticket_comment_url(@endpoint, :index, ticket)
  end
end
