defmodule TicketeeWeb.TicketView do
  use TicketeeWeb, :view
  import TicketeeWeb.StateView, only: [search_state_badge: 2]
  import TicketeeWeb.TagView, only: [search_tag_badge: 2]
  import TicketeeWeb.TicketAttachmentView, only: [
      attachment_path: 2,
      attachment_preview_img: 2,
    ]

  alias Ticketee.Attachments
  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.Projects.Comment
  alias Ticketee.Tags.Tag
  alias Ticketee.Notifications.TicketWatcher

  def ticket_author(%{author: %{email: email}}) do
    email
  end

  def ticket_author(_other) do
    "N/A"
  end

  def ticket_created_at(%{inserted_at: inserted_at = %DateTime{}}) do
    timestamp = DateTime.truncate(inserted_at, :second)
    time_ago = time_ago_in_words(timestamp)

    "#{DateTime.to_string(timestamp)} (#{time_ago})"
  end

  def ticket_created_at(_other) do
    "N/A"
  end
end
