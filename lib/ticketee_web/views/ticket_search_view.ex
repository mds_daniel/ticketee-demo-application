defmodule TicketeeWeb.TicketSearchView do
  use TicketeeWeb, :view
  import TicketeeWeb.TicketView, only: [ticket_author: 1]
  import TicketeeWeb.StateView, only: [search_state_badge: 2]
  import TicketeeWeb.TagView, only: [search_tag_badge: 2]

  def project_search_options(projects) do
    Enum.map(projects, fn project -> {project.name, Phoenix.Param.to_param(project)} end)
  end

  def state_search_options(states) do
    Enum.map(states, fn state -> state.name end)
  end

  def tag_search_options(tags) do
    Enum.map(tags, fn tag -> tag.name end)
  end
end
