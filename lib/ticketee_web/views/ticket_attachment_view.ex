defmodule TicketeeWeb.TicketAttachmentView do
  use TicketeeWeb, :view

  alias Ticketee.Attachments.TicketAttachment
  alias Ticketee.Attachments.Upload

  def attachment_path(conn, %TicketAttachment{ticket_id: ticket_id,
                                              file: filename,
                                              updated_at: updated_at}) do
    # Set version timestamp for browser cache busting
    {seconds, _usecs} = DateTime.to_gregorian_seconds(updated_at)
    query_opts = [{"v", to_string(seconds)}]

    Routes.ticket_attachment_path(conn, :show, ticket_id, filename, query_opts)
  end

  def attachment_preview_img(conn, attachment) do
    if image_type?(attachment.file) do
      attachment_path = attachment_path(conn, attachment)

      link to: attachment_path do
        img_tag(attachment_path, class: "card-img-top attachment-img", alt: "Attachment preview")
      end
    end
  end

  defdelegate image_type?(filename), to: Upload
end
