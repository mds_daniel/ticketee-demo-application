defmodule TicketeeWeb.ProjectView do
  use TicketeeWeb, :view
  import TicketeeWeb.StateView, only: [state_badge: 1]
  import TicketeeWeb.TicketSearchRouteHelpers, only: [search_project_tickets_link: 2]

  alias Ticketee.Projects.Ticket
end
