defmodule TicketeeWeb.TicketSearchRouteHelpers do
  alias TicketeeWeb.Router.Helpers, as: Routes

  def search_project_tickets_link(conn, project) do
    permalink = Phoenix.Param.to_param(project)
    Routes.ticket_search_path(conn, :index, search: %{project_id: permalink})
  end

  def search_tag_tickets_link(conn, tag) do
    Routes.ticket_search_path(conn, :index, search: %{tag_name: tag.name})
  end

  def search_state_tickets_link(conn, state) do
    Routes.ticket_search_path(conn, :index, search: %{state_name: state.name})
  end
end
