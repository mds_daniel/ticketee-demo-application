defmodule TicketeeWeb.LayoutView do
  use TicketeeWeb, :view

  def nav_link(conn, text, opts) do
    opts = add_nav_link_opts(opts, conn.request_path, opts[:to])
    link(text, opts)
  end

  defp add_nav_link_opts(opts, current_path, current_path) do
    [{:class, "nav-link active"}, {"aria-current", "page"} | opts]
  end

  defp add_nav_link_opts(opts, _, _) do
    [{:class, "nav-link"} | opts]
  end
end
