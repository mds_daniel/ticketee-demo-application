defmodule TicketeeWeb.UserView do
  use TicketeeWeb, :view

  alias Ticketee.Accounts.User

  def user_role(%User{archived_at: %DateTime{}}) do
    content_tag(:span, "Archived", class: "badge bg-danger")
  end

  def user_role(%User{admin: true}) do
    content_tag(:span, "Admin", class: "badge bg-success")
  end

  def user_role(%User{admin: false}) do
    content_tag(:span, "Regular User", class: "badge bg-secondary")
  end
end
