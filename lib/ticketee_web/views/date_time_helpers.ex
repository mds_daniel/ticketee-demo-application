defmodule TicketeeWeb.DateTimeHelpers do
  @one_minute 60 # seconds
  @one_hour 60 * @one_minute
  @one_day 24 * @one_hour

  def time_ago_in_words(%DateTime{} = date_time) do
    DateTime.diff(DateTime.utc_now(), date_time, :second)
    |> seconds_ago_in_words()
  end

  def seconds_ago_in_words(value) when value >= @one_day do
    case div(value, @one_day) do
      1 -> "one day ago"
      num_days -> "#{num_days} days ago"
    end
  end

  def seconds_ago_in_words(value) when value >= @one_hour do
    case div(value, @one_hour) do
      1 -> "one hour ago"
      num_hours -> "#{num_hours} hours ago"
    end
  end

  def seconds_ago_in_words(value) when value >= @one_minute do
    case div(value, @one_minute) do
      1 -> "one minute ago"
      num_minutes -> "#{num_minutes} minutes ago"
    end
  end

  def seconds_ago_in_words(1), do: "one second ago"
  def seconds_ago_in_words(num_seconds), do: "#{num_seconds} seconds ago"

  def simple_datetime_format(datetime) do
    datetime
    |> DateTime.to_iso8601()
    |> String.replace(~r/[TZ]/, " ")
  end
end
