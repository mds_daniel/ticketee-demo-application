defmodule TicketeeWeb.API.CommentView do
  use JSONAPI.View, type: "comments"
  alias TicketeeWeb.API.{UserView, TicketView, StateView}

  def fields do
    [:description, :inserted_at]
  end

  def relationships do
    [user: {UserView, :include},
     ticket: {TicketView, :include},
     state: {StateView, :include},
     previous_state: {StateView, :include}]
  end
end
