defmodule TicketeeWeb.API.TicketView do
  use JSONAPI.View, type: "tickets"
  alias TicketeeWeb.API.{UserView, ProjectView, StateView, TagView}

  def fields do
    [:title, :description, :comment_count, :inserted_at]
  end

  def relationships do
    [project: {ProjectView, :include},
     author: {UserView, :include},
     state: {StateView, :include},
     tags: {TagView, :include}]
  end
end
