defmodule TicketeeWeb.API.StateView do
  use JSONAPI.View, type: "states"

  def fields do
    [:name, :color, :order_no, :default]
  end
end
