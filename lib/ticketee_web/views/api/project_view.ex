defmodule TicketeeWeb.API.ProjectView do
  use JSONAPI.View, type: "projects"

  def fields do
    [:name, :description, :slug, :inserted_at]
  end
end
