defmodule TicketeeWeb.APITokens do
  alias TicketeeWeb.Endpoint

  @max_age 24 * 60 * 60 # one day
  @salt "api tokens"

  def sign(value) do
    Phoenix.Token.sign(Endpoint, @salt, value)
  end

  def verify(value) do
    Phoenix.Token.verify(Endpoint, @salt, value, max_age: @max_age)
  end
end
