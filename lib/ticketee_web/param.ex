defimpl Phoenix.Param, for: Ticketee.Projects.Project do
  def to_param(%{id: id, slug: slug}) when not is_nil(slug) do
    "#{id}-#{slug}"
  end

  def to_param(%{id: id, slug: nil}) do
    to_string(id)
  end
end
