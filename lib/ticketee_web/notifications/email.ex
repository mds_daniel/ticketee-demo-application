defmodule TicketeeWeb.Notifications.Email do
  use Bamboo.Phoenix, view: TicketeeWeb.EmailView

  alias Ticketee.Accounts.User

  @from_address "support@ticketee.com"

  def welcome_email(%User{email: email}) do
    base_email()
    |> to(email)
    |> subject("[ticketee] Welcome to Ticketee")
    |> render(:welcome)
  end

  def comment_created_email(email, ticket, comment, indexed_states \\ %{}) do
    base_email()
    |> to(email)
    |> subject("[ticketee] New Comment for Ticket: #{ticket.title}")
    |> render(:new_comment, ticket: ticket, comment: comment, indexed_states: indexed_states)
  end

  defp base_email do
    new_email(from: @from_address)
    |> put_html_layout({TicketeeWeb.LayoutView, "email.html"})
  end
end
