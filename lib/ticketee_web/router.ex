defmodule TicketeeWeb.Router do
  use TicketeeWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TicketeeWeb.Plugs.Auth
    plug TicketeeWeb.Plugs.PreventArchivedUsers
  end

  pipeline :browser_ajax do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TicketeeWeb.Plugs.Auth
    plug TicketeeWeb.Plugs.PreventArchivedUsers
  end

  pipeline :only_logged_in do
    plug TicketeeWeb.Plugs.OnlyLoggedIn
  end

  pipeline :only_admin do
    plug TicketeeWeb.Plugs.OnlyAdmin
  end

  pipeline :jsonapi do
    plug JSONAPI.EnsureSpec
    plug JSONAPI.Deserializer
    plug JSONAPI.UnderscoreParameters
  end

  pipeline :api_logged_in do
    plug TicketeeWeb.Plugs.APIAuth
  end

  # All users (including those not logged in)
  scope "/", TicketeeWeb do
    pipe_through :browser

    resources "/accounts", AccountController, only: [:new, :create]
    resources "/sessions", SessionController, singleton: true, only: [:new, :create, :delete]
  end

  # Admin users
  scope "/", TicketeeWeb do
    pipe_through [:browser, :only_admin]

    get "/admin", PageController, :admin_index

    resources "/projects", ProjectController, only: [:new, :create, :delete]

    resources "/users", UserController, only: [:index, :new, :create]
    patch "/users/:id/promote", UserController, :promote
    patch "/users/:id/archive", UserController, :archive

    get "/users/:user_id/roles", UserRoleController, :show

    resources "/states", StateController, only: [:index, :new, :create, :edit, :update, :delete]
    patch "/states/:id/default", StateController, :make_default
  end

  # Admin users - AJAX support routes
  scope "/", TicketeeWeb do
    pipe_through [:browser_ajax, :only_admin]

    post "/users/:user_id/roles", UserRoleController, :create
    delete "/users/:user_id/roles/:project_id", UserRoleController, :delete
  end

  # Logged in users
  scope "/", TicketeeWeb do
    pipe_through [:browser, :only_logged_in]

    get "/", ProjectController, :index

    # "My Account" routes
    resources "/account", AccountController, singleton: true, only: [:show, :edit, :update]

    resources "/projects", ProjectController, only: [:index, :show, :edit, :update] do
      resources "/tickets", TicketController, except: [:index]
    end

    get "/tickets/search", TicketSearchController, :index

    resources "/tickets", TicketController, only: [] do
      resources "/comments", CommentController, only: [:index, :create, :edit, :update, :delete]
      resources "/tags", TagController, only: [:index, :create, :delete]
      resources "/watcher", TicketWatcherController, only: [:create, :delete],
        as: :watcher, singleton: true

      resources "/attachments", TicketAttachmentController,
        as: :attachment, only: [:show, :new, :create, :delete]
    end
  end

  # API Sessions
  scope "/api", TicketeeWeb.API, as: :api do
    pipe_through [:jsonapi]

    post "/sessions", SessionController, :create
  end

  # JSON-API
  scope "/api", TicketeeWeb.API, as: :api do
    pipe_through [:api_logged_in, :jsonapi]

    get "/account", AccountController, :show

    resources "/projects", ProjectController, only: [:index, :show] do
      get "/tickets", TicketController, :index_for_project
    end

    resources "/tickets", TicketController, only: [:index, :show] do
      resources "/comments", CommentController, only: [:index]
    end

    resources "/comments", CommentController, only: [:show]
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: TicketeeWeb.Telemetry
    end
  end

  if Mix.env() == :dev do
    forward "/sent_emails", Bamboo.SentEmailViewerPlug
  end
end
