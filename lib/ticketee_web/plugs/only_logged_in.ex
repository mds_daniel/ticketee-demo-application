defmodule TicketeeWeb.Plugs.OnlyLoggedIn do
  import Plug.Conn
  @behaviour Plug

  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]

  alias TicketeeWeb.Router.Helpers, as: Routes
  alias Ticketee.Accounts.User

  @impl true
  def init(opts) do
    opts
  end

  @impl true
  def call(conn, _opts) do
    case conn.assigns[:current_user] do
      %User{} ->
        conn

      nil ->
        conn
        |> put_flash(:error, "You must sign in first!")
        |> redirect(to: Routes.session_path(conn, :new))
        |> halt()
    end
  end
end
