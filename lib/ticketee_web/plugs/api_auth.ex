defmodule TicketeeWeb.Plugs.APIAuth do
  import Plug.Conn
  @behaviour Plug

  import TicketeeWeb.API.ControllerHelpers, only: [resp_with_error_status: 2]

  alias TicketeeWeb.APITokens
  alias Ticketee.Accounts

  @impl true
  def init(opts) do
    opts
  end

  @impl true
  def call(conn, _opts) do
    with {:ok, token} <- get_authorization_token(conn),
         {:ok, user_id} when is_binary(user_id) <- APITokens.verify(token),
         user when not is_nil(user) <- Accounts.get_user(user_id) do
      assign(conn, :current_user, user)
    else
      _ ->
        conn
        |> resp_with_error_status(:unauthorized)
        |> halt()
    end
  end

  defp get_authorization_token(conn) do
    case get_authorization(conn) do
      "Token token=" <> token when is_binary(token) ->
        {:ok, token}

      _ ->
        {:error, :invalid}
    end
  end

  defp get_authorization(conn) do
    conn
    |> get_req_header("authorization")
    |> List.first()
  end
end
