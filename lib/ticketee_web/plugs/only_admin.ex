defmodule TicketeeWeb.Plugs.OnlyAdmin do
  import Plug.Conn
  @behaviour Plug

  import Phoenix.Controller, only: [put_flash: 3, redirect: 2, get_format: 1]
  import TicketeeWeb.ControllerHelpers, only: [json_error: 3]

  alias TicketeeWeb.Router.Helpers, as: Routes
  alias Ticketee.Accounts.User

  @impl true
  def init(opts) do
    opts
  end

  @impl true
  def call(conn, _opts) do
    if User.admin?(conn.assigns[:current_user]) do
      conn
    else
      conn
      |> redirect_or_render_error(get_format(conn))
      |> halt()
    end
  end

  defp redirect_or_render_error(conn, "json") do
    conn
    |> json_error(:forbidden, %{"user" => ["must be signed in as an admin"]})
  end

  defp redirect_or_render_error(conn, _other) do
    conn
    |> put_flash(:error, "You must be signed in as an admin!")
    |> redirect(to: Routes.project_path(conn, :index))
  end
end
