defmodule TicketeeWeb.Plugs.PreventArchivedUsers do
  import Plug.Conn
  @behaviour Plug

  import Phoenix.Controller, only: [put_flash: 3, redirect: 2]

  alias TicketeeWeb.Router.Helpers, as: Routes
  alias TicketeeWeb.Plugs.Auth
  alias Ticketee.Accounts.User

  @impl true
  def init(opts) do
    opts
  end

  @impl true
  def call(conn, opts)

  def call(%{assigns: %{current_user: current_user = %User{}}} = conn, _opts) do
    if User.archived?(current_user) do
      conn
      |> Auth.logout()
      |> put_flash(:error, "Your account has been archived!")
      |> redirect(to: Routes.session_path(conn, :new))
      |> halt()
    else
      conn
    end
  end

  # When current_user not set, returns conn
  def call(conn, _opts) do
    conn
  end
end
