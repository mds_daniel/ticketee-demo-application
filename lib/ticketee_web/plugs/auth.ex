defmodule TicketeeWeb.Plugs.Auth do
  import Plug.Conn
  @behaviour Plug

  alias Ticketee.Accounts
  alias Ticketee.Accounts.User

  @impl true
  def init(opts) do
    opts
  end

  @impl true
  def call(conn, _opts) do
    user_id = get_session(conn, :user_id)
    user = user_id && Accounts.get_user(user_id)

    assign(conn, :current_user, user)
  end

  ## Sign in helpers

  def login(conn, %User{id: user_id} = user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user_id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    conn
    |> assign(:current_user, nil)
    |> put_session(:user_id, nil)
    |> configure_session(drop: true)
  end
end
