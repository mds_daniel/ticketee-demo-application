import { getCSRFToken } from './domHelpers.js';

export async function getResource(path) {
  let response = await fetch(path, {
    headers: {
      'Accept': 'application/json',
    }
  });

  return response.json();
}

export async function postResource(path, payload, csrfToken) {
  let response = await fetch(path, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-csrf-token': csrfToken || getCSRFToken(),
    },
    body: JSON.stringify(payload),
  });

  return response.json();
}

export async function deleteResource(path, csrfToken) {
  let response = await fetch(path, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-csrf-token': csrfToken || getCSRFToken(),
    },
    body: JSON.stringify({ '_method': 'DELETE' }),
  });

  return response.json();
}
