export function setupTagsForm(form) {
  let container = form.querySelector('#new-tag-container');
  let input = form.querySelector('#new-tag-name');
  let addButton = form.querySelector('#add-new-tag-button');

  addButton.addEventListener('click', () => {
    let value = input.value.trim();
    if (!value) { return; }

    createNewTagEntry(container, value);
    input.value = "";
    input.focus();
  });
}

function createNewTagEntry(container, value) {
  let node = document.createElement('div');
  node.className = "new-tag-entry";

  node.innerHTML = `
    <span class="badge bg-secondary">
      ${value}
      <button type="button" class="remove-tag" aria-label="Remove tag"></button>
    </span>
    <input type="hidden" name="tags[]" value="${value}">
  `;

  container.appendChild(node);

  let removeButton = node.querySelector('.remove-tag');
  removeButton.addEventListener('click', () => {
    container.removeChild(node);
  });
}
