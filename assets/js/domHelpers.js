export function getCSRFToken() {
  let meta = document.querySelector('meta[name="csrf-token"]');
  return meta && meta.content;
}

export function showFlashInfo(message) {
  let elem = document.querySelector('main p.alert-info[role="alert"]');
  if (!elem) { return; }

  elem.textContent = message;
}
