import { postResource, deleteResource } from './ajaxHelpers.js';
import { showFlashInfo } from './domHelpers.js';

export function setupUserRoleForm(container) {
  if (!container) return;

  let groups = Array.from(container.querySelectorAll('.user-role-group'));
  groups.forEach(setupUserRoleGroup);

  let newForm = container.querySelector('#new_project_role_form');
  setupNewRoleForm(newForm);
}

function setupUserRoleGroup(group) {
  let rolePath = group.action;
  let csrfToken = csrfTokenFromForm(group);
  let projectId = Number(group.querySelector('input[name="role[project_id]"]').value);
  let initialRole = group.querySelector('input[name="role[role]"]').value;

  let roleSelect = group.querySelector(`select[name="role[role]"]`);
  let roleLabel = group.querySelector(`label[for=${roleSelect.id}] span.badge`);
  let errorSpan = group.querySelector('span.invalid-feedback');

  let updateButton = group.querySelector('button[type="submit"]');
  toggleButton(updateButton, false);

  let revokeRoleButton = group.querySelector('button.btn-close');

  roleSelect.addEventListener('change', () => {
    toggleButton(updateButton, roleSelect.value !== initialRole);
  });

  // Handle granting role on form submit
  group.addEventListener('submit', (event) => {
    event.preventDefault();

    // Disable inputs while AJAX request is in flight
    roleSelect.disabled = true;
    toggleButton(updateButton, false);

    let payload = {
      role: {
        project_id: projectId,
        role: roleSelect.value,
      },
    };

    postResource(rolePath, payload, csrfToken)
      .then((response) => {
        let { project_id, role } = response;

        // Role successfully updated
        roleSelect.disabled = false;  // re-activate form select

        if (project_id === projectId) {
          roleSelect.classList.remove('is-invalid');
          removeErrorFeedback(errorSpan)

          initialRole = role;
          roleLabel.className = `badge bg-${role}`;
          roleLabel.textContent = capitalizeRole(role);
        } else {
          console.error('Invalid response returned:', response);
        }
      })
      .catch((err) => {
        toggleButton(updateButton, true); // re-activate submit button
        roleSelect.disabled = false; // re-activate form select
        roleSelect.classList.add('is-invalid');

        console.error(err);
        addErrorFeedback(errorSpan, 'Failed to update role!');
      });
  });

  // Handle revoking role
  revokeRoleButton.addEventListener('click', () => {
    deleteResource(`${rolePath}/${projectId}`, csrfToken)
      .then((response) => {
        let { project_id, role } = response;

        if (project_id === projectId) {
          let projectName = group.querySelector('.project-name').textContent;
          showFlashInfo(`Revoked role ${role} from project ${projectName}`);

          // Remove user role form for this project
          group.parentElement.removeChild(group);
        } else {
          console.error('Invalid response returned:', response);
        }
      })
      .catch((err) => {
        roleSelect.classList.add('is-invalid');
        console.error(err);
        addErrorFeedback(errorSpan, 'Failed to revoke role!');
      });
  })
}

function setupNewRoleForm(form) {
  if (!form) { return; }

  let rolePath = form.action;
  let csrfToken = csrfTokenFromForm(form);

  let submitButton = form.querySelector('button[type="submit"]');
  let errorSpan = form.querySelector('span.invalid-feedback');
  let roleSelect = form.querySelector(`select[name="role[role]"]`);
  let projectSelect = form.querySelector(`select[name="role[project_id]"]`);
  let availableRoles = getAvailableOptions(roleSelect);

  form.addEventListener('submit', (event) => {
    event.preventDefault();

    // Disable inputs while AJAX request is in flight
    roleSelect.disabled = true
    projectSelect.disabled = true
    toggleButton(submitButton, false);

    let projectId = Number(projectSelect.value);
    let payload = {
      role: {
        project_id: projectId,
        role: roleSelect.value,
      },
    };

    postResource(rolePath, payload, csrfToken)
      .then((response) => {
        let { project_id, role } = response;

        // Role successfully updated
        toggleButton(submitButton, true); // re-activate submit button
        roleSelect.disabled = false;  // re-activate form select
        projectSelect.disabled = false;

        if (project_id === projectId) {
          roleSelect.classList.remove('is-invalid');
          removeErrorFeedback(errorSpan)
          showFlashInfo('Role granted successfully!');

          // Refresh page to see new role
          createNewUserRoleGroup({
            container: form.parentElement,
            projectName: getSelectedLabel(projectSelect),
            rolePath,
            projectId,
            role,
            availableRoles,
            csrfToken,
          });
        } else {
          console.error('Invalid response returned:', response);
        }
      })
      .catch((err) => {
        toggleButton(submitButton, true); // re-activate submit button
        roleSelect.disabled = false;  // re-activate form select
        projectSelect.disabled = false;

        roleSelect.classList.add('is-invalid');
        console.error(err);
        addErrorFeedback(errorSpan, 'Failed to grant role!');
      });
  });
}

// Form rendering
function createNewUserRoleGroup(opts) {
  let { container, rolePath, projectId } = opts;

  let form = document.createElement('form');
  form.action = rolePath;
  form.className = 'user-role-group';
  form.method = 'post';
  renderNewUserRoleGroup(form, opts)
  removePreviousUserRoleGroupFor(container, projectId);

  let heading = container.querySelector('#grant-new-role-heading');
  container.insertBefore(form, heading);
  setupUserRoleGroup(form);
}

// Render form from 'templates/user_role/show.html.eex' client-side to prevent need for page reload
function renderNewUserRoleGroup(form, { projectId, projectName, role, availableRoles, csrfToken}) {
  form.innerHTML = `
    <input name="_csrf_token" type="hidden" value="${csrfToken}">
    <input name="role[project_id]" type="hidden" value="${projectId}">
    <input name="role[role]" type="hidden" value="${role}">
    <button type="button" class="btn-close" aria-label="Revoke role"></button>
    <label for="project-role_${projectId}" class="form-label">
      <span class="project-name">${projectName}</span>
      <span class="badge bg-${role}">${capitalizeRole(role)}</span>
    </label>
    <div class="input-group">
      <select id="project-role_${projectId}" name="role[role]" class="form-select" autocomplete="off">
      ${
        availableRoles.map(([value, label]) => {
          return `<option ${(role === value) ? 'selected="selected"' : ''} value="${value}">${label}</option>`;
        }).join("")
      }
      </select>
      <button type="submit" class="btn btn-outline-primary invisible" disabled="">Update</button>
      <span class="invalid-feedback invisible"></span>
    </div>
  `;
}

// In case a form group for this projectId already exists, we must remove it
// in order to prevent inconsistent view state
function removePreviousUserRoleGroupFor(container, projectId) {
  let hiddenInput = container.querySelector(`.user-role-group input[name="role[project_id]"][value="${projectId}"]`);
  if (!hiddenInput) { return; }

  let previousForm = closestParentElement(hiddenInput, (elem) => elem.className === 'user-role-group');
  container.removeChild(previousForm);
}

// Helper functions
function toggleButton(button, value) {
  button.classList.toggle('invisible', !value)
  button.disabled = !value;
}

function getAvailableOptions(select) {
  let options = select.options;
  let values = [];

  for (let i = 0; i < options.length; i++) {
    let option = options[i];
    values.push([option.value, option.label]);
  }

  return values;
}

function getSelectedLabel(select) {
  let option = select.options[select.selectedIndex];
  return option && option.label || 'Missing selected label!';
}

function closestParentElement(node, predicate) {
  node = node.parentElement;

  while (node) {
    if (predicate(node)) {
      return node;
    }

    node = node.parentElement;
  }

  return null;
}

function csrfTokenFromForm(form) {
  let input = form.querySelector('input[name="_csrf_token"]');
  return input && input.value;
}

function addErrorFeedback(errorSpan, message) {
  errorSpan.textContent = message;
  errorSpan.classList.remove('invisible');
}

function removeErrorFeedback(errorSpan) {
  errorSpan.classList.add('invisible');
  errorSpan.textContent = '';
}

function capitalizeRole(role) {
  return role[0].toUpperCase() + role.slice(1);
}
